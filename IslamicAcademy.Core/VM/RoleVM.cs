﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IslamicAcademy.Core.VM
{
    public class RoleVM
    {

        public class RolesCheckBox
        {
            public int Id { get; set; }
            public bool IsChecked { get; set; }

            public string Name { get; set; }
        }


        public class GroupsCheckBox
        {
            public int Id { get; set; }
            public bool IsChecked { get; set; }

            public string Name { get; set; }
        }


        public class NewGroup
        {


            public string Name { get; set; }


            public string Description { get; set; }

            public IList<RolesCheckBox> Roles { get; set; }

        }



        public class EditGroup
        {

            public int Id { get; set; }

            public string Name { get; set; }


            public string Description { get; set; }

            public IList<RolesCheckBox> Roles { get; set; }

        }

        public class ActionRoles
        {

            public int ActionID { get; set; }

            public IList<RolesCheckBox> Roles { get; set; }

        }
    }
}
