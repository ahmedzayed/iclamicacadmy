﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IslamicAcademy.Core.VM
{
   public class EvulationStudentVM
    {
        public int Id { get; set; }
        public string ElmatoneName { get; set; }
        public string StudentName { get; set; }
        public decimal Degree { get; set; }
        public string Nots { get; set; }
        public string Weaky { get; set; }
        public string Day { get; set; }
        public int Oral { get; set; }
        public int ElmatoneId { get; set; }
    }
}
