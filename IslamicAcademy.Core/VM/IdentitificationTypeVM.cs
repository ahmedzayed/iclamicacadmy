﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IslamicAcademy.Core.VM
{
  public   class IdentitificationTypeVM
    {
        public int ID { get; set; }
        public string TypeName { get; set; }

    }
}
