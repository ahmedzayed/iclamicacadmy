﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IslamicAcademy.Core.VM
{
   public class TaskSettingVM
    {
        public int Id { get; set; }
        public int TaskTypeId { get; set; }
        public string TaskTypeName { get; set; }
        public string Name { get; set; }
        public string Details { get; set; }
        public DateTime Date { get; set; }
    }
}
