﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IslamicAcademy.Core.VM
{
   public class SecondBranchRegisterFM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int NationalityId { get; set; }
        public string PhoneNumber { get; set; }
        public string TelegramNumber { get; set; }
        public string WhatsNumber { get; set; }
        public string Email { get; set; }
        public string AcadmayDetails { get; set; }
        public string Case { get; set; }
        public int EducationBodyId { get; set; }
        public int StageId { get; set; }
        public string STageEducationhigher { get; set; }
        public string Bachelorsdegree { get; set; }
        public string Year { get; set; }
        public string Level { get; set; }
        public string Elmatone { get; set; }
    }
}
