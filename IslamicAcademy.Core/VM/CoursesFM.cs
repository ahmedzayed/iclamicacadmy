﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IslamicAcademy.Core.VM
{
  public  class CoursesFM
    {
        public int Id { get; set; }
        public int BranchId { get; set; }
        public int ColleageId { get; set; }
        public int MatarialeId { get; set; }
        public int ElmatoneId { get; set; }
        public int   TrackId { get; set; }
        public int LevelId { get; set; }
        public string Weak { get; set; }
        public string Year { get; set; }
        public string Couresess { get; set; }
        public string Nots { get; set; }

    }
}
