﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IslamicAcademy.Core.VM
{
   public class InputFileVM
    {
        public int Id { get; set; }
        public int InPutId { get; set; }
        public string File { get; set; }
    }
}
