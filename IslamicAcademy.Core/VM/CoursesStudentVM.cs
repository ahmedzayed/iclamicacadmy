﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IslamicAcademy.Core.VM
{
   public class CoursesStudentVM
    {
        public int Id { get; set; }
        public string StudentName { get; set; }
        public string ElmatoneName { get; set; }
        public string MatrailName { get; set; }
        public string Weak { get; set; }
        public string Year { get; set; }
        public string Courses { get; set; }
    }
}
