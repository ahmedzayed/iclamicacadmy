﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IslamicAcademy.Core.VM
{
   public class RepresentationsVM
    {
        public int Id { get; set; }
        public string Details { get; set; }
    }
}
