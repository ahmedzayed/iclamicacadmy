﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IslamicAcademy.Core.VM
{
  public  class ResetPasswordVM
    {
        public int Id { get; set; }
        public string UserId { get; set; }
    }
}
