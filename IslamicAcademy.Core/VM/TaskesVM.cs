﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IslamicAcademy.Core.VM
{
  public  class TaskesVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int TaskTypeId { get; set; }
        public DateTime DateTime { get; set; }
        public int Type { get; set; }
        public string File { get; set; }
        public string TaskTypeName { get; set; }
        public string TimeProposed { get; set; }
        public string Details { get; set; }
        public string SendFrom { get; set; }
        public bool TaskeCase { get; set; }
        public bool RequestTime { get; set; }
        public string TimeProposedRequest { get; set; }
    }
}
