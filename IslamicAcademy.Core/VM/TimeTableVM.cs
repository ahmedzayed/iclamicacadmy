﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IslamicAcademy.Core.VM
{
   public class TimeTableVM
    {
        public int Id { get; set; }
        public string Year { get; set; }
        public string Day { get; set; }
        public string Time { get; set; }
        public string ElmatoneName { get; set; }
        public int ElmatoneId { get; set; }
    }
}
