﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IslamicAcademy.Core.VM
{
   public class SecondBranchRegisterVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string NationalityName{ get; set; }
        public string PhoneNumber { get; set; }
        public string TelegramNumber { get; set; }
        public string WhatsNumber { get; set; }
        public string Email { get; set; }
        public string AcadmayDetails { get; set; }
        public string Case { get; set; }
        public string EducationBodyName { get; set; }
        public string StageName{ get; set; }
        public string STageEducationhigher { get; set; }
        public string Bachelorsdegree { get; set; }
        public int LevelId { get; set; }
        public string Elmatone { get; set; }

    }
}
