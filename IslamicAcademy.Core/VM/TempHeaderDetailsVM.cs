﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IslamicAcademy.Core.VM
{
   public class TempHeaderDetailsVM
    {
        public int Id { get; set; }
        public int ElmatoneId { get; set; }
        public string ElmatoneName { get; set; }
        public int HeaderId { get; set; }
    }
}
