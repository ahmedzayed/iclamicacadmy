﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IslamicAcademy.Core.VM
{
   public class FirestBranchRegisterNotReceivedVM
    {
        public int Id { get; set; }
        public string ColleageName { get; set; }
        public string Name { get; set; }
        public string NationalityName { get; set; }
        public string Code { get; set; }
        public string Phonenumber { get; set; }
        public string WhatsNumber { get; set; }
        public string TelgramNumber { get; set; }
        public string Email { get; set; }
        public string StageName { get; set; }
        public int LevelId { get; set; }
        public string LeveleName { get; set; }
        public string Case { get; set; }
        public bool IsAbroved { get; set; }
        public string JoinedUniversity { get; set; }
        public string EducationBodyName { get; set; }
        public string EvaulationDayes { get; set; }
        public string TrackName { get; set; }
        public int TrackId { get; set; }
        public string UserName { get; set; }
        public bool IsReceived { get; set; }

    }
}
