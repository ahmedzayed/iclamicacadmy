﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IslamicAcademy.Core.VM
{
   public class InputVM
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public int LettersTypeId { get; set; }
        public string LettersTypeName { get; set; }
        public int OfficialAgenciesId { get; set; }
        public string OfficialAgenciesName { get; set; }
        public int Count { get; set; }
        public string Details { get; set; }
    }
}
