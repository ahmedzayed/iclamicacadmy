﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IslamicAcademy.Core.VM
{
  public  class EvulationStudentApprovedVM
    {
        public int Id { get; set; }
        public int StudentId { get; set; }
        public int ElmatoneId { get; set; }
        public int CoursesId { get; set; }
        public decimal Degree { get; set; }
        public int Oral { get; set; }
        public bool IsApproved { get; set; }
        public string StudentName { get; set; }
        public string ElmatoneName { get; set; }
    }
}
