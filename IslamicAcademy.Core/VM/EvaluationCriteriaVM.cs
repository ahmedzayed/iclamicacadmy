﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IslamicAcademy.Core.VM
{
   public class EvaluationCriteriaVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Details { get; set; }
        public string BranchName { get; set; }
    }
}
