﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IslamicAcademy.Core.VM
{
   public class DocCourses
    {
        public int Id { get; set; }
        public int TrackId { get; set; }
        public int DoctrinesId { get; set; }
        public int DoctrinesDetailsId { get; set; }
        public string DoctrinesName { get; set; }
        public string DoctrinesDetailsName { get; set; }
    }
}
