﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IslamicAcademy.Core.VM
{
   public class PreparStudentVM
    {
        public int Id { get; set; }
        public string Weak { get; set; }
        public string Day { get; set; }
        public int StudentId { get; set; }
        public string StudentName { get; set; }
        public bool IsAttandce { get; set; }
    }
}
