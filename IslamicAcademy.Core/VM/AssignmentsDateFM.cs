﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IslamicAcademy.Core.VM
{
  public  class AssignmentsDateFM
    {
        public int Id { get; set; }
        public string Tittel { get; set; }
        public string Details { get; set; }
        public int BranchId { get; set; }
    }
}
