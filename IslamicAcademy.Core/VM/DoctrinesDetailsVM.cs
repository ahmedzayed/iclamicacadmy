﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IslamicAcademy.Core.VM
{
   public class DoctrinesDetailsVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int DoctrinesId { get; set; }
        public string DoctrinesName { get; set; }
    }
}
