﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IslamicAcademy.Core.VM
{
  public  class AcousticsFM
    {
        public int Id { get; set; }
        public string Tittel { get; set; }
        public string Link { get; set; }
        public int ElmatoneId { get; set; }
        public int MatrialId { get; set; }
    }
}
