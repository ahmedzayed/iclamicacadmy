﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IslamicAcademy.Core.VM
{
   public class ThirdBranchRegisterVM
    {
        public int Id { get; set; }
        public string Tracks { get; set; }
        public string Name { get; set; }
        public string NationalityName { get; set; }
        public string Code { get; set; }
        public string PhoneNumber { get; set; }
        public string WhatsNumber { get; set; }
        public string TelgrameNumber { get; set; }
        public string Email { get; set; }
        public string EducationBodyName { get; set; }
        public string    StageName{ get; set; }
        public int LevelId { get; set; }
        public string Case { get; set; }

    }
}
