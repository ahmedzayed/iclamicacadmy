﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IslamicAcademy.Core.VM
{
   public class StudentDocument
    {
        public int Id { get; set; }
        public int StudentId { get; set; }
        public int DocumentId { get; set; }
        public string CodeNumber { get; set; }
        public string StudentName { get; set; }
        public string DocumentName { get; set; }
    }
}
