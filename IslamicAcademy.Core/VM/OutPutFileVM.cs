﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IslamicAcademy.Core.VM
{
  public  class OutPutFileVM
    {
        public int Id { get; set; }
        public int OutPutId { get; set; }
        public string File { get; set; }
    }
}
