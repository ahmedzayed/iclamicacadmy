﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IslamicAcademy.Core.VM
{
  public  class TrackVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LeveleName { get; set; }
        public string CollageName { get; set; }
        public string Tittel { get; set; }
    }
}
