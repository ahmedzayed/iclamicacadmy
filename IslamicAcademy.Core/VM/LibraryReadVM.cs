﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IslamicAcademy.Core.VM
{
   public class LibraryReadVM
    {
        public int Id { get; set; }
        public string Tittel { get; set; }
        public string FilePathe { get; set; }
        public string MatrialName { get; set; }
        public string ElmatoneName { get; set; }
        public string VedoPathe { get; set; }
        public string LinkPathe { get; set; }
    }
}
