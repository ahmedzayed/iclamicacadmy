﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IslamicAcademy.Core.VM
{
   public class TempHeaderVM
    {
        public int Id { get; set; }
        public int StudentId { get; set; }
        public bool CoursesCase { get; set; }
        public string StudentName { get; set; }
    }
}
