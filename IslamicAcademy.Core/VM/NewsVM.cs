﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IslamicAcademy.Core.VM
{
   public class NewsVM
    {
        public int Id { get; set; }
        public string Tittel { get; set; }
        public string Details { get; set; }
        public string ImagePathe { get; set; }
        public DateTime Datetime { get; set; }
    }
}
