﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IslamicAcademy.Core.VM
{
  public  class GardsApprovedVM
    {
        public int Id { get; set; }
        public int StudentId { get; set; }
        public int EmatoneId { get; set; }
        public int CoursesId { get; set; }
        public decimal Degree { get; set; }
        public decimal Oral { get; set; }
        public string StudentName { get; set; }
        public string ElmatoneName { get; set; }
        public string CollageName { get; set; }
        public string LevelName { get; set; }
        public string TrackName { get; set; }
        public int TrackId { get; set; }
        public bool IsAproved { get; set; }
    }
}
