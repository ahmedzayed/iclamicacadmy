﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IslamicAcademy.Core.VM
{
   public  class EmployeeVM
    {
        public int ID { get; set; }
        public string Emp_Name { get; set; }
        public string Emp_Code { get; set; }
        public int TypeID { get; set; }
        public int NationatyID { get; set; }
        public int IdentitFicationID { get; set; }
        public string NationalCode { get; set; }
        public string Phone1 { get; set; }   
        public string Phone2 { get; set; }   
        public string Email { get; set; }   
        public string DetaildAddress { get; set; }   
        public string ImagePath { get; set; }   
    }
}
