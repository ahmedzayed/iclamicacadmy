﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IslamicAcademy.Core.VM
{
   public class ClassesVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public int EducationBodyId { get; set; }
        public string EducationBodyName { get; set; }
    }
}
