﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IslamicAcademy.Core.VM
{
   public class FirestBranchRegisterFM
    {
        public int Id { get; set; }
        public int ColleageId { get; set; }
        public string Name { get; set; }
        public int NationalityId { get; set; }
        public string Code { get; set; }
        public string Phonenumber { get; set; }
        public string WhatsNumber { get; set; }
        public string TelgramNumber { get; set; }
        public string Email { get; set; }
        public int StageId { get; set; }
        public int LevelId { get; set; }
        public string Case { get; set; }
        public bool IsAbroved { get; set; }
        public int TrackId { get; set; }



        public string JoinedUniversity { get; set; }
       
       
        public int EducationBodyId { get; set; }
        public string EvaulationDayes { get; set; }

    }
}
