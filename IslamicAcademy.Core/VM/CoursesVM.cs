﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IslamicAcademy.Core.VM
{
   public class CoursesVM
    {
        public int Id { get; set; }
        public string BranchName { get; set; }
        public string ColleageName { get; set; }
        public string MatarialeName { get; set; }
        public string ElmatoneName{ get; set; }
        public string TrackName { get; set; }
        public string LevelName { get; set; }
        public int ElmatoneId { get; set; }
        public string Weak { get; set; }
        public string year { get; set; }
        public string Couresess { get; set; }
        public int TrackId { get; set; }
        public string Nots { get; set; }

    }
}
