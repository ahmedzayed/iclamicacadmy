﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IslamicAcademy.Core.VM
{
  public  class GardsVM
    {
        public int Id { get; set; }
        public string ElmatoneName { get; set; }
        public string StudentName { get; set; }
        public decimal Degree { get; set; }
        public decimal Oral { get; set; }
        public int EmatoneId { get; set; }
    }
}
