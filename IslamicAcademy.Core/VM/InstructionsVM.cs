﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IslamicAcademy.Core.VM
{
   public class InstructionsVM
    {
        public int Id { get; set; }
        public DateTime Datetime { get; set; }
        public string Tittel { get; set; }
        public string Details { get; set; }
    }
}
