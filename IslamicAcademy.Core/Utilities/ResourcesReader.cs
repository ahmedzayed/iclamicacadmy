﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IslamicAcademy.Core.Utilities
{
  public  class ResourcesReader
    {
        public static bool IsArabic
        {
            get { return Thread.CurrentThread.CurrentUICulture.Name.StartsWith("ar"); }
        }

    }
}
