﻿using IslamicAcademy.Core;
using IslamicAcademy.Core.VM;
using IslamicAcademy.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using IslamicAcademy.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;

namespace IslamicAcademy.Controllers
{
    public class HomeController : Controller

    {

        DB_A3E980_metoon2018Entities db = new DB_A3E980_metoon2018Entities();

       

        public ActionResult StudentMaton(int StudentId, int TrackId)
        {
            var model = db.Courses.Where(a => a.TrackId == TrackId).Select(
               s => new StudentMatoneVM
               {
                   Id = (int)s.MatarialeId,
                   Name = s.Matiriale.Name,
                   MatoneId = (int)s.ElmatoneId,
                   MatoneName = s.Elmatone.Name


               }).ToList();
            var model1 = "SP_SelectFirestBranchRegisterId".ExecuParamsSqlOrStored(false, "Id".KVP(StudentId)).AsList<FirestBranchRegisterFM>();
            var LastId = "SP_GetLAstId".ExecuParamsSqlOrStored(false).AsList<FirestBranchRegisterFM>();

            var model2 = "SP_GetLatId".ExecuParamsSqlOrStored(false).AsList<LastFirestBranchRegisterFM>();
            ViewBag.UserName = model2.LastOrDefault().UserName;
            ViewBag.Name = model1.LastOrDefault().Name;
            ViewBag.Email = model1.LastOrDefault().Email;
            ViewBag.UserId = LastId.LastOrDefault().Id;


            ViewBag.StudentId = StudentId;
            ViewBag.TrackId = TrackId;
            var Track = "SP_SelectTrackId".ExecuParamsSqlOrStored(false, "Id".KVP(TrackId)).AsList<TrackFM>();
            ViewBag.TrackTittel = Track.FirstOrDefault().Tittel;
            ViewBag.TrackName = Track.FirstOrDefault().Name;
            return View(model);


        }

        public ActionResult StudentDoctrines(int StudentId, int TrackId)
        {

            //var model = db.DoctrinesCourses.Select(a => new StudentDoctrinesVM { Id = a.Id, TrackId = (int)a.TrackId, DocDetailsId = (int)a.DoctrinesDetailsId, DocDetailsName = a.DoctrinesDetails.Name, Name = a.Doctrines.Name, DoctrinesDetails = db.DoctrinesDetails.Where(z => z.DoctrinesId == a.DoctrinesId).ToList() }).Where(a => a.TrackId == TrackId).ToList();
            ViewBag.StudentId = StudentId;
            ViewBag.TrackId = TrackId;
            int i =0;

            var matrail = db.DoctrinesCourses.Where(a => a.TrackId == TrackId ).Distinct().ToList();
           List<StudentDoctrinesVM>bb = new List<StudentDoctrinesVM>();
            StudentDoctrinesVM s = new StudentDoctrinesVM();
            foreach (var item in matrail.Select(a=> new StudentDoctrinesVM { Id =(int) a.DoctrinesId,
            TrackId = (int)a.TrackId,
            DocDetailsId = (int)a.DoctrinesDetailsId,
            //s.DocDetailsName = item.DoctrinesDetails.Name;
            Name = a.Doctrines.Name,
            DoctrinesDetails = db.DoctrinesDetails.Where(z => z.DoctrinesId == a.DoctrinesId).ToList()

        }).Distinct())
            {
                if (i == 0)
                {
                    bb.Add(item);
                    i = 1;

                }
                else
                {


                    bb.Add(item);

                    var aa = bb.ToList();
                    foreach (var item1 in aa)
                    {
                        if (item1.Id == item.Id)
                        {
                            bb.Remove(item);

                        }
                        else
                        {
                            bb.Add(item);

                        }

                    }

                }

            };
            var model1 = bb.OrderBy(a=>a.Id).Distinct().ToList();
            



            return View(model1);


        }
        public ActionResult Index()
        {

            return View();
        }
        public void SendEmail(string EmailFrom, string Password, string EmailMessage, string EmailTo, string EmailTitile, string EmaileSTMP)
        {

            string emailFrom = EmailFrom;
            string password = Password;
            string emailTo = EmailTo;
            string body = EmailMessage;
            string subject = EmailTitile;

            using (MailMessage mail = new MailMessage())
            {
                mail.From = new MailAddress(emailFrom);
                mail.To.Add(EmailTo);
                mail.Subject = subject;

                mail.Body = body;
                mail.IsBodyHtml = true;



                SmtpClient smtpClient = new SmtpClient("smtp.gmail.com", Convert.ToInt32(587));
                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(emailFrom, Password);
                smtpClient.Credentials = credentials;
                smtpClient.EnableSsl = true;
                smtpClient.Send(mail);

            }
        }

        public ActionResult Index1()
        {
            string Email = "";
            if (User != null)
            {
                var context = new ApplicationDbContext();
                var username = User.Identity.Name;


                var user = context.Users.SingleOrDefault(u => u.UserName == username);
                Email = user.Email;

                //SendEmail("codlandit@gmail.com", "01061538163", "UserName IS:" +"تم تسجيل دخولك لاكادمية متون بنجاح"+ user.UserName + "Password Is "+user.PasswordHash, Email, "Message", "smtp.gmail.com");
            }


            if (User.IsInRole("admin"))
            {
                //TempData["success"] = "<script>alert('   اهلا بك فى اكادميه متون الاسلامية');</script>";

                return RedirectToAction("Index", "HomeAdmin", new { Area = "Admin" });
            }
            if (User.IsInRole("Student"))
            {
                //TempData["success"] = "<script>alert('   اهلا بك فى اكادميه متون الاسلامية');</script>";

                return RedirectToAction("Index", "HomeStudent", new { Area = "Student" });
            }
            if (User.IsInRole("Teacher"))
            {
                //TempData["success"] = "<script>alert('   اهلا بك فى اكادميه متون الاسلامية');</script>";

                return RedirectToAction("Index", "HomeTeacher", new { Area = "Teacher" });
            }
            if (User.IsInRole("HighCommittee"))
            {
                //TempData["success"] = "<script>alert('   اهلا بك فى اكادميه متون الاسلامية');</script>";

                return RedirectToAction("Index", "HighCommitteeHome", new { Area = "HighCommittee" });
            }
            if (User.IsInRole("Employee"))
            {
                //TempData["success"] = "<script>alert('   اهلا بك فى اكادميه متون الاسلامية');</script>";

                return RedirectToAction("Index", "HomeEmp", new { Area = "Employee" });
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult FirstBranch()
        {
            return View("FirstBranch1");
        }
        public ActionResult FirstBranchAdvantages()
        {
            var model = "SP_GetAllAdvantagesWeb".ExecuParamsSqlOrStored(false).AsList<AdvantagesFM>();
            return View(model.Where(a => a.BranchId == 1).ToList());
        }
        public ActionResult Collage()
        {
            var model = "SP_GetAllColleges".ExecuParamsSqlOrStored(false).AsList<CollegeVM>();

            return PartialView("_Collage", model);
        }
        public ActionResult FirstBranchSideMenu()
        {
            return PartialView("_FirstBranchSideMenu");
        }
        public ActionResult ScandBranchSideMenu()
        {
            return PartialView("_ScandBranchSlideMenu");
        }

        public ActionResult AssignmentsDate(int BranchId)
        {
            var model = "SP_GetAllAssignmentsDate".ExecuParamsSqlOrStored(false, "BranchId".KVP(BranchId)).AsList<AssignmentsDateVM>();
            return View(model);
        }


        public ActionResult EvaluationCriteria(int BranchId)
        {
            var model = "SP_GetAllEvaluationCriteria".ExecuParamsSqlOrStored(false, "BranchId".KVP(BranchId)).AsList<EvaluationCriteriaVM>();
            return View(model);
        }

        public ActionResult GetTrackListByLevel(int Id, int C_Id)
        {
            var model = "SP_GetTraksByCollageAndLevel".ExecuParamsSqlOrStored(false, "LevelId".KVP(Id)
                , "CollageID".KVP(C_Id)).AsList<TrackFM>();
            return PartialView("_Track", model);
        }

        public ActionResult GetCourses(int TrackId)
        {
            var model = "SP_GetAllCourses".ExecuParamsSqlOrStored(false, "TrackId".KVP(TrackId)).AsList<CoursesVM>();

            return PartialView("_CoursesByTrackAndLevel", model);
        }
        public ActionResult Library()
        {
            return View(0);
        }
        public ActionResult LibraryRead()
        {
            var model = "SP_GetAllLibraryRead".ExecuParamsSqlOrStored(false).AsList<LibraryReadVM>();
            return PartialView("_LibraryRead", model);
        }
        public ActionResult LibraryVisualiztions()
        {
            var model = "SP_GetAlVisualiztions".ExecuParamsSqlOrStored(false).AsList<AcousticsVM>();
            return PartialView("_LibraryVisualiztions", model);

        }

        public ActionResult LibraryAcoustics()
        {
            var model = "SP_GetAllAcoustics".ExecuParamsSqlOrStored(false).AsList<LibraryReadVM>();
            return PartialView("_LibraryAcoustics", model);

        }
        public ActionResult Visualiztions()
        {
            var model = "SP_GetAlVisualiztions".ExecuParamsSqlOrStored(false).AsList<AcousticsVM>();
            return PartialView("Visualiztions", model);
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Courses()
        {
            var model = "SP_GetAllMatiriale".ExecuParamsSqlOrStored(false).AsList<MatrailVM>();
            return PartialView("_Courses", model.Take(6));
        }
        public ActionResult PhotoGallery()
        {
            var model = "SP_GetAllPhotoGallery".ExecuParamsSqlOrStored(false).AsList<NewsVM>();
            return PartialView("_PhotoGallery", model.Take(12));
        }

        public ActionResult PhotoGallary()
        {
            return View();
        }
        public ActionResult News()
        {
            var model = "SP_GetAllNews".ExecuParamsSqlOrStored(false).AsList<NewsVM>();
            return PartialView("_News", model.Take(6).OrderByDescending(a => a.Datetime));
        }
        public ActionResult AllNewsSearch()
        {
            var model = "SP_GetAllNews".ExecuParamsSqlOrStored(false).AsList<NewsVM>();
            return PartialView("_AllNews", model.Take(6).OrderByDescending(a => a.Datetime));
        }

        public ActionResult News2()
        {
            var model = "SP_GetAllNews".ExecuParamsSqlOrStored(false).AsList<NewsVM>();
            return PartialView("_News2", model.Take(6).OrderByDescending(a => a.Datetime));
        }
        public ActionResult AboutUs()
        {
            return View();
        }
        public ActionResult IntroducingPrize()
        {
            var model = "SP_GetAllIntroducingPrize".ExecuParamsSqlOrStored(false).AsList<IntroducingPrizeVM>();
            return PartialView(model);
        }

        public ActionResult SysteamPrize()
        {
            var model = "SP_GetAllSysteamPrize".ExecuParamsSqlOrStored(false).AsList<SysteamPrizeVM>();
            return PartialView(model);
        }

        public ActionResult WordsSupevisor()
        {
            var model = "SP_GetAllWordsSupevisor".ExecuParamsSqlOrStored(false).AsList<WordsSupevisorVM>();
            return PartialView(model);
        }
        public ActionResult Representations()
        {
            var model = "SP_GetAllRepresentations".ExecuParamsSqlOrStored(false).AsList<RepresentationsVM>();
            return PartialView(model);
        }
        public ActionResult WordsManage()
        {
            var model = "SP_GetAllWordsManage".ExecuParamsSqlOrStored(false).AsList<WordsSupevisorVM>();
            return PartialView(model);
        }
        public ActionResult NewsDetails(int Id)
        {
            var model = "SP_GetAllNews".ExecuParamsSqlOrStored(false).AsList<NewsVM>();

            return View(model.Where(a => a.Id == Id).FirstOrDefault());
        }
        public ActionResult ContactUs()
        {
            return View();
        }

        public ActionResult Branches(int? Id = 1)
        {
            return View();
        }

        public ActionResult BranchDetails(int Id = 1)
        {
            ViewBag.BranchId = Id;
            return View();

        }
        public ActionResult AdvantagesByBranchId(int Id)
        {
            var model = "SP_GetAllAdvantagesWeb".ExecuParamsSqlOrStored(false).AsList<AdvantagesFM>();
            return PartialView(model.Where(a => a.BranchId == Id).ToList());

        }

        public ActionResult CollegesByBranchId(int Id)
        {
            var model = "SP_GetAllCollegesWeb".ExecuParamsSqlOrStored(false).AsList<AdvantagesFM>();
            return PartialView(model.Where(a => a.BranchId == Id).ToList());

        }
        public ActionResult AssignmentsDateByBranchId(int Id)
        {
            var model = "SP_GetAllAssignmentsDateWeb".ExecuParamsSqlOrStored(false).AsList<AssignmentsDateFM>();
            return PartialView(model.Where(a => a.BranchId == Id).ToList());
        }
        public ActionResult CoursesByBranchId(int Id)
        {
            var model = "SP_GetAllCoursesWeb".ExecuParamsSqlOrStored(false).AsList<CoursesFM>();
            return PartialView(model.Where(a => a.BranchId == Id).ToList());
        }

        public ActionResult EvaluationCriteriaByBranchId(int Id)
        {
            var model = "SP_GetAllEvaluationCriteriaWeb".ExecuParamsSqlOrStored(false).AsList<EvaluationCriteriaFM>();
            return PartialView(model.Where(a => a.BranchId == Id).ToList());

        }
        [HttpPost]
        public ActionResult ContactUs(ContactUsVM model)
        {
            var data = "SP_AddContactUs".ExecuParamsSqlOrStored(false, "Name".KVP(model.Name), "Email".KVP(model.Email),
                "Phone".KVP(model.Phone), "Subject".KVP(model.Subject),
                "Message".KVP(model.Message)
               , "Id".KVP(model.Id)).AsNonQuery();
            TempData["success"] = "<script>toastr.success(' تم اراسل رسالتك بنجاح');</script>";

            return RedirectToAction("ContactUs");
        }
        public ActionResult LastNews()
        {
            var model = "SP_GetAllNews".ExecuParamsSqlOrStored(false).AsList<NewsVM>();
            return PartialView("_LastNews", model.Take(10).OrderByDescending(a => a.Datetime));
        }
        public ActionResult AllNews()
        {
            var model = "SP_GetAllNews".ExecuParamsSqlOrStored(false).AsList<NewsVM>();

            return View(model);
        }
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult FirstBranchRegister()

        {

            RegisterVM Obj = new RegisterVM();

            var Level = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Level.AddRange("Sp_LeveleDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.LevelList = Level;
            var Colleage = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Colleage.AddRange("Sp_ColleageDrop".ExecuParamsSqlOrStored(false, "BranchId".KVP(1)).AsList<ColleageDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,

                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false


            }).ToList());
            ViewBag.ColleageList = Colleage;

            var Nationality = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Nationality.AddRange("SpNationalityDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.NationalityList = Nationality;

            var Stage = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Stage.AddRange("SpStageDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.StageList = Stage;

            var EducationBody = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            EducationBody.AddRange("SpEducationBodyDrop".ExecuParamsSqlOrStored(false, "BranchId".KVP(1)).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.EducationBodyList = EducationBody;

            var TrackList = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            TrackList.AddRange("SpTrackDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.TrackList = TrackList;
            return View(Obj);
        }
        public ActionResult BranchTwoRegister()
        {

            RegisterVM Obj = new RegisterVM();
            Obj.BranchId = 2;
            var Level = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Level.AddRange("Sp_LeveleDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.LevelList = Level;
            var Colleage = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Colleage.AddRange("Sp_ColleageDrop".ExecuParamsSqlOrStored(false, "BranchId".KVP(Obj.BranchId)).AsList<ColleageDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,

                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false


            }).ToList());
            ViewBag.ColleageList = Colleage;

            var Nationality = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Nationality.AddRange("SpNationalityDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.NationalityList = Nationality;

            var Stage = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Stage.AddRange("SpStageDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.StageList = Stage;

            var EducationBody = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            EducationBody.AddRange("SpEducationBodyDrop".ExecuParamsSqlOrStored(false, "BranchId".KVP(Obj.BranchId)).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.EducationBodyList = EducationBody;

            var TrackList = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            TrackList.AddRange("SpTrackDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.TrackList = TrackList;
            return View(Obj);
        }
        public ActionResult BranchTwoCourses()
        {

            var model = "SP_GetAllScandBranchCourses".ExecuParamsSqlOrStored(false).AsList<ScandBranchCoursesVM>();
            return PartialView("_BranchTwoCourses", model);
        }
        public ActionResult Register(int? BranchId = 0)

        {
            ViewBag.BranchId = BranchId;

            RegisterVM Obj = new RegisterVM();

            var Level = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Level.AddRange("Sp_LeveleDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.LevelList = Level;
            var Colleage = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Colleage.AddRange("Sp_ColleageDrop".ExecuParamsSqlOrStored(false, "BranchId".KVP(BranchId)).AsList<ColleageDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,

                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false


            }).ToList());
            ViewBag.ColleageList = Colleage;

            var Nationality = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Nationality.AddRange("SpNationalityDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.NationalityList = Nationality;

            var Stage = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Stage.AddRange("SpStageDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.StageList = Stage;

            var EducationBody = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            EducationBody.AddRange("SpEducationBodyDrop".ExecuParamsSqlOrStored(false, "BranchId".KVP(BranchId)).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.EducationBodyList = EducationBody;

            var TrackList = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            TrackList.AddRange("SpTrackDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.TrackList = TrackList;
            return View(Obj);
        }

        [HttpGet]
        public ActionResult GetTrackList(int id, int CollageId)
        {
            var Colleage = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Colleage.AddRange("Sp_TrackList".ExecuParamsSqlOrStored(false, "LevelId".KVP(id), "CollageId".KVP(CollageId)).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.CollegeList = Colleage;
            //var j = context.EducationalGrades.Where(e => e.StageId == id && e.IsDeleted == false).Select(g => new { g.NameAr, g.Id }).ToList();
            return Json(Colleage, JsonRequestBehavior.AllowGet);
        }
        public ActionResult CoursiesByTrack(int TrackId = 0)
        {
            var model = "SP_GetAllCourses".ExecuParamsSqlOrStored(false, "TrackId".KVP(TrackId)).AsList<CoursesVM>();

            return PartialView(model);
        }
        public ActionResult StudentCase()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult FirstBranchRegister(string Name, int? NationalityId, string JoinedUniversity, string Code,
            string Phonenumber, string WhatsNumber, string TelgramNumber, string Email, int? EducationBodyId, int? StageId, int? Level, string Case, int? ColleageId,
            int TrackId, string EvaulationDayes)
        {

            var data = "SP_AddFirestBranchRegister".ExecuParamsSqlOrStored(false, "Name".KVP(Name),
                  "NationalityId".KVP(NationalityId)
             , "JoinedUniversity".KVP(JoinedUniversity),
                  "Code".KVP(Code),
                  "Phonenumber".KVP(Phonenumber),
                  "WhatsNumber".KVP(WhatsNumber),
                  "TelgramNumber".KVP(TelgramNumber),
             "Email".KVP(Email), "EducationBodyId".KVP(EducationBodyId),
             "StageId".KVP(StageId), "Level".KVP(Level),
             "Case".KVP(Case), "ColleageId".KVP(ColleageId), "TrackId".KVP(TrackId),
             "EvaulationDayes".KVP(EvaulationDayes)
            , "Id".KVP(0)).AsNonQuery();
            var LastId = "SP_GetLAstId".ExecuParamsSqlOrStored(false).AsList<FirestBranchRegisterFM>();


            var studentDocumnet = "SP_AddStudentDocument".ExecuParamsSqlOrStored(false, "StudentId".KVP(LastId.FirstOrDefault().Id), "DocumentId".KVP(JoinedUniversity), "CodeNumber".KVP(Code),

             "Id".KVP(0)).AsNonQuery();
            TempData["success"] = "<script>toastr.success(' تم الاضافه بنجاح');</script>";

            var Track = "SP_SelectTrackId".ExecuParamsSqlOrStored(false, "Id".KVP(TrackId)).AsList<TrackFM>();
            ViewBag.TrackTittel = Track.FirstOrDefault().Tittel;
            ViewBag.TrackName = Track.FirstOrDefault().Name;




            var model = "SP_GetLatId".ExecuParamsSqlOrStored(false).AsList<LastFirestBranchRegisterFM>();
            var UserN = model.Last().UserName;
            var CoursesByTrack = "SP_GetAllCourses".ExecuParamsSqlOrStored(false, "TrackId".KVP(TrackId)).AsList<CoursesVM>();
            var coursesList = CoursesByTrack.Where(a => a.TrackId == TrackId).ToList();
            if (ColleageId == 4 || ColleageId == 5)
            {
                foreach (var item in coursesList)
                {
                    var Data = "SP_GetAllCoursessByElmatone".ExecuParamsSqlOrStored(false, "ElmatoneId".KVP(item.ElmatoneId), "TrackId".KVP(TrackId)).AsList<CoursesFM>();
                    var coursesdata = Data.FirstOrDefault();

                    var data2 = "SP_AddStudentCoursies2".ExecuParamsSqlOrStored(false, "StudentId".KVP(model.LastOrDefault().Id),
                     "ElmatoneId".KVP(item.ElmatoneId), "Year".KVP(coursesdata.Year), "Weak".KVP(coursesdata.Weak), "Courses".KVP(coursesdata.Couresess)

               , "Id".KVP(0)).AsNonQuery();

                }
                return Json(new { StudentId = model.LastOrDefault().Id, ColleageId = ColleageId, TrackId = TrackId }, JsonRequestBehavior.AllowGet);


            }
            if (ColleageId == 2)
            {
                if (TrackId != 37 || TrackId != 38 || TrackId != 39 || TrackId != 40)
                {
                    foreach (var item in coursesList)
                    {
                        var Data = "SP_GetAllCoursessByElmatone".ExecuParamsSqlOrStored(false, "ElmatoneId".KVP(item.ElmatoneId), "TrackId".KVP(TrackId)).AsList<CoursesFM>();
                        var coursesdata = Data.FirstOrDefault();
                        var data2 = "SP_AddStudentCoursies2".ExecuParamsSqlOrStored(false, "StudentId".KVP(model.LastOrDefault().Id),
                         "ElmatoneId".KVP(item.ElmatoneId), "Year".KVP(coursesdata.Year), "Weak".KVP(coursesdata.Weak), "Courses".KVP(coursesdata.Couresess)

                       , "Id".KVP(0)).AsNonQuery();

                    }
                    return Json(new { StudentId = model.LastOrDefault().Id, ColleageId = ColleageId, TrackId = TrackId }, JsonRequestBehavior.AllowGet);


                }
                else
                {
                    coursesList = coursesList.Where(a => a.MatarialeName != "أصول الفقه").ToList();

                    foreach (var item in coursesList)
                    {
                        var Data = "SP_GetAllCoursessByElmatone".ExecuParamsSqlOrStored(false, "ElmatoneId".KVP(item.ElmatoneId), "TrackId".KVP(TrackId)).AsList<CoursesFM>();
                        var coursesdata = Data.FirstOrDefault();
                        var data2 = "SP_AddStudentCoursies2".ExecuParamsSqlOrStored(false, "StudentId".KVP(model.LastOrDefault().Id),
                         "ElmatoneId".KVP(item.ElmatoneId), "Year".KVP(coursesdata.Year), "Weak".KVP(coursesdata.Weak), "Courses".KVP(coursesdata.Couresess)
                           , "Id".KVP(0)).AsNonQuery();

                    }
                    return Json(new { StudentId = model.LastOrDefault().Id, ColleageId = ColleageId, TrackId = TrackId }, JsonRequestBehavior.AllowGet);

                }
            }

            if (ColleageId == 3)
            {
                if (TrackId == 50)
                {
                    coursesList = coursesList.Where(a => a.MatarialeName != "أصول الفقه ").ToList();

                    foreach (var item in coursesList)
                    {
                        var Data = "SP_GetAllCoursessByElmatone".ExecuParamsSqlOrStored(false, "ElmatoneId".KVP(item.ElmatoneId), "TrackId".KVP(TrackId)).AsList<CoursesFM>();
                        var coursesdata = Data.FirstOrDefault();
                        var data2 = "SP_AddStudentCoursies2".ExecuParamsSqlOrStored(false, "StudentId".KVP(model.LastOrDefault().Id),
                         "ElmatoneId".KVP(item.ElmatoneId), "Year".KVP(coursesdata.Year), "Weak".KVP(coursesdata.Weak), "Courses".KVP(coursesdata.Couresess)
                           , "Id".KVP(0)).AsNonQuery();

                    }
                    return Json(new { StudentId = model.LastOrDefault().Id, ColleageId = ColleageId, TrackId = TrackId }, JsonRequestBehavior.AllowGet);


                }
                else if (TrackId == 51 || TrackId == 53 || TrackId == 55 || TrackId == 57 || TrackId == 59)
                {
                    coursesList = coursesList.Where(a => a.MatarialeName != "مصطلح الحديث").ToList();

                    foreach (var item in coursesList)
                    {
                        var Data = "SP_GetAllCoursessByElmatone".ExecuParamsSqlOrStored(false, "ElmatoneId".KVP(item.ElmatoneId), "TrackId".KVP(TrackId)).AsList<CoursesFM>();
                        var coursesdata = Data.FirstOrDefault();
                        var data2 = "SP_AddStudentCoursies2".ExecuParamsSqlOrStored(false, "StudentId".KVP(model.LastOrDefault().Id),
                         "ElmatoneId".KVP(item.ElmatoneId), "Year".KVP(coursesdata.Year), "Weak".KVP(coursesdata.Weak), "Courses".KVP(coursesdata.Couresess)
                           , "Id".KVP(0)).AsNonQuery();

                    }
                    return Json(new { StudentId = model.LastOrDefault().Id, ColleageId = ColleageId, TrackId = TrackId }, JsonRequestBehavior.AllowGet);


                }
                else
                {

                    foreach (var item in coursesList)
                    {
                        var Data = "SP_GetAllCoursessByElmatone".ExecuParamsSqlOrStored(false, "ElmatoneId".KVP(item.ElmatoneId), "TrackId".KVP(TrackId)).AsList<CoursesFM>();
                        var coursesdata = Data.FirstOrDefault();
                        var data2 = "SP_AddStudentCoursies2".ExecuParamsSqlOrStored(false, "StudentId".KVP(model.LastOrDefault().Id),
                         "ElmatoneId".KVP(item.ElmatoneId), "Year".KVP(coursesdata.Year), "Weak".KVP(coursesdata.Weak), "Courses".KVP(coursesdata.Couresess)
                           , "Id".KVP(0)).AsNonQuery();

                    }
                    return Json(new { StudentId = model.LastOrDefault().Id, ColleageId = ColleageId, TrackId = TrackId }, JsonRequestBehavior.AllowGet);

                }

            }

            if (ColleageId == 1)
            {
                if (TrackId == 1)
                {

                    coursesList = coursesList.Where(a => a.MatarialeName != "الحديث").ToList();

                    foreach (var item in coursesList)
                    {
                        var Data = "SP_GetAllCoursessByElmatone".ExecuParamsSqlOrStored(false, "ElmatoneId".KVP(item.ElmatoneId), "TrackId".KVP(TrackId)).AsList<CoursesFM>();
                        var coursesdata = Data.FirstOrDefault();
                        var data2 = "SP_AddStudentCoursies2".ExecuParamsSqlOrStored(false, "StudentId".KVP(model.LastOrDefault().Id),
                         "ElmatoneId".KVP(item.ElmatoneId), "Year".KVP(coursesdata.Year), "Weak".KVP(coursesdata.Weak), "Courses".KVP(coursesdata.Couresess)
                           , "Id".KVP(0)).AsNonQuery();

                    }

                    return Json(new { StudentId = model.LastOrDefault().Id, ColleageId = ColleageId, TrackId = TrackId }, JsonRequestBehavior.AllowGet);
                }
                else if (TrackId == 3)
                {
                    coursesList = coursesList.Where(a => a.MatarialeName != "أصول الفقه"&&a.MatarialeName != "الحديث").ToList();

                    foreach (var item in coursesList)
                    {
                        var Data = "SP_GetAllCoursessByElmatone".ExecuParamsSqlOrStored(false, "ElmatoneId".KVP(item.ElmatoneId), "TrackId".KVP(TrackId)).AsList<CoursesFM>();
                        var coursesdata = Data.FirstOrDefault();
                        var data2 = "SP_AddStudentCoursies2".ExecuParamsSqlOrStored(false, "StudentId".KVP(model.LastOrDefault().Id),
                         "ElmatoneId".KVP(item.ElmatoneId), "Year".KVP(coursesdata.Year), "Weak".KVP(coursesdata.Weak), "Courses".KVP(coursesdata.Couresess)
                           , "Id".KVP(0)).AsNonQuery();

                    }
                    return Json(new { StudentId = model.LastOrDefault().Id, ColleageId = ColleageId, TrackId = TrackId }, JsonRequestBehavior.AllowGet);

                }
                if (TrackId == 12 || TrackId == 16 || TrackId == 20 || TrackId == 24 || TrackId == 28 || TrackId == 32)
                {

                    foreach (var item in coursesList)
                    {
                        var Data = "SP_GetAllCoursessByElmatone".ExecuParamsSqlOrStored(false, "ElmatoneId".KVP(item.ElmatoneId), "TrackId".KVP(TrackId)).AsList<CoursesFM>();
                        var coursesdata = Data.FirstOrDefault();
                        var data2 = "SP_AddStudentCoursies2".ExecuParamsSqlOrStored(false, "StudentId".KVP(model.LastOrDefault().Id),
                         "ElmatoneId".KVP(item.ElmatoneId), "Year".KVP(coursesdata.Year), "Weak".KVP(coursesdata.Weak), "Courses".KVP(coursesdata.Couresess)
                           , "Id".KVP(0)).AsNonQuery();

                    }

                    var model2 = "SP_GetLatId".ExecuParamsSqlOrStored(false).AsList<LastFirestBranchRegisterFM>();
                    ViewBag.UserName = model2.LastOrDefault().UserName;
                    ViewBag.Name = Name;
                    ViewBag.Email = Email;
                    ViewBag.UserId = LastId.LastOrDefault().Id;
                    return Json(new { StudentId = model.LastOrDefault().Id, ColleageId = ColleageId, TrackId = TrackId }, JsonRequestBehavior.AllowGet);

                    //return Json(new { UserName = model2.LastOrDefault().UserName, Name = Name, Email = Email, UserId = LastId.LastOrDefault().Id }, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    foreach (var item in coursesList)
                    {
                        var Data = "SP_GetAllCoursessByElmatone".ExecuParamsSqlOrStored(false, "ElmatoneId".KVP(item.ElmatoneId), "TrackId".KVP(TrackId)).AsList<CoursesFM>();
                        var coursesdata = Data.FirstOrDefault();
                        var data2 = "SP_AddStudentCoursies2".ExecuParamsSqlOrStored(false, "StudentId".KVP(model.LastOrDefault().Id),
                         "ElmatoneId".KVP(item.ElmatoneId), "Year".KVP(coursesdata.Year), "Weak".KVP(coursesdata.Weak), "Courses".KVP(coursesdata.Couresess)
                           , "Id".KVP(0)).AsNonQuery();

                    }
                    return Json(new { StudentId = model.LastOrDefault().Id, ColleageId = ColleageId, TrackId = TrackId }, JsonRequestBehavior.AllowGet);


                }
            }

            return Json(new { StudentId = model.LastOrDefault().Id, TrackId = TrackId }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult StudentMatonCollage2(int StudentId, int TrackId)
        {
            var matrail = db.Courses.Where(a => a.TrackId == TrackId && a.MatarialeId == 8).ToList();
            StudentMatoneVM s = new StudentMatoneVM();

            foreach (var item in matrail)
            {
                s.Id = (int)item.MatarialeId;
                s.Name = item.Matiriale.Name;
                s.Elmatones = matrail.ToList();
                s.MatoneId = (int)item.ElmatoneId;
                s.MatoneName = item.Elmatone.Name;

            };
            //var model = db.Courses.Where(a => a.TrackId == TrackId && a.MatarialeId == 8).Select(
            //   s => new StudentMatoneVM
            //   {
            //       Id = (int)s.MatarialeId,
            //       Name = s.Matiriale.Name,
            //       MatoneId = (int)s.ElmatoneId,
            //       MatoneName = s.Elmatone.Name


            //   }).ToList();


            ViewBag.StudentId = StudentId;
            ViewBag.TrackId = TrackId;
            var Track = "SP_SelectTrackId".ExecuParamsSqlOrStored(false, "Id".KVP(TrackId)).AsList<TrackFM>();
            ViewBag.TrackTittel = Track.FirstOrDefault().Tittel;
            ViewBag.TrackName = Track.FirstOrDefault().Name;
            return View(s);


        }

        public ActionResult StudentMatonCollage3(int StudentId, int TrackId)
        {

            var matrail = db.Courses.Where(a => a.TrackId == TrackId && a.MatarialeId == 8).ToList();
            StudentMatoneVM s = new StudentMatoneVM();

            foreach (var item in matrail)
            {
                s.Id = (int)item.MatarialeId;
                s.Name = item.Matiriale.Name;
                s.Elmatones = matrail.ToList();
                s.MatoneId = (int)item.ElmatoneId;
                s.MatoneName = item.Elmatone.Name;

            };
            //var model = db.Courses.Where(a => a.TrackId == TrackId && a.MatarialeId == 8).Select(
            //   s => new StudentMatoneVM
            //   {
            //       Id = (int)s.MatarialeId,
            //       Name = s.Matiriale.Name,
            //       MatoneId = (int)s.ElmatoneId,
            //       MatoneName = s.Elmatone.Name


            //   }).ToList();
            ViewBag.StudentId = StudentId;
            ViewBag.TrackId = TrackId;
            var Track = "SP_SelectTrackId".ExecuParamsSqlOrStored(false, "Id".KVP(TrackId)).AsList<TrackFM>();
            ViewBag.TrackTittel = Track.FirstOrDefault().Tittel;
            ViewBag.TrackName = Track.FirstOrDefault().Name;
            return View(s);


        }

        public ActionResult StudentMatonCollage51(int StudentId, int TrackId)
        {

            var matrail = db.Courses.Where(a => a.TrackId == TrackId && a.MatarialeId == 19).ToList();
            StudentMatoneVM s = new StudentMatoneVM();

            foreach (var item in matrail)
            {
                s.Id = (int)item.MatarialeId;
                s.Name = item.Matiriale.Name;
                s.Elmatones = matrail.ToList();
                s.MatoneId = (int)item.ElmatoneId;
                s.MatoneName = item.Elmatone.Name;

            };
            //var model = db.Courses.Where(a => a.TrackId == TrackId && a.MatarialeId==19).Select(
            //   s => new StudentMatoneVM
            //   {
            //       Id = (int)s.MatarialeId,
            //       Name = s.Matiriale.Name,
            //       MatoneId = (int)s.ElmatoneId,
            //       MatoneName = s.Elmatone.Name


            //   }).ToList();
            ViewBag.StudentId = StudentId;
            ViewBag.TrackId = TrackId;
            var Track = "SP_SelectTrackId".ExecuParamsSqlOrStored(false, "Id".KVP(TrackId)).AsList<TrackFM>();
            ViewBag.TrackTittel = Track.FirstOrDefault().Tittel;
            ViewBag.TrackName = Track.FirstOrDefault().Name;
            return View(s);


        }

        public ActionResult StudentMatonCollage4(int StudentId, int TrackId)
        {
            ///الحديث

            var matrail = db.Courses.Where(a => a.TrackId == TrackId && a.MatarialeId == 11).ToList();
            StudentMatoneVM s = new StudentMatoneVM();

            foreach (var item in matrail)
            {
                s.Id = (int)item.MatarialeId;
                s.Name = item.Matiriale.Name;
                s.Elmatones = matrail.ToList();
                s.MatoneId = (int)item.ElmatoneId;
                s.MatoneName = item.Elmatone.Name;

            };
            //var model = db.Courses.Where(a => a.TrackId == TrackId && a.MatarialeId == 11).Select(
            //   s => new StudentMatoneVM
            //   {
            //       Id = (int)s.MatarialeId,
            //       Name = s.Matiriale.Name,
            //       MatoneId = (int)s.ElmatoneId,
            //       MatoneName = s.Elmatone.Name


            //   }).ToList();
            ViewBag.StudentId = StudentId;
            ViewBag.TrackId = TrackId;
            var Track = "SP_SelectTrackId".ExecuParamsSqlOrStored(false, "Id".KVP(TrackId)).AsList<TrackFM>();
            ViewBag.TrackTittel = Track.FirstOrDefault().Tittel;
            ViewBag.TrackName = Track.FirstOrDefault().Name;
            return View(s);


        }


      
  
    public ActionResult StudentMatonCollage5(int StudentId, int TrackId)
        {
            ///اصول الفقهة
            
            var matrail = db.Courses.Where(a => a.TrackId == TrackId && a.MatarialeId == 8).ToList();
            StudentMatoneVM s = new StudentMatoneVM();

            foreach (var item in matrail)
            {
                s.Id = (int)item.MatarialeId;
                s.Name = item.Matiriale.Name;
                s.Elmatones = matrail.ToList();
                s.MatoneId = (int)item.ElmatoneId;
                s.MatoneName = item.Elmatone.Name;

            };

            //var model = db.Courses.Where(a => a.TrackId == TrackId && a.MatarialeId == 8).Select(
            //   s => new StudentMatoneVM
            //   {
            //       Id = (int)s.MatarialeId,
            //       Name = s.Matiriale.Name,
            //       MatoneId = (int)s.ElmatoneId,
            //       MatoneName = s.Elmatone.Name


            //   }).GroupBy(a=>a.MatoneId).ToList();
            ViewBag.StudentId = StudentId;
            ViewBag.TrackId = TrackId;
            var Track = "SP_SelectTrackId".ExecuParamsSqlOrStored(false, "Id".KVP(TrackId)).AsList<TrackFM>();
            ViewBag.TrackTittel = Track.FirstOrDefault().Tittel;
            ViewBag.TrackName = Track.FirstOrDefault().Name;
            return View(s);

        }

        public ActionResult Collage1Track3()
        {
            var data = db.Matiriale.Select(a => new StudentMatoneVM { Elmatone = db.Elmatone.Where(s => s.Id == 14||s.Id==15).ToList(), Name = a.Name, Id = a.Id });
            return PartialView("_Collage1Track3", data.Where(a=>a.Id==11));
        }

        public ActionResult ScandBranchDoctrines(int StudentId)
        {
            var model = db.Doctrines.Select(a => new StudentDoctrinesVM { Id = a.Id, Name = a.Name, DoctrinesDetails = db.DoctrinesDetails.Where(z => z.DoctrinesId == a.Id).ToList() });
            ViewBag.StudentId = StudentId;
            return View(model);
        }


        public ActionResult CoursesByStudentId(int StudentId)
        {
            var model = "SP_GetAllCoursesByStudentId".ExecuParamsSqlOrStored(false, "StudentId".KVP(StudentId)).AsList<CoursesStudentVM>();

            return PartialView("_CoursesByStudentId", model);
        }

        public ActionResult UpdateScandBranchDoctrines(Array CoursesList, int StudentId)
        {

            foreach (var item in CoursesList)
            {
                var data2 = "SP_AddStudentScandDoctrines".ExecuParamsSqlOrStored(false, "StudentId".KVP(StudentId),
                  "DoctrinesDetailsId".KVP(item)

            , "Id".KVP(0)).AsNonQuery();

            }
            var model = db.SecondBranchRegister.Where(a => a.Id == StudentId).FirstOrDefault();

            return Json(new { UserName = model.UserName, Name = model.Name, Email = model.Email, UserId = model.Id }, JsonRequestBehavior.AllowGet);

        }
        public ActionResult UpdateStudentElmatone(Array CoursesList, int StudentId, int TrackId)
        {

            var LastId = "SP_GetLAstId".ExecuParamsSqlOrStored(false).AsList<FirestBranchRegisterFM>();

            foreach (var item in CoursesList)
            {
                var Data = "SP_GetAllCoursessByElmatone".ExecuParamsSqlOrStored(false, "ElmatoneId".KVP(item), "TrackId".KVP(TrackId)).AsList<CoursesFM>();
                var coursesdata = Data.FirstOrDefault();
                var data2 = "SP_AddStudentCoursies2".ExecuParamsSqlOrStored(false, "StudentId".KVP(StudentId),
                         "ElmatoneId".KVP(item), "Year".KVP(coursesdata.Year), "Weak".KVP(coursesdata.Weak), "Courses".KVP(coursesdata.Couresess)
                           , "Id".KVP(0)).AsNonQuery();

            }
            var model = "SP_SelectFirestBranchRegisterId".ExecuParamsSqlOrStored(false, "Id".KVP(StudentId)).AsList<FirestBranchRegisterFM>();


            return Json(new { StudentId = model.LastOrDefault().Id }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult UpdateStudentDoctrines(Array CoursesList, int StudentId, string EvaulationDayes)
        {

            var LastId = "SP_GetLAstId".ExecuParamsSqlOrStored(false).AsList<FirestBranchRegisterFM>();
            var Evchange = "SP_UpdateEvaulationday".ExecuParamsSqlOrStored(false, "EvaulationDayes".KVP(EvaulationDayes),"StudentId".KVP(StudentId)
                        ).AsNonQuery();

            foreach (var item in CoursesList)
            {
                if (item =="11")
                {

                    var data1 = "SP_AddStudentCoursies2".ExecuParamsSqlOrStored(false, "StudentId".KVP(StudentId),
                         "ElmatoneId".KVP(item), "Year".KVP(""), "Weak".KVP(""), "Courses".KVP("")
                           , "Id".KVP(0)).AsNonQuery();
                }
              else  if (item == "14")
                {

                    var data1 = "SP_AddStudentCoursies2".ExecuParamsSqlOrStored(false, "StudentId".KVP(StudentId),
                         "ElmatoneId".KVP(item), "Year".KVP(""), "Weak".KVP(""), "Courses".KVP("")
                           , "Id".KVP(0)).AsNonQuery();
                }
                else
                {
                    var data2 = "SP_AddStudentDoctrines".ExecuParamsSqlOrStored(false, "StudentId".KVP(StudentId),
                      "DoctrinesDetailsId".KVP(item)

                , "Id".KVP(0)).AsNonQuery();
                }

            }
            var model = "SP_SelectFirestBranchRegisterId".ExecuParamsSqlOrStored(false, "Id".KVP(StudentId)).AsList<FirestBranchRegisterFM>();
            var model2 = "SP_GetLatId".ExecuParamsSqlOrStored(false).AsList<LastFirestBranchRegisterFM>();

            return Json(new { UserName = model2.LastOrDefault().UserName, Name = model.FirstOrDefault().Name, Email = model.LastOrDefault().Email, UserId = LastId.LastOrDefault().Id }, JsonRequestBehavior.AllowGet);

        }
        public ActionResult AddPassword(string UserName, string Name, string Email, int? UserId)
        {
            ViewBag.Name = Name;
            ViewBag.UserName = UserName;
            ViewBag.Email = Email;
            ViewBag.UserId = UserId;
            return View();
        }
        [HttpPost]
        public ActionResult Register(RegisterVM model)
        {
            if (model.BranchId == 2)
            {
                var data = "SP_AddSecondBranchRegister".ExecuParamsSqlOrStored(false,
                    "AcadmayDetails".KVP(model.AcadmayDetails), "Bachelorsdegree".KVP(model.Bachelorsdegree)
               , "Case".KVP(model.Case), "EducationBodyId".KVP(model.EducationBodyId),
                    "Elmatone".KVP(model.Elmatone), "Email".KVP(model.Phonenumber + "@gmail.com"),
                    "Level".KVP(model.LevelId), "Name".KVP(model.Name),
                    "NationalityId".KVP(model.NationalityId), "PhoneNumber".KVP(model.Phonenumber),
                    "STageEducationhigher".KVP(model.STageEducationhigher), "StageId".KVP(model.StageId),
                    "TelegramNumber".KVP(model.TelgramNumber), "WhatsNumber".KVP(model.WhatsNumber), "JoinedUniversity".KVP(model.JoinedUniversity)
              , "Id".KVP(model.Id)).AsNonQuery();
                if (data != 0)
                {
                    TempData["success"] = "<script>alert(' تم الاضافه بنجاح');</script>";
                }
                var LastRegisterdOne = db.SecondBranchRegister.ToList();


                return RedirectToAction("ScandBranchDoctrines", new
                {
                    StudentId = LastRegisterdOne.LastOrDefault().Id

                });

            }
            else
            {
                var data = "SP_AddThirdBranchRegister".ExecuParamsSqlOrStored(false, "Code".KVP(model.Code), "Tracks".KVP(model.Tracks)
               , "Case".KVP(model.Case), "EducationBodyId".KVP(model.EducationBodyId), "Email".KVP(model.Email), "Level".KVP(model.LevelId),
               "Name".KVP(model.Name), "NationalityId".KVP(model.NationalityId), "PhoneNumber".KVP(model.Phonenumber),
               "StageId".KVP(model.StageId), "TelgrameNumber".KVP(model.TelgramNumber), "WhatsNumber".KVP(model.WhatsNumber)
              , "Id".KVP(model.Id)).AsNonQuery();
                TempData["success"] = "<script>toastr.success(' تم الاضافه بنجاح');</script>";

                return RedirectToAction("Register");
            }

        }
        public ActionResult _SideMenu()
        {


            return PartialView();
        }

        public ActionResult UserDashBoard()
        {
            var user = User.Identity.Name;
            if (User.IsInRole("admin"))
            {
                return RedirectToAction("Index", "HomeAdmin", new { area = "Admin" });
            }
            else if (User.IsInRole("Student"))
            {
                return RedirectToAction("Index", "HomeStudent", new { area = "Student" });
            }
            else if (User.IsInRole("Teacher"))
            {
                return RedirectToAction("Index", "HomeTeacher", new { area = "Teacher" });

            }
            else if (User.IsInRole("HighCommittee"))
            {
                return RedirectToAction("Index", "HighCommitteeHome", new { Area = "HighCommittee" });
            }

            else
                return RedirectToAction("Index", "Home");




        }

        /// new Work 
        /// 
        public ActionResult RestePassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ResetPassword(string DocNumber,string PhoneNumber)
        {
            try
            {
                var model = "SP_GetUSerIdByDoc".ExecuParamsSqlOrStored(false, "DocNumber".KVP(DocNumber), "PhoneNumber".KVP(PhoneNumber)).AsList<ResetPasswordVM>();
                string user = model.FirstOrDefault().UserId;

                return RedirectToAction("ResetPasswordConfirm", new { UserId = user });
            }
            catch
                {
                TempData["Error"] = "<script>toastr.error('لا يوجد مستخدم بهذا الاسم');</script>";

                return RedirectToAction("ResetPassword");
                }

        }

        [HttpGet]
        public ActionResult ResetPasswordConfirm(string UserId)
        {
            ViewBag.UserId = UserId;
            return View();
        }

        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ResetPasswordConfirmchange(string Password, string UserId)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            UserStore<ApplicationUser> store = new UserStore<ApplicationUser>(context);
            UserManager<ApplicationUser> UserManager = new UserManager<ApplicationUser>(store);
            String newPassword = Password; ;
            String hashedNewPassword = UserManager.PasswordHasher.HashPassword(newPassword);
            ApplicationUser cUser = await store.FindByIdAsync(UserId);
            await store.SetPasswordHashAsync(cUser, hashedNewPassword);
            await store.UpdateAsync(cUser);

            return RedirectToAction("Login", "Account");
        }
    }
}