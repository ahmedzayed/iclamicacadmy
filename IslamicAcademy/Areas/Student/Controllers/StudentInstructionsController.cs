﻿using IslamicAcademy.Core;
using IslamicAcademy.Core.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IslamicAcademy.Areas.Student.Controllers
{
    [Authorize(Roles = "Student")]

    public class StudentInstructionsController : Controller
    {
        // GET: Student/StudentInstructions
        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {

            var model = "SP_GetAllStudentInstructions".ExecuParamsSqlOrStored(false).AsList<InstructionsVM>();
            return View(model);
        }

    }
}