﻿using IslamicAcademy.Core;
using IslamicAcademy.Core.VM;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IslamicAcademy.Areas.Student.Controllers
{
    [Authorize(Roles="Student")]
    public class CoursesStudentController : Controller
    {
        // GET: Student/Courses
       

        public ActionResult Index1()
        {
            var UserId = User.Identity.GetUserId();
            var StudentId = "SP_GeStudentId".ExecuParamsSqlOrStored(false, "UserId".KVP(UserId)).AsList<FirestBranchRegisterVM>();
            ViewBag.TrackName = StudentId.FirstOrDefault().TrackName;
            ViewBag.LevelName = StudentId.FirstOrDefault().LeveleName;
            ViewBag.CollageName = StudentId.FirstOrDefault().ColleageName;
            return View();
        }

        public ActionResult SearchDoc()
        {
            var UserId = User.Identity.GetUserId();
            var StudentId = "SP_GeStudentId".ExecuParamsSqlOrStored(false, "UserId".KVP(UserId)).AsList<FirestBranchRegisterVM>();

            var model = "SP_GetAllStudentDocuments".ExecuParamsSqlOrStored(false, "StudentId".KVP(StudentId.FirstOrDefault().Id)).AsList<StudentDocument>();
            return View(model);
        }
        public ActionResult Search()
        {
            var UserId = User.Identity.GetUserId();
            var StudentId= "SP_GeStudentId".ExecuParamsSqlOrStored(false, "UserId".KVP(UserId)).AsList<FirestBranchRegisterVM>();
            
            var model = "SP_GetAllCoursesByStudentId".ExecuParamsSqlOrStored(false,"StudentId".KVP(StudentId.FirstOrDefault().Id)).AsList<CoursesStudentVM>();
            return View(model);
        }
        public ActionResult CoursiesByTrack(int TrackId = 0)
        {
            var model = "SP_GetAllCourses".ExecuParamsSqlOrStored(false, "TrackId".KVP(TrackId)).AsList<CoursesVM>();

            return PartialView(model);
        }
        [HttpGet]
        public ActionResult GetTrackList(int id, int CollageId)
        {
            var Colleage = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Colleage.AddRange("Sp_TrackList".ExecuParamsSqlOrStored(false, "LevelId".KVP(id), "CollageId".KVP(CollageId)).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.CollegeList = Colleage;
            //var j = context.EducationalGrades.Where(e => e.StageId == id && e.IsDeleted == false).Select(g => new { g.NameAr, g.Id }).ToList();
            return Json(Colleage, JsonRequestBehavior.AllowGet);
        }


        public ActionResult Create()
        {

            var TrackList = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            TrackList.AddRange("SpTrackDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());

            ViewBag.TrackList = TrackList;
            var Colleage = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Colleage.AddRange("Sp_ColleageDrop".ExecuParamsSqlOrStored(false, "BranchId".KVP(1)).AsList<ColleageDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.CollegeList = Colleage;

            var Level = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Level.AddRange("Sp_LeveleDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.LevelList = Level;
            return View();
        }
        [HttpPost]
        public ActionResult Create(Array ElmatoneList)
        {
            var UserId = User.Identity.GetUserId();
            var StudentId = "SP_GeStudentId".ExecuParamsSqlOrStored(false, "UserId".KVP(UserId)).AsList<FirestBranchRegisterVM>();

            var data = "SP_AddTempHeader".ExecuParamsSqlOrStored(false, "StudentId".KVP(StudentId.FirstOrDefault().Id)
              , "Id".KVP(0)).AsNonQuery();
            var HeaderId = "SP_GetHeader".ExecuParamsSqlOrStored(false).AsList<TempHeaderVM>();


            foreach (var item in ElmatoneList)
            {
                var data1 = "SP_AddTempDetails".ExecuParamsSqlOrStored(false,
                    "ElmatoneId".KVP(item),"HeaderId".KVP(HeaderId.LastOrDefault().Id)
           ,"Id".KVP(0)).AsNonQuery();


            }

            return RedirectToAction("Index");

        }

        public ActionResult StudentDetails()
        {
            var UserId = User.Identity.GetUserId();
            var StudentId = "SP_GeStudentId".ExecuParamsSqlOrStored(false, "UserId".KVP(UserId)).AsList<FirestBranchRegisterVM>();
            ViewBag.StudentName = StudentId.FirstOrDefault().Name;
            ViewBag.Track = StudentId.FirstOrDefault().TrackName;
            ViewBag.LevelName = StudentId.FirstOrDefault().LeveleName;
            //ViewBag.ClassName = StudentId.FirstOrDefault().EducationBodyName;
            ViewBag.Number = StudentId.FirstOrDefault().Id;
            ViewBag.Year = StudentId.FirstOrDefault().EducationBodyName;

            return PartialView();
        }

    }
}