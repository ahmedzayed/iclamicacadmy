﻿using IslamicAcademy.Core;
using IslamicAcademy.Core.VM;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IslamicAcademy.Areas.Student.Controllers
{
    [Authorize(Roles = "Student")]

    public class EvulationStudentController : Controller
    {
        // GET: Student/EvulationStudent
        public ActionResult Index1()
        {

            return View();
        }
        public ActionResult StudentDetails()
        {
            var UserId = User.Identity.GetUserId();
            var StudentId = "SP_GeStudentId".ExecuParamsSqlOrStored(false, "UserId".KVP(UserId)).AsList<FirestBranchRegisterVM>();
            ViewBag.StudentName = StudentId.FirstOrDefault().Name;
            ViewBag.Track = StudentId.FirstOrDefault().TrackName;
            
            return PartialView();
        }
        public ActionResult StudentDetails2()
        {
            try
            {
                var UserId = User.Identity.GetUserId();
                var StudentId = "SP_GeStudentId".ExecuParamsSqlOrStored(false, "UserId".KVP(UserId)).AsList<FirestBranchRegisterVM>();

                var model = "SP_GetAllStudentGardsByStudentId".ExecuParamsSqlOrStored(false, "StudentId".KVP(StudentId.FirstOrDefault().Id)).AsList<GardsVM>();
                List<EvulationVM> Evla = new List<EvulationVM>();
                decimal Totalde = model.OrderByDescending(a => a.EmatoneId).Sum(a => a.Degree);
                decimal TotalOral = model.OrderByDescending(a => a.EmatoneId).Sum(a => a.Oral);
                decimal Total = Totalde + TotalOral;
                ViewBag.Total = Total;
                var Count = model.OrderByDescending(a => a.EmatoneId).Count();
                int TotalDegree = Count * 100;
                decimal Ev = (Total * 100) / TotalDegree;
                ViewBag.Ev = Ev;

            }
            catch
            {

            }
            return PartialView();
        }

        public ActionResult Search()
        {
            var UserId = User.Identity.GetUserId();
            var StudentId = "SP_GeStudentId".ExecuParamsSqlOrStored(false, "UserId".KVP(UserId)).AsList<FirestBranchRegisterVM>();

            var model = "SP_GetAllStudentGardsByStudentId".ExecuParamsSqlOrStored(false, "StudentId".KVP(StudentId.LastOrDefault().Id)).AsList<GardsVM>();
            List<EvulationVM> Evla = new List<EvulationVM>();

            foreach (var item in model.GroupBy(a=>a.EmatoneId))
            {
                decimal sumDegree = item.Sum(a => a.Degree);
                decimal sumOral = item.Sum(a => a.Oral);
                decimal Total = sumDegree + sumOral;
                EvulationVM Ev = new EvulationVM();
                Ev.Degree = sumDegree;
                Ev.ElmatoneName = item.FirstOrDefault().ElmatoneName;
                Ev.Ev =Total / 100 ;
                Ev.Evulation = "أ+";
                Ev.Oral = sumOral;
                Ev.Total = Total;
                Evla.Add(Ev);

            }

           
            
          
            return View(Evla);
        }
    }
}