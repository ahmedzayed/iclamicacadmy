﻿using IslamicAcademy.Core;
using IslamicAcademy.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CompareAttribute = System.ComponentModel.DataAnnotations.CompareAttribute;

namespace IslamicAcademy.Areas.Student.Controllers
{
    [Authorize(Roles = "Student")]

    public class HomeStudentController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public HomeStudentController()
        {
        }

        public HomeStudentController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        // GET: Student/HomeStudent
        public ActionResult Index()
        {
            ApplicationUser user = UserManager.FindById(User.Identity.GetUserId());
            ViewBag.user = user;
            return View();
        }

        public ActionResult _SideMenu()
        {


            return PartialView();
        }
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Home", new { Area = "" });
        }
        [HttpPost]
        public ActionResult ChnagePassword(ChangePasswordVM model)
        {
            if (!ModelState.IsValid)
                return Json(new { Succeeded = false }, JsonRequestBehavior.AllowGet);
            var userId = User.Identity.GetUserId();
            var result = UserManager.ChangePassword(userId, model.OldPassword, model.NewPassword);
            if (result.Succeeded)
                SignInManager.SignIn(UserManager.FindById(userId), false, false);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult ChnageImage()
        {

            var user = UserManager.FindById(User.Identity.GetUserId());
            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                if (file != null && file.ContentLength > 0)
                {
                    var fileNameWithExtension = Path.GetFileName(file.FileName);
                    var fileNameOnly = Path.GetFileNameWithoutExtension(file.FileName);
                    string RootPath = Server.MapPath("~/Uploads/Images/");
                    if (!Directory.Exists(RootPath))
                    {
                        Directory.CreateDirectory(RootPath);
                    }
                    var unique = new Guid();
                    var path = Path.Combine(RootPath, fileNameOnly + unique + Path.GetExtension(file.FileName));
                    file.SaveAs(path);
                    user.ImagePath = "Images/" + fileNameOnly + unique + Path.GetExtension(file.FileName);

                }
            }
            var data = "SP_UpdateUserImage".ExecuParamsSqlOrStored(false, "Id".KVP(user.Id),
                "ImagePath".KVP(user.ImagePath)).AsNonQuery();
            if (data == 0)
                return Json(new { Succeeded = false }, JsonRequestBehavior.AllowGet);
            return Json(new { Succeeded = true }, JsonRequestBehavior.AllowGet);

        }


        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
    }
    public class ChangePasswordVM
    {
        [Required]
        public string OldPassword { get; set; }
        [Required]
        public string NewPassword { get; set; }
        [Required, Compare("NewPassword")]
        public string ConfirmPassword { get; set; }
    }

}