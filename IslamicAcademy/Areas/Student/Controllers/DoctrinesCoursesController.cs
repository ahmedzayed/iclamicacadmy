﻿using IslamicAcademy.Core;
using IslamicAcademy.Core.VM;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IslamicAcademy.Areas.Student.Controllers
{
    public class DoctrinesCoursesController : Controller
    {
        // GET: Student/DoctrinesCourses
        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {
            var UserId = User.Identity.GetUserId();
            var StudentId = "SP_GeStudentId".ExecuParamsSqlOrStored(false, "UserId".KVP(UserId)).AsList<FirestBranchRegisterVM>();

            var model = "SP_GetAllDocCoursesByStudentId".ExecuParamsSqlOrStored(false, "StudentId".KVP(StudentId.FirstOrDefault().Id)).AsList<StudentDoctrinesCourses>();
            return View(model);
        }
    }
}