﻿using IslamicAcademy.Core;
using IslamicAcademy.Core.VM;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IslamicAcademy.Areas.Student.Controllers
{
    public class StudentDocumentsController : Controller
    {
        // GET: Student/StudentDocuments
        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {
            var UserId = User.Identity.GetUserId();
            var StudentId = "SP_GeStudentId".ExecuParamsSqlOrStored(false, "UserId".KVP(UserId)).AsList<FirestBranchRegisterVM>();

            var model = "SP_GetAllStudentDocuments".ExecuParamsSqlOrStored(false, "StudentId".KVP(StudentId.FirstOrDefault().Id)).AsList<StudentDocument>();
            return View(model);
        }


        public ActionResult Create()
        {
            StudentDocument Obj = new StudentDocument();
           
            return PartialView("~/Areas/Student/Views/StudentDocuments/Create.cshtml", Obj);
        }
        [HttpPost]
        public JsonResult Create(StudentDocument model)
        {
            var UserId = User.Identity.GetUserId();
            var StudentId = "SP_GeStudentId".ExecuParamsSqlOrStored(false, "UserId".KVP(UserId)).AsList<FirestBranchRegisterVM>();

            var data = "SP_AddStudentDocument".ExecuParamsSqlOrStored(false, "StudentId".KVP(StudentId.FirstOrDefault().Id), "DocumentId".KVP(model.DocumentId), "CodeNumber".KVP(model.CodeNumber),
             
               "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0 && model.Id != 0)
            {
                return Json("successEdit," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else if (data != 0)
            {
                return Json("success," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحفظ ", JsonRequestBehavior.AllowGet);
        }


        public ActionResult Edit(int id = 0)
        {
          
            var model = "SP_SelectStudentDocuments".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<StudentDocument>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            StudentDocument obj = new StudentDocument();
            obj.Id = id;
            return PartialView("~/Areas/Student/Views/StudentDocuments/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteStudentDocuments".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }
    }
}