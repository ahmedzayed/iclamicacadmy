﻿using IslamicAcademy.Core;
using IslamicAcademy.Core.VM;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IslamicAcademy.Areas.Student.Controllers
{
    public class CertificateController : Controller
    {
        // GET: Student/Certificate
        public ActionResult Index1()
        {
            return View();
        }
        public ActionResult StudentDetails()
        {
            var UserId = User.Identity.GetUserId();
            var StudentId = "SP_GeStudentId".ExecuParamsSqlOrStored(false, "UserId".KVP(UserId)).AsList<FirestBranchRegisterVM>();
            ViewBag.StudentName = StudentId.FirstOrDefault().Name;
            ViewBag.CollageName = StudentId.FirstOrDefault().ColleageName;
            ViewBag.Class = StudentId.FirstOrDefault().EducationBodyName;
            ViewBag.Level = StudentId.FirstOrDefault().LeveleName;
            ViewBag.Track = StudentId.FirstOrDefault().TrackName;

            var model = "SP_GetAllStudentGardsByStudentId".ExecuParamsSqlOrStored(false, "StudentId".KVP(StudentId.FirstOrDefault().Id)).AsList<GardsVM>();
            if (model.Count != 0)
            {
                List<EvulationVM> Evla = new List<EvulationVM>();
                decimal Totalde = model.OrderByDescending(a => a.EmatoneId).Sum(a => a.Degree);
                decimal TotalOral = model.OrderByDescending(a => a.EmatoneId).Sum(a => a.Oral);
                decimal Total = Totalde + TotalOral;
                ViewBag.Total = Total;
                var Count = model.OrderByDescending(a => a.EmatoneId).Count();
                int TotalDegree = Count * 100;
                decimal Ev = (Total * 100) / TotalDegree;
                if (Ev > 50)
                {
                    ViewBag.Ev = "مقبول";
                }
                if (Ev > 75)
                {
                    ViewBag.Ev = "جيد";

                }
                if (Ev > 90)
                {
                    ViewBag.Ev = "جيد جدا";
                }
                if (Ev > 95)
                {
                    ViewBag.Ev = "امتياز ";
                }
            }
            return PartialView();
        }
    }
}