﻿using IslamicAcademy.Core;
using IslamicAcademy.Core.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IslamicAcademy.Areas.Teacher.Controllers
{
    public class TeacherInstructionsDetailsController : Controller
    {
        // GET: Teacher/TeacherInstructionsDetails
        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {

            var model = "SP_GetAllTeacherInstructions".ExecuParamsSqlOrStored(false).AsList<InstructionsVM>();
            return View(model);
        }
    }
}