﻿using IslamicAcademy.Core;
using IslamicAcademy.Core.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IslamicAcademy.Areas.Teacher.Controllers
{
    public class AddgradesController : Controller
    {
        // GET: Teacher/Addgrades
        public ActionResult Index1()
        {
            var Colleage = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Colleage.AddRange("Sp_ColleageDrop".ExecuParamsSqlOrStored(false, "BranchId".KVP(1)).AsList<ColleageDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.CollegeList = Colleage;

            var Level = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Level.AddRange("Sp_LeveleDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.LevelList = Level;
            var Track = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Track.AddRange("SpTrackDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.TrackList = Track;

            return View();
          
        }

        [HttpGet]
        public ActionResult GetTrackList(int id, int CollageId)
        {
            var Colleage = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Colleage.AddRange("Sp_TrackList".ExecuParamsSqlOrStored(false, "LevelId".KVP(id), "CollageId".KVP(CollageId)).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.CollegeList = Colleage;
            //var j = context.EducationalGrades.Where(e => e.StageId == id && e.IsDeleted == false).Select(g => new { g.NameAr, g.Id }).ToList();
            return Json(Colleage, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Search(int? LevelId ,int? TrackId)
        {

                var model = "SP_GetAllFirestBranchRegisterC".ExecuParamsSqlOrStored(false, "LevelId".KVP(LevelId)).AsList<FirestBranchRegisterVM>();
            if(TrackId != null)
            {
                var Data = model.Where(a => a.TrackId == TrackId).ToList();
                return PartialView("_FirstSearch", Data);

            }
            else
            return PartialView("_FirstSearch", model);
            

        }

        public ActionResult AddGrades(int TrackingId,int StudentId)
        {
            ViewBag.TrackingId = TrackingId;
            ViewBag.StudentId = StudentId;

            return View();

        }

        public ActionResult GradesList(int? TrackingId,int? StudentId)
        {
            ViewBag.TrackingId = TrackingId;
            ViewBag.StudentId = StudentId;

            var model = "SP_GetAllCouressByStudentId".ExecuParamsSqlOrStored(false, "StudentId".KVP(StudentId)).AsList<StudentCoursesVM>();

            return PartialView("_ListGrades",model);
        }


        public ActionResult EditGrades(int ElmtoneId=0, decimal Value=0, int StudentId=0, int TrackingId=0,decimal Oral=0)
        {


            var data = "SP_AddStudentGards".ExecuParamsSqlOrStored(false,"StudentId".KVP(StudentId), "Value".KVP(Value), "ElmtoneId".KVP(ElmtoneId),"Oral".KVP(Oral),"Id".KVP(0)).AsNonQuery();
            if (data != 0)
            {
                TempData["success"] = "<script>toastr.success(' تم الاضافه بنجاح');</script>";

                return RedirectToAction("AddGrades", new { TrackingId = TrackingId , StudentId =StudentId});
            }
            else 
            {
                TempData["success"] = "<script>toastr.success(' لم يتم الحفظ  ');</script>";

                return RedirectToAction("AddGrades", new { TrackingId = TrackingId });
            }
        }
    }
}