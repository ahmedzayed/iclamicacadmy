﻿using IslamicAcademy.Core;
using IslamicAcademy.Core.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IslamicAcademy.Areas.Teacher.Controllers
{
    public class SettingController : Controller
    {
        // GET: Teacher/Setting
        public ActionResult Index1()
        {
            var Colleage = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Colleage.AddRange("Sp_ColleageDrop".ExecuParamsSqlOrStored(false, "BranchId".KVP(1)).AsList<ColleageDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.CollegeList = Colleage;

            var Level = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Level.AddRange("Sp_LeveleDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.LevelList = Level;
            var Track = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Track.AddRange("SpTrackDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.TrackList = Track;
            return View();
        }

        public ActionResult Search(int? TrackId)
        {

            var model = "SP_GetAllFirestBranchRegisterNotReceived".ExecuParamsSqlOrStored(false, "TrackId".KVP(TrackId)).AsList<FirestBranchRegisterNotReceivedVM>();
            return View(model.Where(a=>a.IsReceived != true));
        }

        public ActionResult Received(int id = 0)
        {
            var data = "SP_AddFirestBranchRegisterReceived".ExecuParamsSqlOrStored(false
             , "Id".KVP(id)).AsNonQuery();
            TempData["success"] = "<script>toastr.success(' تم  الصرف بنجاح');</script>";

            return RedirectToAction("Index1");

        }

        public ActionResult StudentReceived()
        {
            var Colleage = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Colleage.AddRange("Sp_ColleageDrop".ExecuParamsSqlOrStored(false, "BranchId".KVP(1)).AsList<ColleageDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.CollegeList = Colleage;

            var Level = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Level.AddRange("Sp_LeveleDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.LevelList = Level;
            var Track = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Track.AddRange("SpTrackDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.TrackList = Track;
            return View();
        }

        public ActionResult StudentReceivedSearch(int? TrackId)
        {

            var model = "SP_GetAllFirestBranchRegisterNotReceived".ExecuParamsSqlOrStored(false, "TrackId".KVP(TrackId)).AsList<FirestBranchRegisterNotReceivedVM>();
            return View(model.Where(a => a.IsReceived == true));
        }

    }
}