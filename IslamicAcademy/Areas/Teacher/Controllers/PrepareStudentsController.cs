﻿using IslamicAcademy.Core;
using IslamicAcademy.Core.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IslamicAcademy.Areas.Teacher.Controllers
{
    public class PrepareStudentsController : Controller
    {
        // GET: Teacher/PrepareStudents
        public ActionResult Index1()
        {
            var Level = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Level.AddRange("Sp_LeveleDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.LevelList = Level;
            return View();
        }

        public ActionResult Search(int? LevelId)
        {

            var model = "SP_GetAllSecondBranchRegister".ExecuParamsSqlOrStored(false).AsList<SecondBranchRegisterVM>();
            if(LevelId != null)
            {
                return View(model.Where(a=>a.LevelId==LevelId));

            }
            else
            return View(model);
        }

        public ActionResult AddPerparStudent(string Weak,string Day,Array CaseList, int StudentId)
        {
            foreach (var item in CaseList)
            {

          
          
                var data = "SP_AddStudentPrepare".ExecuParamsSqlOrStored(false, "StudentId".KVP(StudentId), "Weak".KVP(Weak), "Day".KVP(Day), "Attandce".KVP(item), "Id".KVP(0)).AsNonQuery();
            }
            
            return RedirectToAction("Index1");
        }

    }
}