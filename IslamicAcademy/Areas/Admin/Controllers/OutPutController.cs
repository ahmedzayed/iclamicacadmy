﻿using IslamicAcademy.Core;
using IslamicAcademy.Core.VM;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IslamicAcademy.Areas.Admin.Controllers
{
    public class OutPutController : Controller
    {
        // GET: Admin/OutPut
        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {

            var model = "SP_GetAllOutPut".ExecuParamsSqlOrStored(false).AsList<OutPutVM>();
            return View(model);
        }


        public ActionResult Create()
        {
            OutPutVM Obj = new OutPutVM();
            var LetterType = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            LetterType.AddRange("Sp_LetterTypeDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.LetterTypeList = LetterType;

            var OfficialAgencies = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            OfficialAgencies.AddRange("Sp_OfficialAgenciesDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.OfficialAgenciesList = OfficialAgencies;
            return View("~/Areas/Admin/Views/OutPut/Create.cshtml", Obj);
        }
        [HttpPost, ValidateInput(false)]
        public JsonResult Create(OutPutVM model)
        {

            model.Date = DateTime.Now;
           
            var data = "SP_AddOutPut".ExecuParamsSqlOrStored(false, "Count".KVP(model.Count), "Date".KVP(DateTime.Now.ToString()),
                "Details".KVP(model.Details), "LettersTypeId".KVP(model.LettersTypeId), "OfficialAgenciesId".KVP(model.OfficialAgenciesId)
               , "Id".KVP(model.Id)).AsNonQuery();

            var model1 = "SP_GetAllOutPut".ExecuParamsSqlOrStored(false).AsList<OutPutVM>();


            if (data != 0)
            {
                if (Request.Files.Count > 0)
                {
                    OutPutFileVM obj = new OutPutFileVM();
                     int i = 0;
                    foreach (var item in Request.Files)
                    {
                        var file = Request.Files[i];
                        i++;
                        if (file != null && file.ContentLength > 0)
                        {
                            var fileNameWithExtension = Path.GetFileName(file.FileName);
                            var fileNameOnly = Path.GetFileNameWithoutExtension(file.FileName);
                            string RootPath = Server.MapPath("~/Uploads/Images/");
                            if (!Directory.Exists(RootPath))
                            {
                                Directory.CreateDirectory(RootPath);
                            }
                            var unique = new Guid();
                            var path = Path.Combine(RootPath, fileNameOnly + unique + Path.GetExtension(file.FileName));
                            file.SaveAs(path);
                            obj.File = "Uploads/Images/" + fileNameOnly + unique + Path.GetExtension(file.FileName);

                        }
                        var data1 = "SP_AddOutPutFile".ExecuParamsSqlOrStored(false, "OutPutId".KVP(model1.LastOrDefault().Id), "File".KVP(obj.File), "Id".KVP(model.Id)).AsNonQuery();


                    }

                  



                }
                return Json("success," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);


            }
            else
                return Json("error," + "لم يتم الحفظ ", JsonRequestBehavior.AllowGet);

        }


        public ActionResult Edit(int id = 0)
        {
            var model = "SP_SelectOutPutById".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<OutPutVM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            OutPutVM obj = new OutPutVM();
            obj.Id = id;
            return PartialView("~/Areas/Admin/Views/OutPut/Delete.cshtml", obj);
        }


        [HttpGet]
        public ActionResult File(int id)
        {
            var model = "SP_GetAllOutPutFileById".ExecuParamsSqlOrStored(false,"OutPutId".KVP(id)).AsList<OutPutFileVM>();

            return PartialView("~/Areas/Admin/Views/OutPut/File.cshtml", model);
        }

        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteOutPut".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }
    }
}