﻿using IslamicAcademy.Core;
using IslamicAcademy.Core.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IslamicAcademy.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]

    public class ElmatoneController : Controller
    {
        // GET: Admin/Elmatone
        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {

            var model = "SP_GetAllElmatone".ExecuParamsSqlOrStored(false).AsList<ElmatoneVM>();
            return View(model);
        }


        public ActionResult Create()
        {
            ElmatoneFM Obj = new ElmatoneFM();
            var Matariale = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Matariale.AddRange("Sp_MatarialeDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.MatarialeList = Matariale;
            return PartialView("~/Areas/Admin/Views/Elmatone/Create.cshtml", Obj);
        }
        [HttpPost, ValidateInput(false)]
        public JsonResult Create(ElmatoneFM model)
        {
            var data = "SP_AddElmatone".ExecuParamsSqlOrStored(false, "Name".KVP(model.Name), "MatrialeId".KVP(model.MatrialeId)
               , "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0 && model.Id != 0)
            {
                return Json("successEdit," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else if (data != 0)
            {
                return Json("success," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحفظ ", JsonRequestBehavior.AllowGet);
        }


        public ActionResult Edit(int id = 0)
        {
            var Branchs = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            var Matariale = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Matariale.AddRange("Sp_MatarialeDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.MatarialeList = Matariale;
            var model = "SP_SelectElmatoneId".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<ElmatoneFM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            ElmatoneFM obj = new ElmatoneFM();
            obj.Id = id;
            return PartialView("~/Areas/Admin/Views/Elmatone/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteElmatone".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }
    }
}