﻿using IslamicAcademy.Core;
using IslamicAcademy.Core.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IslamicAcademy.Areas.Admin.Controllers
{
    public class ClassesController : Controller
    {
        // GET: Admin/Classes
        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {

            var model = "SP_GetAllClassess".ExecuParamsSqlOrStored(false).AsList<ClassesVM>();
            return View(model);
        }


        public ActionResult Create()
        {
            ClassesVM Obj = new ClassesVM();
            var EducationBody = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            EducationBody.AddRange("SpEducationBodyDrop".ExecuParamsSqlOrStored(false, "BarnchId".KVP(1)).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.EducationList = EducationBody;
            return PartialView("~/Areas/Admin/Views/Classes/Create.cshtml", Obj);
        }
        [HttpPost]
        public JsonResult Create(ClassesFM model)
        {
            var data = "SP_AddClasses".ExecuParamsSqlOrStored(false, "Name".KVP(model.Name), "From".KVP(model.From),"To".KVP(model.To), "EducationBodyId".KVP(model.EducationBodyId)
               , "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0 && model.Id != 0)
            {
                return Json("successEdit," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else if (data != 0)
            {
                return Json("success," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحفظ ", JsonRequestBehavior.AllowGet);
        }


        public ActionResult Edit(int id = 0)
        {
            var EducationBody = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            EducationBody.AddRange("SpEducationBodyDrop".ExecuParamsSqlOrStored(false,"BarnchId".KVP(1)).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.EducationList = EducationBody;
            var model = "SP_SelectClassesId".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<ClassesVM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            ClassesVM obj = new ClassesVM();
            obj.Id = id;
            return PartialView("~/Areas/Admin/Views/Classes/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteClasses".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }
    }
}