﻿using IslamicAcademy.Core;
using IslamicAcademy.Core.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IslamicAcademy.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]

    public class AdvantagesController : Controller
    {
        // GET: Admin/Advantages
        public ActionResult Index1(int? BranchId)
        {

            TempData["BranchId"] = BranchId;
            ViewBag.BranchId = BranchId;
            return View();
        }

        public ActionResult Search()
        {
            var BranchId =Convert.ToInt32( TempData["BranchId"]);

            var model = "SP_GetAllAdvantages".ExecuParamsSqlOrStored(false,"BranchId".KVP(BranchId)).AsList<AdvantagesVM>();
            return View(model);
        }


        public ActionResult Create(int BranchId)
        {
            ViewBag.BranchId = BranchId;
            AdvantagesFM Obj = new AdvantagesFM();
            //var Branchs = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            //Branchs.AddRange("Sp_BranchesDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            //{
            //    Text = s.Name,
            //    Value = s.Id.ToString(),
            //    Selected = s.Id == 0 ? true : false
            //}).ToList());
            //ViewBag.BranchList = Branchs;
            return PartialView("~/Areas/Admin/Views/Advantages/Create.cshtml", Obj);
        }
        [HttpPost, ValidateInput(false)]
        public JsonResult Create(AdvantagesFM model)
        {
            var data = "SP_AddAdvantages".ExecuParamsSqlOrStored(false, "Details".KVP(model.Details),"BranchId".KVP(model.BranchId)
               , "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0 && model.Id!=0)
            {
                return Json("successEdit," +model.BranchId+ "," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else if(data !=0)
            {
                return Json("success," + model.BranchId+","+  "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }

            else
                return Json("error," + "لم يتم الحفظ ", JsonRequestBehavior.AllowGet);
        }


        public ActionResult Edit(int id = 0)
        {
            var Branchs = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Branchs.AddRange("Sp_BranchesDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.BranchList = Branchs;
            var model = "SP_SelectAdvantagesId".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<AdvantagesFM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            AdvantagesFM obj = new AdvantagesFM();
            obj.Id = id;
            var model = "SP_SelectAdvantagesId".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<AdvantagesFM>();

            TempData["BranchId"] = model.FirstOrDefault().BranchId;
            return PartialView("~/Areas/Admin/Views/Advantages/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id )
        {
            var BranchId = TempData["BranchId"];
            var data = "SP_DeleteAdvantages".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + BranchId + "," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + BranchId + "," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }
    }
}