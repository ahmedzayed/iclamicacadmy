﻿using IslamicAcademy.Core;
using IslamicAcademy.Core.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IslamicAcademy.Areas.Admin.Controllers
{
    public class EducationBodyController : Controller
    {
        // GET: Admin/EducationBody
        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {

            var model = "SP_GetAllEducationBody".ExecuParamsSqlOrStored(false).AsList<EducationBodyVM>();
            return View(model);
        }


        public ActionResult Create()
        {
            EducationBodyVM Obj = new EducationBodyVM();
            var Branchs = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Branchs.AddRange("Sp_BranchesDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.BranchList = Branchs;
            return PartialView("~/Areas/Admin/Views/EducationBody/Create.cshtml", Obj);
        }
        [HttpPost, ValidateInput(false)]
        public JsonResult Create(EducationBodyVM model)
        {
            var data = "SP_AddEducationBody".ExecuParamsSqlOrStored(false, "Name".KVP(model.Name), "BranchId".KVP(model.BranchId)
               , "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0 && model.Id != 0)
            {
                return Json("successEdit," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else if (data != 0)
            {
                return Json("success," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحفظ ", JsonRequestBehavior.AllowGet);
        }


        public ActionResult Edit(int id = 0)
        {
            var Branchs = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Branchs.AddRange("Sp_BranchesDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.BranchList = Branchs;
            var model = "SP_SelectEducationBodyId".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<EducationBodyVM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            EducationBodyVM obj = new EducationBodyVM();
            obj.Id = id;
            return PartialView("~/Areas/Admin/Views/EducationBody/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteEducationBody".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }
    }
}