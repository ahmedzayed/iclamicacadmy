﻿using IslamicAcademy.Core;
using IslamicAcademy.Core.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IslamicAcademy.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]

    public class SysteamPrizeController : Controller
    {
        // GET: Admin/SysteamPrize
        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {

            var model = "SP_GetAllSysteamPrize".ExecuParamsSqlOrStored(false).AsList<SysteamPrizeVM>();
            return View(model);
        }


        public ActionResult Create()
        {
            SysteamPrizeVM Obj = new SysteamPrizeVM();
            return PartialView("~/Areas/Admin/Views/SysteamPrize/Create.cshtml", Obj);
        }
        [HttpPost, ValidateInput(false)]
        public JsonResult Create(SysteamPrizeVM model)
        {
            var data = "SP_AddSysteamPrize".ExecuParamsSqlOrStored(false, "Details".KVP(model.Details)
               , "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0 && model.Id != 0)
            {
                return Json("successEdit," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else if (data != 0)
            {
                return Json("success," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحفظ ", JsonRequestBehavior.AllowGet);
        }


        public ActionResult Edit(int id = 0)
        {
            var model = "SP_SelectSysteamPrizeId".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<SysteamPrizeVM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            SysteamPrizeVM obj = new SysteamPrizeVM();
            obj.Id = id;
            return PartialView("~/Areas/Admin/Views/SysteamPrize/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteSysteamPrize".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }
    }
}