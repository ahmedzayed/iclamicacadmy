﻿using IslamicAcademy.Core;
using IslamicAcademy.Core.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IslamicAcademy.Areas.Admin.Controllers
{
    public class OfficialAgenciesController : Controller
    {
        // GET: Admin/OfficialAgencies
        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {

            var model = "SP_GetAllOfficialAgencies".ExecuParamsSqlOrStored(false).AsList<SettingVM>();
            return View(model);
        }


        public ActionResult Create()
        {
            SettingVM Obj = new SettingVM();

            return PartialView("~/Areas/Admin/Views/OfficialAgencies/Create.cshtml", Obj);
        }
        [HttpPost, ValidateInput(false)]
        public JsonResult Create(SettingVM model)
        {
            var data = "SP_AddOfficialAgencies".ExecuParamsSqlOrStored(false, "Name".KVP(model.Name)
               , "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0 && model.Id != 0)
            {
                return Json("successEdit," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else if (data != 0)
            {
                return Json("success," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحفظ ", JsonRequestBehavior.AllowGet);
        }


        public ActionResult Edit(int id = 0)
        {

            var model = "SP_SelectOfficialAgenciesId".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<SettingVM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            SettingVM obj = new SettingVM();
            obj.Id = id;
            return PartialView("~/Areas/Admin/Views/OfficialAgencies/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteOfficialAgencies".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }
    }
}