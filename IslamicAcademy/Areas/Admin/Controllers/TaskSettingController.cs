﻿using IslamicAcademy.Core;
using IslamicAcademy.Core.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IslamicAcademy.Areas.Admin.Controllers
{
    public class TaskSettingController : Controller
    {
        // GET: Admin/TaskSetting
        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {

            var model = "SP_GetAllTaskSetting".ExecuParamsSqlOrStored(false).AsList<TaskSettingVM>();
            return View(model);
        }


        public ActionResult Create()
        {
            TaskSettingVM Obj = new TaskSettingVM();
            var TaskType = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            TaskType.AddRange("Sp_TaskTypeDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.TaskTypeList = TaskType;
            return PartialView("~/Areas/Admin/Views/TaskSetting/Create.cshtml", Obj);
        }
        [HttpPost, ValidateInput(false)]
        public JsonResult Create(TaskSettingVM model)
        {
            var data = "SP_AddTaskSetting".ExecuParamsSqlOrStored(false, "Name".KVP(model.Name), "Nots".KVP(model.Details),"Date".KVP(model.Date), "TaskTypeId".KVP(model.TaskTypeId)
               , "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0 && model.Id != 0)
            {
                return Json("successEdit," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else if (data != 0)
            {
                return Json("success," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحفظ ", JsonRequestBehavior.AllowGet);
        }


        public ActionResult Edit(int id = 0)
        {
            var TaskType = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            TaskType.AddRange("Sp_TaskTypeDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.TaskTypeList = TaskType;
            var model = "SP_SelectTaskSettingId".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<TaskSettingVM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            TaskSettingVM obj = new TaskSettingVM();
            obj.Id = id;
            return PartialView("~/Areas/Admin/Views/TaskSetting/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteTaskSetting".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }
    }
}