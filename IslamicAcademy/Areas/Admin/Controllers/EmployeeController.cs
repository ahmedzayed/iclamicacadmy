﻿using IslamicAcademy.Core;
using IslamicAcademy.Core.VM;
using IslamicAcademy.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IslamicAcademy.Areas.Admin.Controllers
{
    public class EmployeeController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        // GET: Admin/Employee
        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {

            var model = "SP_GetAllEmployee".ExecuParamsSqlOrStored(false).AsList<EmployeeVM>();
            return View(model);
        }


        public ActionResult Create()
        {
            EmployeeFM Obj = new EmployeeFM();
            var nationalityList = new List<SelectListItem>()
            {
                new SelectListItem{ Text = "--اختر--", Value = "0" }
            };
            nationalityList.AddRange("SP_GetAllNationality".ExecuParamsSqlOrStored(false).AsList<NationalityVM>()
                .Select(s => new SelectListItem
                {
                    Text = s.Name,
                    Value = s.Id.ToString(),
                    Selected = s.Id == 0 ? true : false
                }));
            var EmployeeTypeList = new List<SelectListItem>()
            {
                new SelectListItem{ Text = "--اختر--", Value = "0" }
            };
            EmployeeTypeList.AddRange("SP_GetAllEmployeeTypes".ExecuParamsSqlOrStored(false).AsList<EmployeeTypeVM>()
                .Select(s => new SelectListItem
                {
                    Text = s.TypeName,
                    Value = s.ID.ToString(),
                    Selected = s.ID == 0 ? true : false
                }));
            var IdentitifcationTypeList = new List<SelectListItem>()
            {
                new SelectListItem{ Text = "--اختر--", Value = "0" }
            };
            IdentitifcationTypeList.AddRange("SP_GetAllIdintitficationsType".ExecuParamsSqlOrStored(false).AsList<EmployeeTypeVM>()
                .Select(s => new SelectListItem
                {
                    Text = s.TypeName,
                    Value = s.ID.ToString(),
                    Selected = s.ID == 0 ? true : false
                }));

            ViewBag.NationalityList = nationalityList;
            ViewBag.EmployeeTypeList = EmployeeTypeList;
            ViewBag.IdentitifcationTypeList = IdentitifcationTypeList;
            return PartialView("~/Areas/Admin/Views/Employee/Create.cshtml", Obj);
        }
        [HttpPost, ValidateInput(false)]
        public async System.Threading.Tasks.Task<JsonResult> Create(EmployeeFM model)
        {
            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                if (file != null && file.ContentLength > 0)
                {
                    var fileNameWithExtension = Path.GetFileName(file.FileName);
                    var fileNameOnly = Path.GetFileNameWithoutExtension(file.FileName);
                    string RootPath = Server.MapPath("~/Uploads/Images/");
                    if (!Directory.Exists(RootPath))
                    {
                        Directory.CreateDirectory(RootPath);
                    }
                    var unique = new Guid();
                    var path = Path.Combine(RootPath, fileNameOnly + unique + Path.GetExtension(file.FileName));
                    file.SaveAs(path);
                    model.ImagePath = "Images/" + fileNameOnly + unique + Path.GetExtension(file.FileName);

                }
            }

            ApplicationDbContext context = new ApplicationDbContext();


            var user = new ApplicationUser { UserName = model.UserName, Email = model.Email };
            var result = await UserManager.CreateAsync(user, model.Password);
            
            var UserId = user.Id;

            UserManager.AddToRole(UserId, "Employee");
            context.SaveChanges();

            var data = "SP_AddEditEmployee".ExecuParamsSqlOrStored(false, "ID".KVP(Convert.ToInt32(model.ID)),
                "Emp_code".KVP(model.Emp_Code),
                "Emp_Name".KVP(model.Emp_Name),
                "TypeID".KVP(Convert.ToInt32( model.TypeID)),
                "NationaltyID".KVP(Convert.ToInt32( model.NationatyID)),
                "IdentitFicationID".KVP(Convert.ToInt32(model.IdentitFicationID)),
                "NantionlCode".KVP(model.NationalCode),
                "Phone1".KVP(model.Phone1),
                "Phone2".KVP(model.Phone2),
                "Email".KVP(model.Email),
                "DetaildAddress".KVP(model.DetaildAddress),
                "JobTitle".KVP(model.JobTitle),
                "ImagePath".KVP(model.ImagePath),"UserId".KVP(UserId)
                ).AsNonQuery();
            if (data != 0 )
            {
                return Json("successEdit," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else if (data != 0)
            {
                return Json("success," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحفظ ", JsonRequestBehavior.AllowGet);
        }


        public ActionResult Edit(int id = 0)
        {
            var nationalityList = new List<SelectListItem>()
            {
                new SelectListItem{ Text = "--اختر--", Value = "0" }
            };
            nationalityList.AddRange("SP_GetAllNationality".ExecuParamsSqlOrStored(false).AsList<NationalityVM>()
                .Select(s => new SelectListItem
                {
                    Text = s.Name,
                    Value = s.Id.ToString(),
                    Selected = s.Id == 0 ? true : false
                }));
            var EmployeeTypeList = new List<SelectListItem>()
            {
                new SelectListItem{ Text = "--اختر--", Value = "0" }
            };
            EmployeeTypeList.AddRange("SP_GetAllEmployeeTypes".ExecuParamsSqlOrStored(false).AsList<EmployeeTypeVM>()
                .Select(s => new SelectListItem
                {
                    Text = s.TypeName,
                    Value = s.ID.ToString(),
                    Selected = s.ID == 0 ? true : false
                }));
            var IdentitifcationTypeList = new List<SelectListItem>()
            {
                new SelectListItem{ Text = "--اختر--", Value = "0" }
            };
            IdentitifcationTypeList.AddRange("SP_GetAllIdintitficationsType".ExecuParamsSqlOrStored(false).AsList<EmployeeTypeVM>()
                .Select(s => new SelectListItem
                {
                    Text = s.TypeName,
                    Value = s.ID.ToString(),
                    Selected = s.ID == 0 ? true : false
                }));

            ViewBag.NationalityList = nationalityList;
            ViewBag.EmployeeTypeList = EmployeeTypeList;
            ViewBag.IdentitifcationTypeList = IdentitifcationTypeList;
            var model = "SP_SelectEmployeeId".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<EmployeeVM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return PartialView("Edit", model.FirstOrDefault());
        }


        
        [HttpPost, ValidateInput(false)]
        public ActionResult Edit(EmployeeFM model)
        {
            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                if (file != null && file.ContentLength > 0)
                {
                    var fileNameWithExtension = Path.GetFileName(file.FileName);
                    var fileNameOnly = Path.GetFileNameWithoutExtension(file.FileName);
                    string RootPath = Server.MapPath("~/Uploads/Images/");
                    if (!Directory.Exists(RootPath))
                    {
                        Directory.CreateDirectory(RootPath);
                    }
                    var unique = new Guid();
                    var path = Path.Combine(RootPath, fileNameOnly + unique + Path.GetExtension(file.FileName));
                    file.SaveAs(path);
                    model.ImagePath = "Images/" + fileNameOnly + unique + Path.GetExtension(file.FileName);

                }
            }

          

            var data = "SP_AddEditEmployee".ExecuParamsSqlOrStored(false, "ID".KVP(Convert.ToInt32(model.ID)),
                "Emp_code".KVP(model.Emp_Code),
                "Emp_Name".KVP(model.Emp_Name),
                "TypeID".KVP(Convert.ToInt32(model.TypeID)),
                "NationaltyID".KVP(Convert.ToInt32(model.NationatyID)),
                "IdentitFicationID".KVP(Convert.ToInt32(model.IdentitFicationID)),
                "NantionlCode".KVP(model.NationalCode),
                "Phone1".KVP(model.Phone1),
                "Phone2".KVP(model.Phone2),
                "Email".KVP(model.Email),
                "DetaildAddress".KVP(model.DetaildAddress),
                "ImagePath".KVP(model.ImagePath), "UserId".KVP("")
                ).AsNonQuery();
            if (data != 0)
            {
                return Json("successEdit," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else if (data != 0)
            {
                return Json("success," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحفظ ", JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            EmployeeVM obj = new EmployeeVM();
            obj.ID = id;
            return PartialView("~/Areas/Admin/Views/Employee/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteEmployee".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }
    }
}