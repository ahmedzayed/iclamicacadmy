﻿using IslamicAcademy.Core;
using IslamicAcademy.Core.VM;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IslamicAcademy.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]

    public class VisualiztionsController : Controller
    {
        // GET: Admin/Visualiztions
        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {

            var model = "SP_GetAlVisualiztions".ExecuParamsSqlOrStored(false).AsList<AcousticsVM>();
            return View(model);
        }


        public ActionResult Create()
        {
            AcousticsFM Obj = new AcousticsFM();

            var Matariale = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Matariale.AddRange("Sp_MatarialeDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.MatarialeList = Matariale;

            var Elmatone = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Elmatone.AddRange("Sp_ElmatoneDrop".ExecuParamsSqlOrStored(false, "MatarialeId".KVP(0)).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.ElmatoneList = Elmatone;
            return PartialView("~/Areas/Admin/Views/Visualiztions/Create.cshtml", Obj);
        }
        [HttpPost]
        public JsonResult Create(AcousticsFM model)
        {
            
            
            var data = "SP_AddVisualiztions".ExecuParamsSqlOrStored(false, "Tittel".KVP(model.Tittel), "Link".KVP(model.Link), "MatrialId".KVP(model.MatrialId), "ElmatoneId".KVP(model.ElmatoneId)
               , "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحفظ ", JsonRequestBehavior.AllowGet);

        }


        public ActionResult Edit(int id = 0)
        {

            LibraryReadFM Obj = new LibraryReadFM();
            var Matariale = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Matariale.AddRange("Sp_MatarialeDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.MatarialeList = Matariale;

            var Elmatone = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Elmatone.AddRange("Sp_ElmatoneDrop".ExecuParamsSqlOrStored(false, "MatarialeId".KVP(0)).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.ElmatoneList = Elmatone;
            var model = "SP_SelectVisualiztionsById".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<AcousticsFM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            AcousticsFM obj = new AcousticsFM();
            obj.Id = id;
            return PartialView("~/Areas/Admin/Views/Visualiztions/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteVisualiztions".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }
    }
}