﻿using IslamicAcademy.Core;
using IslamicAcademy.Core.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IslamicAcademy.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]

    public class ThirdBranchRegisterController : Controller
    {
        // GET: Admin/ThirdBranchRegister
        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {

            var model = "SP_GetAllThirdBranchRegister".ExecuParamsSqlOrStored(false).AsList<ThirdBranchRegisterVM>();
            return View(model);
        }


        public ActionResult Create()
        {
            ThirdBranchRegisterFM Obj = new ThirdBranchRegisterFM();


            var Nationality = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Nationality.AddRange("SpNationalityDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.NationalityList = Nationality;

            var Stage = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Stage.AddRange("SpStageDrop".ExecuParamsSqlOrStored(false, "BranchId".KVP(1)).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.StageList = Stage;

            var EducationBody = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            EducationBody.AddRange("SpEducationBodyDrop".ExecuParamsSqlOrStored(false, "BranchId".KVP(1)).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.EducationBodyList = EducationBody;
            return PartialView("~/Areas/Admin/Views/ThirdBranchRegister/Create.cshtml", Obj);
        }
        [HttpPost, ValidateInput(false)]
        public JsonResult Create(ThirdBranchRegisterFM model)
        {
            var data = "SP_AddThirdBranchRegister".ExecuParamsSqlOrStored(false, "Code".KVP(model.Code), "Tracks".KVP(model.Tracks)
                , "Case".KVP(model.Case), "EducationBodyId".KVP(model.EducationBodyId), "Email".KVP(model.Email), "Level".KVP(model.Level),
                "Name".KVP(model.Name), "NationalityId".KVP(model.NationalityId), "PhoneNumber".KVP(model.PhoneNumber),
                "StageId".KVP(model.StageId), "TelgrameNumber".KVP(model.TelgrameNumber), "WhatsNumber".KVP(model.WhatsNumber)
               , "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0 && model.Id != 0)
            {
                return Json("successEdit," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else if (data != 0)
            {
                return Json("success," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }

            else
                return Json("error," + "لم يتم الحفظ ", JsonRequestBehavior.AllowGet);
        }


        public ActionResult Edit(int id = 0)
        {
            var Nationality = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Nationality.AddRange("SpNationalityDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.NationalityList = Nationality;

            var Stage = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Stage.AddRange("SpStageDrop".ExecuParamsSqlOrStored(false, "BranchId".KVP(1)).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.StageList = Stage;

            var EducationBody = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            EducationBody.AddRange("SpEducationBodyDrop".ExecuParamsSqlOrStored(false, "BranchId".KVP(1)).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.EducationBodyList = EducationBody;
            var model = "SP_SelectThirdBranchRegisterId".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<ThirdBranchRegisterFM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            ThirdBranchRegisterFM obj = new ThirdBranchRegisterFM();
            obj.Id = id;
            return PartialView("~/Areas/Admin/Views/ThirdBranchRegister/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteThirdBranchRegister".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }
    }
}