﻿using IslamicAcademy.Core;
using IslamicAcademy.Core.VM;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IslamicAcademy.Areas.Admin.Controllers
{
    public class TaskesController : Controller
    {
        // GET: Admin/Taskes
        public ActionResult Index1()
        {

          
            return View();
        }

        public ActionResult Search()
        {
          
            var model = "SP_GetAllTaskes".ExecuParamsSqlOrStored(false).AsList<TaskesVM>();
            return View(model);
        }

        public ActionResult Create()
        {
            TaskesVM Obj = new TaskesVM();
            var TaskType = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            TaskType.AddRange("Sp_TaskTypeDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.TaskTypeList = TaskType;
            return PartialView("~/Areas/Admin/Views/Taskes/Create.cshtml", Obj);
        }
        [HttpPost, ValidateInput(false)]
        public JsonResult Create(EmpDetails obj, string Name ,int? Type,string Date, int? TaskTypeId,string TimeProposed,string Details)
        {
            var File = "";

            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                if (file != null && file.ContentLength > 0)
                {
                    var fileNameWithExtension = Path.GetFileName(file.FileName);
                    var fileNameOnly = Path.GetFileNameWithoutExtension(file.FileName);
                    string RootPath = Server.MapPath("~/Uploads/Images/");
                    if (!Directory.Exists(RootPath))
                    {
                        Directory.CreateDirectory(RootPath);
                    }
                    var unique = new Guid();
                    var path = Path.Combine(RootPath, fileNameOnly + unique + Path.GetExtension(file.FileName));
                    file.SaveAs(path);
                    File = "Uploads/Images/" + fileNameOnly + unique + Path.GetExtension(file.FileName);

                }



            }
            var data = "SP_AddTaskes".ExecuParamsSqlOrStored(false, "Name".KVP(Name),
                "Type".KVP(Type), "Date".KVP(Date), "TaskTypeId".KVP(TaskTypeId),"File".KVP(File), "TimeProposed".KVP(TimeProposed),"Details".KVP(Details)
               , "Id".KVP(0)).AsNonQuery();

            var model1 = "SP_GetAllTaskes".ExecuParamsSqlOrStored(false).AsList<TaskesVM>();
            int Task = model1.LastOrDefault().Id;
            if (data != 0)
            {
                foreach (var item in obj.EmpList)
                {
                 
                    var model = "SP_GetEmpByTaskId".ExecuParamsSqlOrStored(false).AsList<TaskeEmpDetailsVM>().Where(a => a.TaskId == Task && a.EmployeeId == Convert.ToInt32(item));
                    if (model.Count() == 0)
                    {
                        var data1 = "SP_AddEmpTaskes".ExecuParamsSqlOrStored(false, "TaskId".KVP(Task),
                         "EmpId".KVP(item)
                        , "Id".KVP(0)).AsNonQuery();
                    }

                }

                foreach (var item in obj.EmpShowList)
                {
                    var model = "SP_GetEmpShowByTaskId".ExecuParamsSqlOrStored(false).AsList<TaskeEmpDetailsVM>().Where(a => a.TaskId == Task && a.EmployeeId == Convert.ToInt32(item));
                    if (model.Count() == 0)
                    {
                        var data2 = "SP_AddEmpShowTaskes".ExecuParamsSqlOrStored(false, "TaskId".KVP(Task),
                         "EmpId".KVP(item)
                        , "Id".KVP(0)).AsNonQuery();
                    }

                }




                return Json("success," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحفظ ", JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetAllEmployee()
        {
            var model = "SP_GetAllEmp".ExecuParamsSqlOrStored(false).AsList<SettingVM>();

            return PartialView("_EmpList", model);
        }
        public ActionResult EmpShow()
        {
            var model = "SP_GetAllEmp".ExecuParamsSqlOrStored(false).AsList<SettingVM>();

            return PartialView("EmpShow", model);
        }

        [HttpGet]
        public ActionResult EmpDetails(int id)
        {
            var model = "SP_GetAllEmpDetailsByTaskId".ExecuParamsSqlOrStored(false,"TaskId".KVP(id)).AsList<TaskeEmpDetailsVM>();

            return PartialView("~/Areas/Admin/Views/Taskes/EmpDetails.cshtml", model);
        }

        [HttpGet]
        public ActionResult EmpShowOnly(int id)
        {
            var model = "SP_GetAllEmpShowDetailsByTaskId".ExecuParamsSqlOrStored(false, "TaskId".KVP(id)).AsList<TaskeEmpDetailsVM>();

            return PartialView("~/Areas/Admin/Views/Taskes/EmpShowOnly.cshtml", model);
        }


        public ActionResult RequestTime()
        {
            return View();
        }
        public ActionResult RequestTimeSearch()
        {
            var model = "SP_GetAllTaskes".ExecuParamsSqlOrStored(false).AsList<TaskesVM>().Where(a => a.RequestTime == true);
            return View(model);
        }
        public ActionResult TransfearedTasks()
        {
         var model=   "SP_GetAllTransferdTaskes".ExecuParamsSqlOrStored(false).AsList<TaskesVM>();
            return View(model);
        }
        [HttpPost]
        public ActionResult RequestTime(int TaskeId,string RequestTime)
        {
            var data = "SP_EditeRequestTime".ExecuParamsSqlOrStored(false, "RequestTime".KVP(RequestTime)

           , "TaskeId".KVP(TaskeId)).AsNonQuery();
            TempData["success"] = "<script>toastr.success(' تم الاضافه بنجاح');</script>";
            return RedirectToAction("Index1", "TaskEmp");
        }

    }
}