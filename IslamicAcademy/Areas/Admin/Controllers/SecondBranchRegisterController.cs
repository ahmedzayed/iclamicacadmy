﻿using IslamicAcademy.Core;
using IslamicAcademy.Core.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IslamicAcademy.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]

    public class SecondBranchRegisterController : Controller
    {
        // GET: Admin/SecondBranchRegister
        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {

            var model = "SP_GetAllSecondBranchRegister".ExecuParamsSqlOrStored(false).AsList<SecondBranchRegisterVM>();
            return View(model);
        }


        public ActionResult Create()
        {
            SecondBranchRegisterFM Obj = new SecondBranchRegisterFM();
           

            var Nationality = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Nationality.AddRange("SpNationalityDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.NationalityList = Nationality;

            var Stage = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Stage.AddRange("SpStageDrop".ExecuParamsSqlOrStored(false, "BranchId".KVP(1)).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.StageList = Stage;

            var EducationBody = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            EducationBody.AddRange("SpEducationBodyDrop".ExecuParamsSqlOrStored(false, "BranchId".KVP(1)).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.EducationBodyList = EducationBody;
            return PartialView("~/Areas/Admin/Views/SecondBranchRegister/Create.cshtml", Obj);
        }
        [HttpPost, ValidateInput(false)]
        public JsonResult Create(SecondBranchRegisterFM model)
        {
            var data = "SP_AddSecondBranchRegister".ExecuParamsSqlOrStored(false, "AcadmayDetails".KVP(model.AcadmayDetails), "Bachelorsdegree".KVP(model.Bachelorsdegree)
                , "Case".KVP(model.Case), "EducationBodyId".KVP(model.EducationBodyId), "Elmatone".KVP(model.Elmatone), "Email".KVP(model.Email), "Level".KVP(model.Level),
                "Name".KVP(model.Name), "NationalityId".KVP(model.NationalityId), "PhoneNumber".KVP(model.PhoneNumber), "STageEducationhigher".KVP(model.STageEducationhigher),
                "StageId".KVP(model.StageId), "TelegramNumber".KVP(model.TelegramNumber), "WhatsNumber".KVP(model.WhatsNumber)
               , "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0 && model.Id != 0)
            {
                return Json("successEdit," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else if (data != 0)
            {
                return Json("success," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }

            else
                return Json("error," + "لم يتم الحفظ ", JsonRequestBehavior.AllowGet);
        }


        public ActionResult Edit(int id = 0)
        {
            var Nationality = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Nationality.AddRange("SpNationalityDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.NationalityList = Nationality;

            var Stage = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Stage.AddRange("SpStageDrop".ExecuParamsSqlOrStored(false, "BranchId".KVP(1)).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.StageList = Stage;

            var EducationBody = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            EducationBody.AddRange("SpEducationBodyDrop".ExecuParamsSqlOrStored(false, "BranchId".KVP(1)).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.EducationBodyList = EducationBody;
            var model = "SP_SelectSecondBranchRegisterId".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<SecondBranchRegisterFM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            SecondBranchRegisterFM obj = new SecondBranchRegisterFM();
            obj.Id = id;
            return PartialView("~/Areas/Admin/Views/SecondBranchRegister/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteSecondBranchRegister".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }
    }
}