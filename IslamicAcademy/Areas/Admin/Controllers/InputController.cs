﻿using IslamicAcademy.Core;
using IslamicAcademy.Core.VM;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IslamicAcademy.Areas.Admin.Controllers
{
    public class InputController : Controller
    {
        // GET: Admin/Input
        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {

            var model = "SP_GetAllInput".ExecuParamsSqlOrStored(false).AsList<InputVM>();
            return View(model);
        }


        public ActionResult Create()
        {
            InputVM Obj = new InputVM();
            var LetterType = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            LetterType.AddRange("Sp_LetterTypeDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.LetterTypeList = LetterType;

            var OfficialAgencies = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            OfficialAgencies.AddRange("Sp_OfficialAgenciesDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.OfficialAgenciesList = OfficialAgencies;
            return View("~/Areas/Admin/Views/Input/Create.cshtml", Obj);
        }
        [HttpPost, ValidateInput(false)]
        public JsonResult Create(InputVM model)
        {

            model.Date = DateTime.Now;

            var data = "SP_AddInput".ExecuParamsSqlOrStored(false, "Count".KVP(model.Count), "Date".KVP(model.Date),
                "Details".KVP(model.Details), "LettersTypeId".KVP(model.LettersTypeId), "OfficialAgenciesId".KVP(model.OfficialAgenciesId)
               , "Id".KVP(model.Id)).AsNonQuery();

            var model1 = "SP_GetAllInput".ExecuParamsSqlOrStored(false).AsList<InputVM>();


            if (data != 0)
            {
                if (Request.Files.Count > 0)
                {
                    InputFileVM obj = new InputFileVM();
                    int i = 0;
                    foreach (var item in Request.Files)
                    {
                        var file = Request.Files[i];
                        i++;
                        if (file != null && file.ContentLength > 0)
                        {
                            var fileNameWithExtension = Path.GetFileName(file.FileName);
                            var fileNameOnly = Path.GetFileNameWithoutExtension(file.FileName);
                            string RootPath = Server.MapPath("~/Uploads/Images/");
                            if (!Directory.Exists(RootPath))
                            {
                                Directory.CreateDirectory(RootPath);
                            }
                            var unique = new Guid();
                            var path = Path.Combine(RootPath, fileNameOnly + unique + Path.GetExtension(file.FileName));
                            file.SaveAs(path);
                            obj.File = "Uploads/Images/" + fileNameOnly + unique + Path.GetExtension(file.FileName);

                        }
                        var data1 = "SP_AddInputFile".ExecuParamsSqlOrStored(false, "InPutId".KVP(model1.LastOrDefault().Id), "File".KVP(obj.File), "Id".KVP(model.Id)).AsNonQuery();


                    }





                }
                return Json("success," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);


            }
            else
                return Json("error," + "لم يتم الحفظ ", JsonRequestBehavior.AllowGet);

        }


        public ActionResult Edit(int id = 0)
        {
            var model = "SP_SelectInputById".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<InputVM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            InputVM obj = new InputVM();
            obj.Id = id;
            return PartialView("~/Areas/Admin/Views/Input/Delete.cshtml", obj);
        }


        [HttpGet]
        public ActionResult File(int id)
        {
            var model = "SP_GetAllInputFileById".ExecuParamsSqlOrStored(false, "InputId".KVP(id)).AsList<InputFileVM>();

            return PartialView("~/Areas/Admin/Views/Input/File.cshtml", model);
        }

        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteInput".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }
    }
}