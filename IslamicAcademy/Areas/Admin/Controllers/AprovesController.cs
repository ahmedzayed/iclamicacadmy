﻿using IslamicAcademy.Core;
using IslamicAcademy.Core.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IslamicAcademy.Areas.Admin.Controllers
{
    public class AprovesController : Controller
    {
        // GET: Admin/Aproves
        public ActionResult GardsAproved()
        {
            var Colleage = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Colleage.AddRange("Sp_ColleageDrop".ExecuParamsSqlOrStored(false, "BranchId".KVP(1)).AsList<ColleageDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.CollegeList = Colleage;

            var Level = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Level.AddRange("Sp_LeveleDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.LevelList = Level;
            var Track = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Track.AddRange("SpTrackDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.TrackList = Track;

            return View();
        }

        public ActionResult GardsList(int? TrackId)

        {
            if (TrackId != null)
            {
                var model = "SP_GetAllGardsApproved".ExecuParamsSqlOrStored(false).AsList<GardsApprovedVM>().OrderBy(a => a.StudentId).Where(a => a.IsAproved == false);
                var Data1 = model.Where(z => z.TrackId == TrackId).ToList().GroupBy(a => a.StudentId);
                List<GardsApprovedVM> ss = new List<GardsApprovedVM>();
                foreach (var item in Data1)
                {
                    ss.Add(item.FirstOrDefault());

                }
                return PartialView(ss);
            }
            else
            {
                var model1 = "SP_GetAllGardsApproved".ExecuParamsSqlOrStored(false).AsList<GardsApprovedVM>().OrderBy(a => a.StudentId).Where(a => a.IsAproved == false);
                var Data1 = model1.ToList().GroupBy(a => a.StudentId);
                List<GardsApprovedVM> ss1 = new List<GardsApprovedVM>();
                foreach (var item in Data1)
                {
                    ss1.Add(item.FirstOrDefault());

                }
                return PartialView(ss1);
            }

        }
        public ActionResult Approved(int Id)
        {

            var model = "SP_GetAllGardsApproved".ExecuParamsSqlOrStored(false).AsList<GardsApprovedVM>();
            var Data1 = model.Where(z => z.StudentId == Id).ToList();
            foreach (var item in Data1)
            {
                var data = "SP_ApprovedStudentGards".ExecuParamsSqlOrStored(false, "Id".KVP(item.StudentId)).AsNonQuery();
                
            }
            TempData["success"] = "<script>toastr.success(' تم الاضافه بنجاح');</script>";

            return RedirectToAction("GardsAproved");
        }
        [HttpGet]
        public ActionResult Edit(int id)
        {
            var model = "SP_GetAllGardsApproved".ExecuParamsSqlOrStored(false).AsList<GardsApprovedVM>();
            var Data1 = model.Where(z => z.StudentId == id).ToList();
            ViewBag.StudentId = id;
            return PartialView("~/Areas/Admin/Views/Aproves/GardsDetailsByStudent.cshtml", Data1);
        }

        //public ActionResult EvulationApproved()
        //{
        //    return View();
        //}

        //public ActionResult EvulationList()
        //{
        //    var model = "SP_GetAllEvulationApproved".ExecuParamsSqlOrStored(false).AsList<EvulationStudentApprovedVM>();
        //    return PartialView(model.Where(a => a.IsApproved == false));
        //}


        //public ActionResult EVApproved(int Id)
        //{
        //    var data = "SP_ApprovedStudentEv".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
        //    if (data != 0)
        //    {
        //        TempData["success"] = "<script>toastr.success(' تم الاضافه بنجاح');</script>";

        //    }
        //    return RedirectToAction("GardsAproved");
        //}


    }
}