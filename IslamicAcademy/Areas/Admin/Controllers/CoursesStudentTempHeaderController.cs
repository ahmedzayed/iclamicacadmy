﻿using IslamicAcademy.Core;
using IslamicAcademy.Core.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IslamicAcademy.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]

    public class CoursesStudentTempHeaderController : Controller
    {
        // GET: Admin/CoursesStudentTempHeader
        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {

            var model = "SP_GetAllTempHeader".ExecuParamsSqlOrStored(false).AsList<TempHeaderVM>().Where(a=>a.CoursesCase==false).ToList();
            return View(model);
        }


        public ActionResult Create()
        {
            TempHeaderVM Obj = new TempHeaderVM();
         
            return PartialView("~/Areas/Admin/Views/Elmatone/Create.cshtml", Obj);
        }
        [HttpPost, ValidateInput(false)]
        public JsonResult Create(ElmatoneFM model)
        {
            var data = "SP_AddElmatone".ExecuParamsSqlOrStored(false, "Name".KVP(model.Name), "MatrialeId".KVP(model.MatrialeId)
               , "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0 && model.Id != 0)
            {
                return Json("successEdit," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else if (data != 0)
            {
                return Json("success," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحفظ ", JsonRequestBehavior.AllowGet);
        }


        public ActionResult Edit(int id = 0)
        {
            var Branchs = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            var Matariale = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Matariale.AddRange("Sp_MatarialeDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.MatarialeList = Matariale;
            var model = "SP_SelectElmatoneId".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<ElmatoneFM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }


        [HttpGet]
        public ActionResult CoursesList(int id)
        {
            var model = "SP_GetAllTempHeaderDetails".ExecuParamsSqlOrStored(false,"HeaderId".KVP(id)).AsList<TempHeaderDetailsVM>();

            return PartialView("~/Areas/Admin/Views/CoursesStudentTempHeader/CoursesList.cshtml", model);
        }


      


       public ActionResult  AcciptChange(int StudentId,int Id)
        {
            var data = "SP_AddTempHeader".ExecuParamsSqlOrStored(false, "StudentId".KVP(StudentId)
           , "Id".KVP(Id)).AsNonQuery();

            var DeleteOldCources = "DeleteOldCourses".ExecuParamsSqlOrStored(false, "StudentId".KVP(StudentId)
          ).AsNonQuery();


            var model = "SP_GetAllTempHeaderDetails".ExecuParamsSqlOrStored(false, "HeaderId".KVP(Id)).AsList<TempHeaderDetailsVM>();

            foreach (var item in model)
            {
                var data2 = "SP_AddStudentCoursies".ExecuParamsSqlOrStored(false, "StudentId".KVP(StudentId),
                 "ElmatoneId".KVP(item.ElmatoneId)

           , "Id".KVP(0)).AsNonQuery();
            }
            TempData["success"] = "<script>toastr.success(' تم قبول التعديل ');</script>";

            return RedirectToAction("Index1");
        }
    }
}