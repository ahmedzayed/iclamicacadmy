﻿using IslamicAcademy.Core;
using IslamicAcademy.Core.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IslamicAcademy.Areas.Admin.Controllers
{
    public class DoctrinesDetailsController : Controller
    {
        // GET: Admin/DoctrinesDetails
        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {

            var model = "SP_GetAllDoctrinesDetailss".ExecuParamsSqlOrStored(false).AsList<DoctrinesDetailsVM>();
            return View(model);
        }


        public ActionResult Create()
        {
            DoctrinesDetailsVM Obj = new DoctrinesDetailsVM();
            var Doctrines = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Doctrines.AddRange("Sp_DoctrinesDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.DoctrinesList = Doctrines;
            return PartialView("~/Areas/Admin/Views/DoctrinesDetails/Create.cshtml", Obj);
        }
        [HttpPost, ValidateInput(false)]
        public JsonResult Create(DoctrinesDetailsVM model)
        {
            var data = "SP_AddDoctrinesDetailss".ExecuParamsSqlOrStored(false, "Name".KVP(model.Name), "DoctrinesId".KVP(model.DoctrinesId)
               , "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0 && model.Id != 0)
            {
                return Json("successEdit," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else if (data != 0)
            {
                return Json("success," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحفظ ", JsonRequestBehavior.AllowGet);
        }


        public ActionResult Edit(int id = 0)
        {
            var Doctrines = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Doctrines.AddRange("Sp_DoctrinesDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.DoctrinesList = Doctrines;
            var model = "SP_SelectDoctrinesDetailssId".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<DoctrinesDetailsVM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            DoctrinesDetailsVM obj = new DoctrinesDetailsVM();
            obj.Id = id;
            return PartialView("~/Areas/Admin/Views/DoctrinesDetails/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteDoctrinesDetailss".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }
    }
}