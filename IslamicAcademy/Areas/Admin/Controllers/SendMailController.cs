﻿using IslamicAcademy.Core;
using IslamicAcademy.Core.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace IslamicAcademy.Areas.Admin.Controllers
{
    public class SendMailController : Controller
    {
        // GET: Admin/SendMail

        public void SendEmail(string EmailFrom, string Password, string EmailMessage, string EmailTo, string EmailTitile, string EmaileSTMP)
        {

            string emailFrom = EmailFrom;
            string password = Password;
            string emailTo = EmailTo;
            string body = EmailMessage;
            string subject = EmailTitile;

            using (MailMessage mail = new MailMessage())
            {
                mail.From = new MailAddress(emailFrom);
                mail.To.Add(EmailTo);
                mail.Subject = subject;

                mail.Body = body;
                mail.IsBodyHtml = true;



                SmtpClient smtpClient = new SmtpClient("smtp.gmail.com", Convert.ToInt32(587));
                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(emailFrom, Password);
                smtpClient.Credentials = credentials;
                smtpClient.EnableSsl = true;
                smtpClient.Send(mail);

            }
        }

        public ActionResult Index1()
        {
            var Colleage = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Colleage.AddRange("Sp_ColleageDrop".ExecuParamsSqlOrStored(false, "BranchId".KVP(1)).AsList<ColleageDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.CollegeList = Colleage;

            var Level = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Level.AddRange("Sp_LeveleDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.LevelList = Level;
            var Track = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Track.AddRange("SpTrackDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.TrackList = Track;
            return View();
        }

        public ActionResult Search(int? TrackId)
        {

            var model = "SP_GetAllFirestBranchRegister".ExecuParamsSqlOrStored(false, "TrackId".KVP(TrackId)).AsList<FirestBranchRegisterVM>();
            return View(model);
        }
        [HttpPost, ValidateInput(false)]

        public ActionResult SendMail(Array EmpList, string Details)
        {

            foreach (var item in EmpList)
            {
                SendEmail("codlandit@gmail.com", "01061538163", "UserName IS:" + "تم تسجيل دخولك لاكادمية متون بنجاح" + Details, item.ToString(), "Message", "smtp.gmail.com");


            }
            return Json("Success", JsonRequestBehavior.AllowGet);
        }

    }
}