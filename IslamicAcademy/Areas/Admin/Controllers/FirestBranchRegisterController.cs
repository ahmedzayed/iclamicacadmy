﻿using IslamicAcademy.Core;
using IslamicAcademy.Core.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IslamicAcademy.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]

    public class FirestBranchRegisterController : Controller
    {
        // GET: Admin/FirestBranchRegister
        public ActionResult Index1()
        {
            var Colleage = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Colleage.AddRange("Sp_ColleageDrop".ExecuParamsSqlOrStored(false, "BranchId".KVP(1)).AsList<ColleageDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.CollegeList = Colleage;

            var Level = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Level.AddRange("Sp_LeveleDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.LevelList = Level;
            var Track = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Track.AddRange("SpTrackDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.TrackList = Track;
            return View();
        }

        public ActionResult Search(int? TrackId)
        {

            var model = "SP_GetAllFirestBranchRegister".ExecuParamsSqlOrStored(false,"TrackId".KVP(TrackId)).AsList<FirestBranchRegisterVM>();
            return View(model.Where(a=>a.IsPostponed !=true));
        }

        public ActionResult Approved(int id = 0)
        {
            var data = "SP_AddFirestBranchRegisterApproved".ExecuParamsSqlOrStored(false
             , "Id".KVP(id)).AsNonQuery();
            return RedirectToAction("Index1");

        }

        public ActionResult Create()
        {
            FirestBranchRegisterFM Obj = new FirestBranchRegisterFM();
            var Colleage = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Colleage.AddRange("Sp_ColleageDrop".ExecuParamsSqlOrStored(false,"BranchId".KVP(1)).AsList<ColleageDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.ColleageList = Colleage;

            var Nationality = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Nationality.AddRange("SpNationalityDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.NationalityList = Nationality;

            var Stage = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Stage.AddRange("SpStageDrop".ExecuParamsSqlOrStored(false, "BranchId".KVP(1)).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.StageList = Stage;
            return PartialView("~/Areas/Admin/Views/FirestBranchRegister/Create.cshtml", Obj);
        }
        [HttpPost, ValidateInput(false)]
        public JsonResult Create(FirestBranchRegisterFM model)
        {
            var data = "SP_AddFirestBranchRegister".ExecuParamsSqlOrStored(false, "Case".KVP(model.Case), "Code".KVP(model.Code)
                , "ColleageId".KVP(model.ColleageId), "EducationBody".KVP(model.EducationBodyId), "Email".KVP(model.Email), "Level".KVP(model.LevelId), "Name".KVP(model.Name),
                "NationalityId".KVP(model.NationalityId), "Phonenumber".KVP(model.Phonenumber), "StageId".KVP(model.StageId), "TelgramNumber".KVP(model.TelgramNumber),
                "WhatsNumber".KVP(model.WhatsNumber)
               , "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0 && model.Id != 0)
            {
                return Json("successEdit," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else if (data != 0)
            {
                return Json("success," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }

            else
                return Json("error," + "لم يتم الحفظ ", JsonRequestBehavior.AllowGet);
        }


        public ActionResult Edit(int id = 0)
        {
            var Level = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Level.AddRange("Sp_LeveleDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.LevelList = Level;
            var Colleage = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Colleage.AddRange("Sp_ColleageDrop".ExecuParamsSqlOrStored(false, "BranchId".KVP(1)).AsList<ColleageDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,

                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false


            }).ToList());
            ViewBag.ColleageList = Colleage;

            var Nationality = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Nationality.AddRange("SpNationalityDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.NationalityList = Nationality;

            var Stage = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Stage.AddRange("SpStageDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.StageList = Stage;

            var EducationBody = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            EducationBody.AddRange("SpEducationBodyDrop".ExecuParamsSqlOrStored(false, "BranchId".KVP(1)).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.EducationBodyList = EducationBody;

            var TrackList = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            TrackList.AddRange("SpTrackDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.TrackList = TrackList;
            var model = "SP_SelectFirestBranchRegisterId".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<FirestBranchRegisterFM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            FirestBranchRegisterFM obj = new FirestBranchRegisterFM();
            obj.Id = id;
            return PartialView("~/Areas/Admin/Views/FirestBranchRegister/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteFirestBranchRegister".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult Postponed(int id)
        {
            FirestBranchRegisterFM obj = new FirestBranchRegisterFM();
            obj.Id = id;
            return PartialView("~/Areas/Admin/Views/FirestBranchRegister/Postponed.cshtml", obj);
        }

        [HttpPost]
        public JsonResult PostponedRow(int Id,string DateTime)
        {
            var data = "SP_PostponedFirestBranchRegister".ExecuParamsSqlOrStored(false,"DateTime".KVP(DateTime), "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }



        public ActionResult StudentPostPoned()
        {
            var Colleage = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Colleage.AddRange("Sp_ColleageDrop".ExecuParamsSqlOrStored(false, "BranchId".KVP(1)).AsList<ColleageDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.CollegeList = Colleage;

            var Level = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Level.AddRange("Sp_LeveleDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.LevelList = Level;
            var Track = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Track.AddRange("SpTrackDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.TrackList = Track;
            return View();
        }

        public ActionResult StudentPostPonedSearch(int? TrackId)
        {

            var model = "SP_GetAllFirestBranchRegister".ExecuParamsSqlOrStored(false, "TrackId".KVP(TrackId)).AsList<FirestBranchRegisterVM>();
            return View(model.Where(a=>a.IsPostponed==true));
        }
    }
}