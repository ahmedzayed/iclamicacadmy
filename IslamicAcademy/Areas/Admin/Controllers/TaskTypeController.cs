﻿using IslamicAcademy.Core;
using IslamicAcademy.Core.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IslamicAcademy.Areas.Admin.Controllers
{
    public class TaskTypeController : Controller
    {
        // GET: Admin/TaskType
        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {

            var model = "SP_GetAllTask".ExecuParamsSqlOrStored(false).AsList<TaskTypeVM>();
            return View(model);
        }


        public ActionResult Create()
        {
            TaskTypeVM Obj = new TaskTypeVM();
           
            return PartialView("~/Areas/Admin/Views/TaskType/Create.cshtml", Obj);
        }
        [HttpPost]
        public JsonResult Create(TaskTypeVM model)
        {
            var data = "SP_AddTaskType".ExecuParamsSqlOrStored(false, "Name".KVP(model.Name)
               , "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0 && model.Id != 0)
            {
                return Json("successEdit," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else if (data != 0)
            {
                return Json("success," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحفظ ", JsonRequestBehavior.AllowGet);
        }


        public ActionResult Edit(int id = 0)
        {
           
            var model = "SP_SelectTaskTypeId".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<TaskTypeVM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return PartialView("~/Areas/Admin/Views/TaskType/Create.cshtml", model.FirstOrDefault());
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            TaskTypeVM obj = new TaskTypeVM();
            obj.Id = id;
            return PartialView("~/Areas/Admin/Views/TaskType/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteTaskType".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }
    }
}