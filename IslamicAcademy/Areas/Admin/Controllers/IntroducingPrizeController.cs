﻿using IslamicAcademy.Core;
using IslamicAcademy.Core.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IslamicAcademy.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]

    public class IntroducingPrizeController : Controller
    {
        // GET: Admin/IntroducingPrize
        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {

            var model = "SP_GetAllIntroducingPrize".ExecuParamsSqlOrStored(false).AsList<IntroducingPrizeVM>();
            return View(model);
        }


        public ActionResult Create()
        {
            IntroducingPrizeVM Obj = new IntroducingPrizeVM();
            return PartialView("~/Areas/Admin/Views/IntroducingPrize/Create.cshtml", Obj);
        }
        [HttpPost, ValidateInput(false)]
        public JsonResult Create(IntroducingPrizeVM model)
        {
            var data = "SP_AddIntroducingPrize".ExecuParamsSqlOrStored(false, "Details".KVP(model.Details)
               , "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0 && model.Id != 0)
            {
                return Json("successEdit," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else if (data != 0)
            {
                return Json("success," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحفظ ", JsonRequestBehavior.AllowGet);
        }


        public ActionResult Edit(int id = 0)
        {
            var model = "SP_SelectIntroducingPrizeId".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<IntroducingPrizeVM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            IntroducingPrizeVM obj = new IntroducingPrizeVM();
            obj.Id = id;
            return PartialView("~/Areas/Admin/Views/IntroducingPrize/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteIntroducingPrize".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }
    }
}