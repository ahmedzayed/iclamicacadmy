﻿using IslamicAcademy.Core;
using IslamicAcademy.Core.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IslamicAcademy.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]

    public class TimeTableController : Controller
    {
        // GET: Admin/TimeTable
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Search()
        {
            var Elmatone = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Elmatone.AddRange("Sp_ElmatoneDrop".ExecuParamsSqlOrStored(false, "MatarialeId".KVP(0)).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.ElmatoneList = Elmatone;
            var model = "SP_GetAllTimeTable".ExecuParamsSqlOrStored(false).AsList<TimeTableVM>();

            return PartialView(model);
        }
        [HttpPost]
        public ActionResult Edite(int? Id, int? ElmatoneId)
        {


            var data = "SP_EditTimeTable".ExecuParamsSqlOrStored(false,
                "ElmatoneId".KVP(ElmatoneId)

               , "Id".KVP(Id)).AsNonQuery();
            return RedirectToAction("Index");
        }
    }
}