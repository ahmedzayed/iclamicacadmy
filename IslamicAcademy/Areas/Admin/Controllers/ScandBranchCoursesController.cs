﻿using IslamicAcademy.Core;
using IslamicAcademy.Core.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IslamicAcademy.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]

    public class ScandBranchCoursesController : Controller
    {
        // GET: Admin/ScandBranchScandBranchCourses
        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {

            var model = "SP_GetAllScandBranchCourses".ExecuParamsSqlOrStored(false).AsList<ScandBranchCoursesVM>();
            return View(model);
        }


        public ActionResult Create()
        {
            ScandBranchCoursesFM Obj = new ScandBranchCoursesFM();
           

          
            var Matariale = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Matariale.AddRange("Sp_MatarialeDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.MatarialeList = Matariale;

            var Elmatone = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Elmatone.AddRange("Sp_ElmatoneDrop".ExecuParamsSqlOrStored(false, "MatarialeId".KVP(0)).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.ElmatoneList = Elmatone;

            return PartialView("~/Areas/Admin/Views/ScandBranchCourses/Create.cshtml", Obj);
        }
        [HttpPost, ValidateInput(false)]
        public JsonResult Create(ScandBranchCoursesFM model)
        {


            var data = "SP_AddScandBranchCourses".ExecuParamsSqlOrStored(false,
                "MatrialId".KVP(model.MatrialId), "ElmatoneId".KVP(model.ElmatoneId)
               
               , "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0 && model.Id != 0)
            {
                return Json("successEdit," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else if (data != 0)
            {
                return Json("success," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحفظ ", JsonRequestBehavior.AllowGet);
        }


        public ActionResult Edit(int id = 0)
        {
           
            var Matariale = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Matariale.AddRange("Sp_MatarialeDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.MatarialeList = Matariale;

            var Elmatone = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Elmatone.AddRange("Sp_ElmatoneDrop".ExecuParamsSqlOrStored(false, "MatarialeId".KVP(0)).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.ElmatoneList = Elmatone;
            var model = "SP_SelectScandBranchCoursesId".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<ScandBranchCoursesFM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            ScandBranchCoursesFM obj = new ScandBranchCoursesFM();
            obj.Id = id;
            return PartialView("~/Areas/Admin/Views/ScandBranchCourses/Delete.cshtml", obj);
        }

        [HttpGet]
        public ActionResult GetCollageList(int id)
        {
            var Colleage = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Colleage.AddRange("Sp_ColleageDrop".ExecuParamsSqlOrStored(false, "BranchId".KVP(id)).AsList<ColleageDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.CollegeList = Colleage;
            //var j = context.EducationalGrades.Where(e => e.StageId == id && e.IsDeleted == false).Select(g => new { g.NameAr, g.Id }).ToList();
            return Json(Colleage, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetElmotenList(int id)
        {
            var Elmatone = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Elmatone.AddRange("Sp_ElmatoneDrop".ExecuParamsSqlOrStored(false, "MatarialeId".KVP(id)).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.ElmatoneList = Elmatone;
            //var j = context.EducationalGrades.Where(e => e.StageId == id && e.IsDeleted == false).Select(g => new { g.NameAr, g.Id }).ToList();
            return Json(Elmatone, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteScandBranchCourses".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }
    }
}