﻿using IslamicAcademy.Core;
using IslamicAcademy.Core.VM;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IslamicAcademy.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]

    public class CoursesController : Controller
    {
        // GET: Admin/Courses
        public ActionResult Index1()
        {
            var Colleage = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Colleage.AddRange("Sp_ColleageDrop".ExecuParamsSqlOrStored(false, "BranchId".KVP(1)).AsList<ColleageDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.CollegeList = Colleage;

            var Level = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Level.AddRange("Sp_LeveleDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.LevelList = Level;
            var Track = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Track.AddRange("SpTrackDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.TrackList = Track;

            return View();
        }

        public ActionResult Search(int? TrackId)
        {
            
            var model = "SP_GetAllCourses".ExecuParamsSqlOrStored(false,"TrackId".KVP(TrackId)).AsList<CoursesVM>();
            return View(model);
        }

        public ActionResult ElmatoneList()
        {
            var model = "SP_GetAllElmatoneCoursesList".ExecuParamsSqlOrStored(false).AsList<ElmatoneCoursesListVM>();
            return PartialView(model);
        }

        public ActionResult DoctrinesList()
        {
            var model = "SP_GetAllDoctrinesDetailss".ExecuParamsSqlOrStored(false).AsList<DoctrinesDetailsVM>();
            return View(model);
        }

        public ActionResult Create(int? BranchId,int? CollageId,int? LevelId,int? TrackId)
        {
            CoursesFM Obj = new CoursesFM();
            var Branchs = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Branchs.AddRange("Sp_BranchesDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == BranchId ? true : false
            }).ToList());
            ViewBag.BranchList = Branchs;

            var Colleage = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Colleage.AddRange("Sp_ColleageDrop".ExecuParamsSqlOrStored(false,"BranchId".KVP(1)).AsList<ColleageDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == CollageId ? true : false
            }).ToList());
            ViewBag.CollegeList = Colleage;
            var Matariale = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Matariale.AddRange("Sp_MatarialeDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.MatarialeList = Matariale;

            var Elmatone = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Elmatone.AddRange("Sp_ElmatoneDrop".ExecuParamsSqlOrStored(false,"MatarialeId".KVP(0)).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.ElmatoneList = Elmatone;


            var Level = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Level.AddRange("Sp_LeveleDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == LevelId ? true : false
            }).ToList());
            ViewBag.LevelList = Level;
            var Track = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Track.AddRange("SpTrackDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == TrackId ? true : false
            }).ToList());
            ViewBag.TrackList = Track;

            return View("~/Areas/Admin/Views/Courses/Create.cshtml", Obj);
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult Create(int ElmatoneId, int? BranchId,int? ColleageId, int? LevelId,int TrackId, string Weak ,string Year,string Couresess,string Nots)
        {
            var model = "SP_GetAllCourses".ExecuParamsSqlOrStored(false, "TrackId".KVP(TrackId)).AsList<CoursesVM>();
            var OldData = model.Where(a => a.TrackId == TrackId && a.ElmatoneId == ElmatoneId).ToList();
            if (OldData.Count > 0)
            {
                return Json("Erro", JsonRequestBehavior.AllowGet);

            }
            else
            {

                var data = "SP_AddCoursesNew".ExecuParamsSqlOrStored(false, "BranchId".KVP(BranchId), "ColleageId".KVP(ColleageId),
                     "ElmatoneId".KVP(ElmatoneId), "LevelId".KVP(LevelId), "TrackId".KVP(TrackId)
                    , "Weak".KVP(Weak), "Year".KVP(Year), "Couresess".KVP(Couresess),"Nots".KVP(Nots)
                   , "Id".KVP(0)).AsNonQuery();


                //return RedirectToAction("Create", new { BranchId = model.BranchId, CollageId = model.ColleageId, LevelId = model.LevelId, TrackId = model.TrackId });

                return Json("success",  JsonRequestBehavior.AllowGet);

            }
        }


      

        [HttpGet]
        public ActionResult GetTrackList(int id,int CollageId)
        {
            var Colleage = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Colleage.AddRange("Sp_TrackList".ExecuParamsSqlOrStored(false, "LevelId".KVP(id),"CollageId".KVP(CollageId)).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.CollegeList = Colleage;
            //var j = context.EducationalGrades.Where(e => e.StageId == id && e.IsDeleted == false).Select(g => new { g.NameAr, g.Id }).ToList();
            return Json(Colleage, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Edit(int id = 0)
        {
            var Branchs = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Branchs.AddRange("Sp_BranchesDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.BranchList = Branchs;

            var Colleage = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Colleage.AddRange("Sp_ColleageDrop".ExecuParamsSqlOrStored(false, "BranchId".KVP(1)).AsList<ColleageDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.CollegeList = Colleage;
            var Matariale = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Matariale.AddRange("Sp_MatarialeDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.MatarialeList = Matariale;

            var Elmatone = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Elmatone.AddRange("Sp_ElmatoneDrop".ExecuParamsSqlOrStored(false, "MatarialeId".KVP(0)).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.ElmatoneList = Elmatone;


            var Level = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Level.AddRange("Sp_LeveleDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.LevelList = Level;
            var Track = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Track.AddRange("SpTrackDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.TrackList = Track;
            var model = "SP_SelectCoursesId".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<CoursesFM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Edit", model.FirstOrDefault());
        }

        [HttpPost]
        public ActionResult Edit(CoursesFM model)
        {


            var data = "SP_AddCourses".ExecuParamsSqlOrStored(false, "BranchId".KVP(model.BranchId), "ColleageId".KVP(model.ColleageId),
                "MatarialeId".KVP(model.MatarialeId), "ElmatoneId".KVP(model.ElmatoneId), "LevelId".KVP(model.LevelId), "TrackId".KVP(model.TrackId)
                , "Weak".KVP(model.Weak), "Year".KVP(model.Year), "Couresess".KVP(model.Couresess)
               , "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0 && model.Id != 0)
            {
                return Json("successEdit," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
                return RedirectToAction("Create", new { BranchId = model.BranchId, CollageId = model.ColleageId, LevelId = model.LevelId, TrackId = model.TrackId });
            }
            else if (data != 0)
            {
                //return RedirectToAction("Create", new { BranchId = model.BranchId, CollageId = model.ColleageId, LevelId = model.LevelId, TrackId = model.TrackId });

                return Json("success," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحفظ ", JsonRequestBehavior.AllowGet);
        }



        [HttpGet]
        public ActionResult Delete(int id)
        {
            CoursesFM obj = new CoursesFM();
            obj.Id = id;
            return PartialView("~/Areas/Admin/Views/Courses/Delete.cshtml", obj);
        }

        [HttpGet]
        public ActionResult AddCoursess(int BranchId,int ElmatoneId, int ColleageId, int LevelId, int TrackId,string Nots)
        {
            CoursesFM obj = new CoursesFM();
            obj.ElmatoneId = ElmatoneId;
            obj.ColleageId = ColleageId;
            obj.LevelId = LevelId;
            obj.Nots = Nots;

            obj.TrackId = TrackId;
            obj.BranchId = BranchId;

            return View("~/Areas/Admin/Views/Courses/_Courses.cshtml", obj);

        }

        public ActionResult OldCourses(int TrackId)
        {
            var model = "SP_GetAllCourses".ExecuParamsSqlOrStored(false, "TrackId".KVP(TrackId)).AsList<CoursesVM>();
            return View("~/Areas/Admin/Views/Courses/_OldCourses.cshtml", model.Where(a=>a.TrackId==TrackId).ToList());


        }


        public ActionResult OldDocCourses(int TrackId)
        {
            var model = "SP_GetAllDocCourses".ExecuParamsSqlOrStored(false).AsList<DocCourses>();
            return View("~/Areas/Admin/Views/Courses/_OldDocCourses.cshtml", model.Where(a => a.TrackId == TrackId).ToList());


        }

        [HttpPost]
        public ActionResult AddDoctrinesCourses( Array CoursesList, int TrackId)
        {

            var OldDate = "SP_GetAllDocCourses".ExecuParamsSqlOrStored(false).AsList<DocCourses>().Where(a=>a.TrackId==TrackId);

            foreach (var item in OldDate)
            {
                var data = "SP_DeleteDocCourses".ExecuParamsSqlOrStored(false, "Id".KVP(item.TrackId)).AsNonQuery();


            }
            foreach (var item in CoursesList)
            {

                var data = "SP_AddDocourses".ExecuParamsSqlOrStored(false,  "TrackId".KVP(TrackId),"DocDetailsId".KVP(item)
                
              , "Id".KVP(0)).AsNonQuery();

                //return RedirectToAction("Create", new { BranchId = model.BranchId, CollageId = model.ColleageId, LevelId = model.LevelId, TrackId = model.TrackId });
            }
            var model = "SP_GetAllDoctrinesDetailss".ExecuParamsSqlOrStored(false).AsList<DoctrinesDetailsVM>();

            return PartialView("DoctrinesList", model);



        }
        [HttpGet]
        public ActionResult GetCollageList(int id)
        {
            var Colleage = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Colleage.AddRange("Sp_ColleageDrop".ExecuParamsSqlOrStored(false, "BranchId".KVP(id)).AsList<ColleageDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.CollegeList = Colleage;
            //var j = context.EducationalGrades.Where(e => e.StageId == id && e.IsDeleted == false).Select(g => new { g.NameAr, g.Id }).ToList();
            return Json(Colleage, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetElmotenList(int id)
        {
            var Elmatone = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Elmatone.AddRange("Sp_ElmatoneDrop".ExecuParamsSqlOrStored(false, "MatarialeId".KVP(id)).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.ElmatoneList = Elmatone;
            //var j = context.EducationalGrades.Where(e => e.StageId == id && e.IsDeleted == false).Select(g => new { g.NameAr, g.Id }).ToList();
            return Json(Elmatone, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteCourses".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }
    }
}