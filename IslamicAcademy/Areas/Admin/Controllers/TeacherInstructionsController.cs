﻿using IslamicAcademy.Core;
using IslamicAcademy.Core.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IslamicAcademy.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]

    public class TeacherInstructionsController : Controller
    {
        // GET: Admin/TeacherInstructions
        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {

            var model = "SP_GetAllTeacherInstructions".ExecuParamsSqlOrStored(false).AsList<InstructionsVM>();
            return View(model);
        }


        public ActionResult Create()
        {
            InstructionsVM Obj = new InstructionsVM();
            return View( Obj);
        }
        [HttpPost, ValidateInput(false)]
        public JsonResult Create(InstructionsVM model)
        {


      
            model.Datetime = DateTime.Now;
            var data = "SP_AddTeacherInstructions".ExecuParamsSqlOrStored(false, "Tittel".KVP(model.Tittel), "Details".KVP(model.Details),  "Datetime".KVP(model.Datetime)
               , "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0 && model.Id != 0)
            {
                return Json("successEdit," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else if (data != 0)
            {
                return Json("success," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحفظ ", JsonRequestBehavior.AllowGet);

        }


        public ActionResult Edit(int id = 0)
        {
            var model = "SP_SelecTeacherInstructionsById".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<InstructionsVM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            InstructionsVM obj = new InstructionsVM();
            obj.Id = id;
            return PartialView("~/Areas/Admin/Views/TeacherInstructions/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteTeacherInstructions".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }
    }
}