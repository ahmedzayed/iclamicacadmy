﻿using IslamicAcademy.Core;
using IslamicAcademy.Core.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IslamicAcademy.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]

    public class MatirialeController : Controller
    {
        // GET: Admin/Matiriale
        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {

            var model = "SP_GetAllMatiriale".ExecuParamsSqlOrStored(false).AsList<MatrailVM>();
            return View(model);
        }


        public ActionResult Create()
        {
            MatrailVM Obj = new MatrailVM();
            
            return PartialView("~/Areas/Admin/Views/Matiriale/Create.cshtml", Obj);
        }
        [HttpPost, ValidateInput(false)]
        public JsonResult Create(MatrailVM model)
        {
            var data = "SP_AddMatiriale".ExecuParamsSqlOrStored(false, "Name".KVP(model.Name),"Details".KVP(model.Details)
               , "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0 && model.Id != 0)
            {
                return Json("successEdit," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else if (data != 0)
            {
                return Json("success," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحفظ ", JsonRequestBehavior.AllowGet);
        }


        public ActionResult Edit(int id = 0)
        {
           
            var model = "SP_SelectMatirialeId".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<MatrailVM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            MatrailVM obj = new MatrailVM();
            obj.Id = id;
            return PartialView("~/Areas/Admin/Views/Matiriale/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteMatiriale".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }
    }
}