﻿var Courses = {
    init: function () {
        Search();
    },
}
function Save() {
    var $form = $("#ModelForm");
    //if ($form.valid()) {
    var data = new FormData();
  
    $form.find('input[type="file"]').each(function (count, fileInput) {
        var files = $(fileInput).get(0).files;
        // Add the uploaded image content to the form data collection
        if (files.length > 0) {
            $.each(files, function (index, file) {
                data.append($(fileInput).attr("name") + "[" + index + "]", file);
            });
        }
    });
    var formData = $form.serializeArray();
    $.each(formData, function (key, value) {
        data.append(this.name, this.value);
    });

    //if (IsValid()) {
    ajaxRequest($("#ModelForm").attr('method'), $("#ModelForm").attr('action'), data, 'json', false, false).done(function (result) {

        var res = result.split(',');

        //debugger;
        if (res[0] == "success") {

            toastr.success(res[1]);
            var ddl = document.getElementById('MatarialeId');
            ddl.options[0].selected = true;

            var dd2 = document.getElementById('ElmatoneId');
            dd2.options[0].selected = true;
var dd3 = document.getElementById('Weak')
var dd4 = document.getElementById('Couresess')
var dd5 = document.getElementById('Year')
           dd3.val("");
dd4.val("");
 dd5.val("");          



            //location.href = "../Courses/Create";

            Search();
        }
        else if (res[0] == "successEdit") {
            toastr.success(res[1]);

            location.href = "../../Courses/Index1";
            Search();
        }
        else
            toastr.error(res[1]);
    });
    //  }
    //}
}
function Create() {
    var Url = "../Courses/Create";
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}
function Edit(id) {
    var Url = "../Courses/Edit?Id=" + id;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}
function Delete(id) {
    var Url = "../Courses/Delete?Id=" + id;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}

function AddCoursess(ElmatoneId) {
    alert(ElmatoneId);

    var BranchId = $("#BranchId").val();
    var ColleageId = $("#ColleageId").val();
    var LevelId = $("#LevelId").val();
    var TrackId = $("#TrackId").val();
    alert(BranchId);

    var Url = "../Courses/AddCoursess?ElmatoneId=" + ElmatoneId + "&BranchId=" + BranchId + "&ColleageId=" + ColleageId + "&LevelId=" + LevelId + "&TrackId=" + TrackId;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}


function DeleteRow(id) {
    var Url = "../Courses/DeleteRow?Id=" + id;
    ajaxRequest("Post", Url, "", 'json', false, false).done(function (result) {
        var res = result.split(',');
        if (res[0] == "success") {
            $('#myModalAddEdit').modal('hide');
            toastr.success(res[1]);
            Search();
            //  window.location = "../ManageTasksAnalysis/Index";
        }
        else
            toastr.error(res[1]);
    });
}
function IsValid() {
    var isValidItem = true;

    if ($('#Name').val() == "") {
        isValidItem = false;
        toastr.error("من فضلك ادخل الاسم عربي");
    }

    return isValidItem;
}
function Search() {

    var url = "../Courses/Search";
    ajaxRequest("post", url, "", 'html').done(function (data) {
        $("#SearchTableContainer").html(data);
    });
}


