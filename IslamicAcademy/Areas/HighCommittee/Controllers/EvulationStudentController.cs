﻿using IslamicAcademy.Core;
using IslamicAcademy.Core.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IslamicAcademy.Areas.HighCommittee.Controllers
{
    public class EvulationStudentController : Controller
    {
        // GET: HighCommittee/EvulationStudent
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult StudentRegister(int TrackId)
        {
            var Elmatone = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Elmatone.AddRange("Sp_ElmatoneDrop".ExecuParamsSqlOrStored(false, "MatarialeId".KVP(0)).AsList<ColleageDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.ElmatoneList = Elmatone;

            var model = "SP_GetAllFirestBranchRegister".ExecuParamsSqlOrStored(false, "TrackId".KVP(TrackId)).AsList<FirestBranchRegisterVM>();
            return PartialView(model);
        }

        public ActionResult AddEvulation(int? Degree, string Day, string Weak, int? Oral, string Nots, int? ElmatoneId, int? StudentId)
        {

            var data = "SP_AddEvualtionStudent".ExecuParamsSqlOrStored(false, "StudentId".KVP(StudentId), "ElmatoneId".KVP(ElmatoneId), "Degree".KVP(Degree), "Day".KVP(Day), "Weak".KVP(Weak), "Oral".KVP(Oral), "Nots".KVP(Nots), "Id".KVP(0)).AsNonQuery();
            if (data != 0)
            {
                TempData["success"] = "<script>toastr.success(' تم الاضافه بنجاح');</script>";

                return RedirectToAction("Index");
            }
            else
            {
                TempData["success"] = "<script>toastr.error(' لم يتم الحفظ  ');</script>";

                return RedirectToAction("Index");
            }
        }

    }
}