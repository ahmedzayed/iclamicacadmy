﻿using System.Web.Mvc;

namespace IslamicAcademy.Areas.HighCommittee
{
    public class HighCommitteeAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "HighCommittee";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "HighCommittee_default",
                "HighCommittee/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}