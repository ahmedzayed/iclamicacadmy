﻿var TaskEmp = {
    init: function () {
        Search();
    },
}
function Save() {
    var $form = $("#ModelForm");
    //if ($form.valid()) {
    var data = new FormData();

    var EmpList = [];
    $.each($("input[name='EmpId']:checked"), function () {
        EmpList.push($(this).val());
    });

    var EmpShowList = [];
    $.each($("input[name='EmpShowId']:checked"), function () {
        EmpShowList.push($(this).val());
    });


    $form.find('input[type="file"]').each(function (count, fileInput) {
        var files = $(fileInput).get(0).files;
        // Add the uploaded image content to the form data collection
        if (files.length > 0) {
            $.each(files, function (index, file) {
                data.append($(fileInput).attr("name") + "[" + index + "]", file);
            });
        }
    });
    var formData = $form.serializeArray();
    $.each(formData, function (key, value) {
        data.append(this.name, this.value);
        data.append(this.EmpList, this.value);
        data.append(this.EmpShowList, this.value);
    });

    //if (IsValid()) {
    ajaxRequest($("#ModelForm").attr('method'), $("#ModelForm").attr('action'), data, 'json', false, false).done(function (result) {

        var res = result.split(',');

        //debugger;
        //debugger;
        if (res[0] == "success") {

            toastr.success(res[1]);

            location.href = "../TaskEmp/Index1";
            Search();
        }
        else if (res[0] == "successEdit") {
            toastr.success(res[1]);

            location.href = "../../TaskEmp/Index1";
            Search();
        }
        else
            toastr.error(res[1]);
    });
    //  }
    //}
}
function Create() {
    var Url = "../TaskEmp/Create";
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}
function Edit(id) {
    var Url = "../TaskEmp/Edit?Id=" + id;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}
function Delete(id) {
    var Url = "../TaskEmp/Delete?Id=" + id;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}
function EmpDetails(id) {
    var Url = "../TaskEmp/EmpDetails?Id=" + id;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}
function EmpShowOnly(id) {
    var Url = "../TaskEmp/EmpShowOnly?Id=" + id;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}

function DeleteRow(id) {
    var Url = "../TaskEmp/DeleteRow?Id=" + id;
    ajaxRequest("Post", Url, "", 'json', false, false).done(function (result) {
        var res = result.split(',');
        if (res[0] == "success") {
            $('#myModalAddEdit').modal('hide');
            toastr.success(res[1]);
            Search();
            //  window.location = "../ManageTasksAnalysis/Index";
        }
        else
            toastr.error(res[1]);
    });
}
function IsValid() {
    var isValidItem = true;

    if ($('#Name').val() == "") {
        isValidItem = false;
        toastr.error("من فضلك ادخل الاسم عربي");
    }

    return isValidItem;
}
function Search() {

    var url = "../TaskEmp/Search";
    ajaxRequest("post", url, "", 'html').done(function (data) {
        $("#SearchTableContainer").html(data);
    });
}


