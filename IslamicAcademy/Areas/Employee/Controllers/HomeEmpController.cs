﻿
using IslamicAcademy.Core;
using IslamicAcademy.Core.VM;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IslamicAcademy.Areas.Employee.Controllers
{
    public class HomeEmpController : Controller
    {
        // GET: Employee/Default
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult _SideMenu()
        {
            var UserId = User.Identity.GetUserId();
            var EmployeeByUserId = "SP_GetEmployeeByUserId".ExecuParamsSqlOrStored(false, "UserId".KVP(UserId)).AsList<EmployeeVM>();
            /// EmployeeTaske
            var model = "SP_GetAllTaskesBympId".ExecuParamsSqlOrStored(false, "EmpId".KVP(EmployeeByUserId.FirstOrDefault().ID)).AsList<TaskesVM>();
            var data = model.Where(a => a.TaskeCase != true);
            ViewBag.EmpTask = data.Count();



            var Ended = "SP_GetAllTaskesBympId".ExecuParamsSqlOrStored(false, "EmpId".KVP(EmployeeByUserId.FirstOrDefault().ID)).AsList<TaskesVM>().Where(a => a.TaskeCase == true);
            ViewBag.TaskEndedEmp = Ended.Count();

            var EmpShow = "SP_GetAllTaskesShowOnlyBympId".ExecuParamsSqlOrStored(false, "EmpId".KVP(EmployeeByUserId.FirstOrDefault().ID)).AsList<TaskesVM>();
            ViewBag.EmpShow = EmpShow.Count();

            return PartialView();
        }
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Home", new { Area = "" });
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
    }

}