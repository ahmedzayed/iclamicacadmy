﻿using IslamicAcademy.Core;
using IslamicAcademy.Core.VM;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IslamicAcademy.Areas.Employee.Controllers
{
    public class TaskEmpController : Controller
    {
        // GET: Employee/TaskEmp
        public ActionResult Index1()
        {
            return View();
        }

        public ActionResult Search()
        {

            var UserId = User.Identity.GetUserId();
            var EmployeeByUserId = "SP_GetEmployeeByUserId".ExecuParamsSqlOrStored(false, "UserId".KVP(UserId)).AsList<EmployeeVM>();

            var model = "SP_GetAllTaskesBympId".ExecuParamsSqlOrStored(false,"EmpId".KVP(EmployeeByUserId.FirstOrDefault().ID)).AsList<TaskesVM>();
            var data = model.Where(a => a.TaskeCase != true);
            return View(data);
        }
        public ActionResult TaskeEnd()
        {
            return View();
        }
        public ActionResult SearchEnd()
        {

            var UserId = User.Identity.GetUserId();
            var EmployeeByUserId = "SP_GetEmployeeByUserId".ExecuParamsSqlOrStored(false, "UserId".KVP(UserId)).AsList<EmployeeVM>();

            var model = "SP_GetAllTaskesBympId".ExecuParamsSqlOrStored(false, "EmpId".KVP(EmployeeByUserId.FirstOrDefault().ID)).AsList<TaskesVM>();
            return View(model.Where(a => a.TaskeCase == true));
        }

        public ActionResult TaskEmpshowOnly()
        {
            return View();
        }

        public ActionResult SearchShowOnly()
        {

            var UserId = User.Identity.GetUserId();
            var EmployeeByUserId = "SP_GetEmployeeByUserId".ExecuParamsSqlOrStored(false, "UserId".KVP(UserId)).AsList<EmployeeVM>();

            var model = "SP_GetAllTaskesShowOnlyBympId".ExecuParamsSqlOrStored(false, "EmpId".KVP(EmployeeByUserId.FirstOrDefault().ID)).AsList<TaskesVM>();
            return View(model);
        }


        public ActionResult EndTaske(int TaskeId)
        {
            var data = "SP_EndTaske".ExecuParamsSqlOrStored(false
            
            , "TaskeId".KVP(TaskeId)).AsNonQuery();
            TempData["success"] = "<script>toastr.success(' تم الاضافه بنجاح');</script>";
            return RedirectToAction("Index1", "TaskEmp");
        }


        public ActionResult EditTaske(int TaskeId)
        {
            ViewBag.TaskeId = TaskeId;
            return View();
        }

        public ActionResult GetAllEmployee()
        {
            var model = "SP_GetAllEmp".ExecuParamsSqlOrStored(false).AsList<SettingVM>();

            return PartialView("_EmpList", model);
        }

        [HttpPost]
        public ActionResult EditTaske(int? TaskeId, Array EmpList)
        {
            var model = "SP_GetAllEmpDetailsByTaskId".ExecuParamsSqlOrStored(false, "TaskId".KVP(TaskeId)).AsList<TaskeEmpDetailsVM>();
            foreach (var item in model)
            {
                var Deletedata = "SP_DeleteTaske".ExecuParamsSqlOrStored(false, "TaskeId".KVP(TaskeId)).AsNonQuery();

            }

            foreach (var item1 in EmpList)
                {

                    var data = "SP_EditTaske".ExecuParamsSqlOrStored(false, "EmployeeId".KVP(item1)

            , "TaskeId".KVP(TaskeId)).AsNonQuery();
                }
                
               
            
            TempData["success"] = "<script>toastr.success(' تم الاضافه بنجاح');</script>";

            return RedirectToAction("Index1", "TaskEmp");
        }


        public ActionResult RequestTime(int TaskeId)
        {
            var data = "SP_RequestTaske".ExecuParamsSqlOrStored(false

            , "TaskeId".KVP(TaskeId)).AsNonQuery();
            TempData["success"] = "<script>toastr.success(' تم الاضافه بنجاح');</script>";
            return RedirectToAction("Index1", "TaskEmp");
        }

        public ActionResult Create()
        {
            TaskesVM Obj = new TaskesVM();
            var TaskType = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            TaskType.AddRange("Sp_TaskTypeDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.TaskTypeList = TaskType;
            return PartialView("~/Areas/Employee/Views/TaskEmp/Create.cshtml", Obj);
        }
        [HttpPost, ValidateInput(false)]
        public JsonResult Create(EmpDetails obj, string Name, int? Type, string Date, int? TaskTypeId, string TimeProposed, string Details)
        {
            var File = "";

          
            var data = "SP_AddTaskes".ExecuParamsSqlOrStored(false, "Name".KVP(Name),
                "Type".KVP(Type), "Date".KVP(Date), "TaskTypeId".KVP(TaskTypeId), "File".KVP(File), "TimeProposed".KVP(TimeProposed), "Details".KVP(Details)
               , "Id".KVP(0)).AsNonQuery();

            var model1 = "SP_GetAllTaskes".ExecuParamsSqlOrStored(false).AsList<TaskesVM>();
            int Task = model1.LastOrDefault().Id;
            if (data != 0)
            {
                foreach (var item in obj.EmpList)
                {

                    var model = "SP_GetEmpByTaskId".ExecuParamsSqlOrStored(false).AsList<TaskeEmpDetailsVM>().Where(a => a.TaskId == Task && a.EmployeeId == Convert.ToInt32(item));
                    if (model.Count() == 0)
                    {
                        var data1 = "SP_AddEmpTaskes".ExecuParamsSqlOrStored(false, "TaskId".KVP(Task),
                         "EmpId".KVP(item)
                        , "Id".KVP(0)).AsNonQuery();
                    }

                }

                foreach (var item in obj.EmpShowList)
                {
                    var model = "SP_GetEmpShowByTaskId".ExecuParamsSqlOrStored(false).AsList<TaskeEmpDetailsVM>().Where(a => a.TaskId == Task && a.EmployeeId == Convert.ToInt32(item));
                    if (model.Count() == 0)
                    {
                        var data2 = "SP_AddEmpShowTaskes".ExecuParamsSqlOrStored(false, "TaskId".KVP(Task),
                         "EmpId".KVP(item)
                        , "Id".KVP(0)).AsNonQuery();
                    }

                }




                return Json("success," + "تم الحفظ بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحفظ ", JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAllEmployeeDe()
        {
            var model = "SP_GetAllEmp".ExecuParamsSqlOrStored(false).AsList<SettingVM>();

            return PartialView("_EmpList", model);
        }
        public ActionResult EmpShow()
        {
            var model = "SP_GetAllEmp".ExecuParamsSqlOrStored(false).AsList<SettingVM>();

            return PartialView("EmpShow", model);
        }

        public ActionResult TaskeList()
        {
            return View();
        }
        public ActionResult TaskeListSearch()
        {
            var model = "SP_GetAllTaskes".ExecuParamsSqlOrStored(false).AsList<TaskesVM>();
            return View(model);
        }

        [HttpGet]
        public ActionResult EmpDetails(int id)
        {
            var model = "SP_GetAllEmpDetailsByTaskId".ExecuParamsSqlOrStored(false, "TaskId".KVP(id)).AsList<TaskeEmpDetailsVM>();

            return PartialView("~/Areas/Employee/Views/TaskEmp/EmpDetails.cshtml", model);
        }

        [HttpGet]
        public ActionResult EmpShowOnly(int id)
        {
            var model = "SP_GetAllEmpShowDetailsByTaskId".ExecuParamsSqlOrStored(false, "TaskId".KVP(id)).AsList<TaskeEmpDetailsVM>();

            return PartialView("~/Areas/Employee/Views/TaskEmp/EmpShowOnly.cshtml", model);
        }



    }
}