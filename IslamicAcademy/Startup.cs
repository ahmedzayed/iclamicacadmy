﻿using IslamicAcademy.Entity;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using System;
using System.Linq;
using System.Reflection;
using System.Security.Claims;

[assembly: OwinStartupAttribute(typeof(IslamicAcademy.Startup))]
namespace IslamicAcademy
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);

         //   DB_A3E980_metoonEntities un = new DB_A3E980_metoonEntities();

         //   // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=316888
         //   System.Web.Helpers.AntiForgeryConfig.UniqueClaimTypeIdentifier = ClaimTypes.Name;
         //   app.UseCookieAuthentication(new CookieAuthenticationOptions
         //   {
         //       AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
         //       LoginPath = new PathString("/Account/Login"),
         //       CookieSecure = CookieSecureOption.SameAsRequest,
         //       CookieName = "IslamicAcademy",
         //       ExpireTimeSpan = TimeSpan.FromDays(5.0)
         //   });
         //   Assembly asm = Assembly.GetExecutingAssembly();

         //   var controlleractionlist = asm.GetTypes()
         //.Where(type => typeof(System.Web.Mvc.Controller).IsAssignableFrom(type))
         //.SelectMany(type => type.GetMethods(BindingFlags.Instance | BindingFlags.DeclaredOnly | BindingFlags.Public))
         //.Where(m => !m.GetCustomAttributes(typeof(System.Runtime.CompilerServices.CompilerGeneratedAttribute), true).Any())
         //.Select(x => new { Controller = x.DeclaringType.Name, NameSpace = x.DeclaringType.Namespace, FullName = x.DeclaringType.FullName, Action = x.Name })
         //.OrderBy(x => x.Controller).ThenBy(x => x.Action).ToList();

         //   var groupedControllerAction = from c in controlleractionlist
         //                                 group c by new
         //                                 {
         //                                     c.FullName,
         //                                     c.Controller,

         //                                 } into grps
         //                                 select new
         //                                 {
         //                                     FullName = grps.Key.FullName,
         //                                     Controller = grps.Key.Controller,

         //                                     Acions = grps.ToList().Distinct(),
         //                                 };
         //   var contrs = un.AppControllers.ToList();

         //   foreach (var cnt in groupedControllerAction.Where(t => contrs.Any(x => t.FullName == x.ControllerPath)))
         //   {

         //       var cont = un.AppControllers.FirstOrDefault(x => x.ControllerPath == cnt.FullName);
         //       foreach (var item in cnt.Acions.Where(ac => !cont.AppActions.Any(x => ac.Action == x.ActionName)))
         //       {
         //           AppActions aa = new AppActions();
         //           aa.ActionName = item.Action;
         //           cont.AppActions.Add(aa);
         //       }
         //   }

         //   foreach (var cnt in groupedControllerAction.Where(t => !contrs.Any(x => t.FullName == x.ControllerPath)))
         //   {
         //       AppControllers appcnt = new AppControllers();
         //       if (cnt.FullName.Contains(@"Areas."))
         //       {

         //           int pFrom = cnt.FullName.IndexOf(@"Areas.") + @"Areas.".Length;
         //           int pTo = cnt.FullName.LastIndexOf(".Controllers");

         //           String result = cnt.FullName.Substring(pFrom, pTo - pFrom);
         //           appcnt.Area = result;

         //       }
         //       appcnt.ControllerName = cnt.Controller;
         //       appcnt.ControllerPath = cnt.FullName;
         //       foreach (var item in cnt.Acions)
         //       {
         //           AppActions aa = new AppActions();
         //           aa.ActionName = item.Action;
         //           appcnt.AppActions.Add(aa);
         //       }
         //       un.AppControllers.Add(appcnt);


         //   }
         //   un.SaveChanges();
        }
    }
}
