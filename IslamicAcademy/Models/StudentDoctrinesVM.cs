﻿using IslamicAcademy.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IslamicAcademy.Models
{
    public class StudentDoctrinesVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<DoctrinesDetails> DoctrinesDetails { get; set; }
        public int TrackId { get; set; }
        public string DocDetailsName { get; set; }
        public int DocDetailsId { get; set; }
    }
}