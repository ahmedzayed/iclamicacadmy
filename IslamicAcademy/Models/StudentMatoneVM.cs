﻿using IslamicAcademy.Core.VM;
using IslamicAcademy.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IslamicAcademy.Models
{
    public class StudentMatoneVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Courses> Elmatones { get; set; }
        public List<Elmatone> Elmatone { get; set; }

        public string MatoneName { get; set; }
        public int MatoneId { get; set; }

    }
}