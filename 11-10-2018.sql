USE [master]
GO
/****** Object:  Database [IslamicAcademy]    Script Date: 11/10/2018 04:32:04 م ******/
CREATE DATABASE [IslamicAcademy]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'IslamicAcademy', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\IslamicAcademy.mdf' , SIZE = 4288KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'IslamicAcademy_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\IslamicAcademy_log.ldf' , SIZE = 1072KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [IslamicAcademy] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [IslamicAcademy].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [IslamicAcademy] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [IslamicAcademy] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [IslamicAcademy] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [IslamicAcademy] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [IslamicAcademy] SET ARITHABORT OFF 
GO
ALTER DATABASE [IslamicAcademy] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [IslamicAcademy] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [IslamicAcademy] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [IslamicAcademy] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [IslamicAcademy] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [IslamicAcademy] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [IslamicAcademy] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [IslamicAcademy] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [IslamicAcademy] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [IslamicAcademy] SET  ENABLE_BROKER 
GO
ALTER DATABASE [IslamicAcademy] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [IslamicAcademy] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [IslamicAcademy] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [IslamicAcademy] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [IslamicAcademy] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [IslamicAcademy] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [IslamicAcademy] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [IslamicAcademy] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [IslamicAcademy] SET  MULTI_USER 
GO
ALTER DATABASE [IslamicAcademy] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [IslamicAcademy] SET DB_CHAINING OFF 
GO
ALTER DATABASE [IslamicAcademy] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [IslamicAcademy] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [IslamicAcademy] SET DELAYED_DURABILITY = DISABLED 
GO
USE [IslamicAcademy]
GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Acoustics]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Acoustics](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Tittel] [nvarchar](50) NULL,
	[MatrialId] [int] NULL,
	[ElmatoneId] [int] NULL,
	[FilePathe] [nvarchar](50) NULL,
 CONSTRAINT [PK_Acoustics] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Advantages]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Advantages](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Details] [nvarchar](max) NULL,
	[BranchId] [int] NULL,
 CONSTRAINT [PK_Advantages] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Advertisement]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Advertisement](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Tittel] [nvarchar](50) NULL,
	[Details] [nvarchar](max) NULL,
	[ImagePathe] [nvarchar](max) NULL,
	[DateTime] [datetime] NULL,
 CONSTRAINT [PK_Advertisement] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
	[FullName] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AssignmentsDate]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AssignmentsDate](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Tittel] [nvarchar](50) NULL,
	[Details] [nvarchar](max) NULL,
	[BranchId] [int] NULL,
 CONSTRAINT [PK_AssignmentsDate] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Branches]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Branches](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
 CONSTRAINT [PK_Branches] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Colleges]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Colleges](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[BranchId] [int] NULL,
 CONSTRAINT [PK_Colleges] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ContactUs]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContactUs](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[Subject] [nvarchar](50) NULL,
	[Phone] [nvarchar](50) NULL,
	[Message] [nvarchar](max) NULL,
 CONSTRAINT [PK_ContactUs] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CourcesStudent]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CourcesStudent](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[StudentId] [int] NULL,
	[ElmatoneId] [int] NULL,
 CONSTRAINT [PK_CourcesStudent] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Courses]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Courses](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BranchId] [int] NULL,
	[ColleageId] [int] NULL,
	[MatarialeId] [int] NULL,
	[ElmatoneId] [int] NULL,
	[Level] [nvarchar](50) NULL,
	[Masare] [nvarchar](50) NULL,
	[TrackId] [int] NULL,
	[Weak] [nvarchar](50) NULL,
	[Year] [nvarchar](50) NULL,
 CONSTRAINT [PK_Courses] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CoursesStudentTempDetails]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CoursesStudentTempDetails](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ElmatoneId] [int] NULL,
	[HeaderId] [int] NULL,
 CONSTRAINT [PK_TempCoursesStudent] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CoursesStudentTempHeader]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CoursesStudentTempHeader](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[StudentId] [int] NULL,
	[CoursesCase] [bit] NULL,
 CONSTRAINT [PK_CoursesStudentHeader] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EducationBody]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EducationBody](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[BranchId] [int] NULL,
 CONSTRAINT [PK_EducationBody] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Elmatone]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Elmatone](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[MatrialeId] [int] NULL,
 CONSTRAINT [PK_Elmatone] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EvaluationCriteria]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EvaluationCriteria](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Details] [nvarchar](50) NULL,
	[BranchId] [int] NULL,
 CONSTRAINT [PK_EvaluationCriteria] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EvulationStudent]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EvulationStudent](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ElmatoneId] [int] NULL,
	[CoursesId] [int] NULL,
	[StudentId] [int] NULL,
	[Degree] [money] NULL,
	[Nots] [nvarchar](50) NULL,
	[Weaky] [nvarchar](50) NULL,
	[Day] [nvarchar](50) NULL,
	[Oral] [nvarchar](50) NULL,
 CONSTRAINT [PK_EvulationStudent] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FirestBranchRegister]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FirestBranchRegister](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ColleageId] [int] NULL,
	[Name] [nvarchar](50) NULL,
	[NationalityId] [int] NULL,
	[Code] [nvarchar](50) NULL,
	[Phonenumber] [nvarchar](50) NULL,
	[WhatsNumber] [nvarchar](50) NULL,
	[TelgramNumber] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[StageId] [int] NULL,
	[Level] [nvarchar](50) NULL,
	[Case] [nvarchar](50) NULL,
	[IsAbroved] [bit] NULL,
	[JoinedUniversity] [nvarchar](50) NULL,
	[EducationBodyId] [int] NULL,
	[EvaulationDayes] [nvarchar](50) NULL,
	[TrackId] [int] NULL,
	[UserId] [nvarchar](128) NULL,
 CONSTRAINT [PK_FirestBranchRegister] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[IntroducingPrize]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IntroducingPrize](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Details] [nvarchar](max) NULL,
 CONSTRAINT [PK_IntroducingPrize] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Library]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Library](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Tittel] [nvarchar](50) NULL,
	[Details] [nvarchar](max) NULL,
	[ImagePathe] [nvarchar](max) NULL,
	[Datetime] [datetime] NULL,
 CONSTRAINT [PK_Library] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LibraryRead]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LibraryRead](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Tittel] [nvarchar](50) NULL,
	[FilePathe] [nvarchar](max) NULL,
	[MatrialId] [int] NULL,
	[ElmatoneId] [int] NULL,
 CONSTRAINT [PK_LibraryRead] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Matiriale]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Matiriale](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
 CONSTRAINT [PK_Matiriale] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Nationality]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Nationality](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
 CONSTRAINT [PK_Nationality] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[News]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[News](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Tittel] [nvarchar](50) NULL,
	[Details] [nvarchar](max) NULL,
	[Datetime] [datetime] NULL,
	[ImagePathe] [nvarchar](max) NULL,
 CONSTRAINT [PK_News] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PhotoGallery]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PhotoGallery](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Tittel] [nvarchar](50) NULL,
	[Details] [nvarchar](max) NULL,
	[ImagePathe] [nvarchar](max) NULL,
	[Datetime] [datetime] NULL,
 CONSTRAINT [PK_PhotoGallery] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PrepareStudents]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PrepareStudents](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Weak] [nvarchar](50) NULL,
	[Day] [nvarchar](50) NULL,
	[Degree] [money] NULL,
	[IsAttandce] [bit] NULL,
	[StudentId] [int] NULL,
 CONSTRAINT [PK_PrepareStudents] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Representations]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Representations](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Details] [nvarchar](max) NULL,
 CONSTRAINT [PK_Representations] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Satage]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Satage](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
 CONSTRAINT [PK_Satage] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ScandBranchCourses]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ScandBranchCourses](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MatrialId] [int] NULL,
	[ElmatoneId] [int] NULL,
 CONSTRAINT [PK_ScandBranchCourses] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SecondBranchRegister]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SecondBranchRegister](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[NationalityId] [int] NULL,
	[PhoneNumber] [nvarchar](50) NULL,
	[TelegramNumber] [nvarchar](50) NULL,
	[WhatsNumber] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[AcadmayDetails] [nvarchar](50) NULL,
	[Case] [nvarchar](50) NULL,
	[EducationBodyId] [int] NULL,
	[StageId] [int] NULL,
	[STageEducationhigher] [nvarchar](50) NULL,
	[Year] [nvarchar](50) NULL,
	[Bachelorsdegree] [nvarchar](50) NULL,
	[Level] [nvarchar](50) NULL,
	[Elmatone] [nvarchar](50) NULL,
	[IsAbroved] [bit] NULL,
	[JoinedUniversity] [nvarchar](50) NULL,
 CONSTRAINT [PK_SecondBranchRegister] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[StudentGrades]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StudentGrades](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[StudentId] [int] NULL,
	[EmatoneId] [int] NULL,
	[Value] [nchar](10) NULL,
	[Datetime] [datetime] NULL,
 CONSTRAINT [PK_StudentGrades] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[StudentInstructions]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StudentInstructions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Datetime] [datetime] NULL,
	[Tittel] [nvarchar](50) NULL,
	[Details] [nvarchar](max) NULL,
 CONSTRAINT [PK_StudentInstructions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SysteamPrize]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SysteamPrize](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Details] [nvarchar](max) NULL,
 CONSTRAINT [PK_SysteamPrize] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TeacherInstructions]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TeacherInstructions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Datetime] [datetime] NULL,
	[Tittel] [nvarchar](50) NULL,
	[Details] [nvarchar](max) NULL,
 CONSTRAINT [PK_TeacherInstructions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ThirdBranchRegister]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ThirdBranchRegister](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Tracks] [nvarchar](50) NULL,
	[Name] [nvarchar](50) NULL,
	[NationalityId] [int] NULL,
	[Code] [nvarchar](50) NULL,
	[PhoneNumber] [nvarchar](50) NULL,
	[WhatsNumber] [nvarchar](50) NULL,
	[TelgrameNumber] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[EducationBodyId] [int] NULL,
	[StageId] [int] NULL,
	[Level] [nvarchar](50) NULL,
	[Case] [nvarchar](50) NULL,
	[IsAbroved] [bit] NULL,
 CONSTRAINT [PK_ThirdBranchRegister] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TimeTable]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TimeTable](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Year] [nvarchar](50) NULL,
	[Day] [nvarchar](50) NULL,
	[Time] [nvarchar](50) NULL,
	[ElmatoneId] [int] NULL,
 CONSTRAINT [PK_TimeTable] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Tracks]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tracks](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
 CONSTRAINT [PK_Tracks] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Visualiztions]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Visualiztions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Tittel] [nvarchar](50) NULL,
	[Link] [nvarchar](50) NULL,
	[MatrialId] [int] NULL,
	[ElmatoneId] [int] NULL,
 CONSTRAINT [PK_Visualiztions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Words]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Words](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Details] [nvarchar](max) NULL,
	[IsSupervisor] [bit] NULL,
	[ImagePathe] [nvarchar](max) NULL,
 CONSTRAINT [PK_Words] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [RoleNameIndex]    Script Date: 11/10/2018 04:32:04 م ******/
CREATE UNIQUE NONCLUSTERED INDEX [RoleNameIndex] ON [dbo].[AspNetRoles]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 11/10/2018 04:32:04 م ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserClaims]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 11/10/2018 04:32:04 م ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserLogins]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_RoleId]    Script Date: 11/10/2018 04:32:04 م ******/
CREATE NONCLUSTERED INDEX [IX_RoleId] ON [dbo].[AspNetUserRoles]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 11/10/2018 04:32:04 م ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserRoles]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UserNameIndex]    Script Date: 11/10/2018 04:32:04 م ******/
CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex] ON [dbo].[AspNetUsers]
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Advantages]  WITH CHECK ADD  CONSTRAINT [FK_Advantages_Branches] FOREIGN KEY([BranchId])
REFERENCES [dbo].[Branches] ([Id])
GO
ALTER TABLE [dbo].[Advantages] CHECK CONSTRAINT [FK_Advantages_Branches]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AssignmentsDate]  WITH CHECK ADD  CONSTRAINT [FK_AssignmentsDate_Branches] FOREIGN KEY([BranchId])
REFERENCES [dbo].[Branches] ([Id])
GO
ALTER TABLE [dbo].[AssignmentsDate] CHECK CONSTRAINT [FK_AssignmentsDate_Branches]
GO
ALTER TABLE [dbo].[Colleges]  WITH CHECK ADD  CONSTRAINT [FK_Colleges_Colleges] FOREIGN KEY([BranchId])
REFERENCES [dbo].[Branches] ([Id])
GO
ALTER TABLE [dbo].[Colleges] CHECK CONSTRAINT [FK_Colleges_Colleges]
GO
ALTER TABLE [dbo].[Courses]  WITH CHECK ADD  CONSTRAINT [FK_Courses_Branches] FOREIGN KEY([BranchId])
REFERENCES [dbo].[Branches] ([Id])
GO
ALTER TABLE [dbo].[Courses] CHECK CONSTRAINT [FK_Courses_Branches]
GO
ALTER TABLE [dbo].[Courses]  WITH CHECK ADD  CONSTRAINT [FK_Courses_Colleges] FOREIGN KEY([ColleageId])
REFERENCES [dbo].[Colleges] ([Id])
GO
ALTER TABLE [dbo].[Courses] CHECK CONSTRAINT [FK_Courses_Colleges]
GO
ALTER TABLE [dbo].[Courses]  WITH CHECK ADD  CONSTRAINT [FK_Courses_Elmatone] FOREIGN KEY([ElmatoneId])
REFERENCES [dbo].[Elmatone] ([Id])
GO
ALTER TABLE [dbo].[Courses] CHECK CONSTRAINT [FK_Courses_Elmatone]
GO
ALTER TABLE [dbo].[Courses]  WITH CHECK ADD  CONSTRAINT [FK_Courses_Matiriale] FOREIGN KEY([MatarialeId])
REFERENCES [dbo].[Matiriale] ([Id])
GO
ALTER TABLE [dbo].[Courses] CHECK CONSTRAINT [FK_Courses_Matiriale]
GO
ALTER TABLE [dbo].[EducationBody]  WITH CHECK ADD  CONSTRAINT [FK_EducationBody_Branches] FOREIGN KEY([BranchId])
REFERENCES [dbo].[Branches] ([Id])
GO
ALTER TABLE [dbo].[EducationBody] CHECK CONSTRAINT [FK_EducationBody_Branches]
GO
ALTER TABLE [dbo].[Elmatone]  WITH CHECK ADD  CONSTRAINT [FK_Elmatone_Matiriale] FOREIGN KEY([MatrialeId])
REFERENCES [dbo].[Matiriale] ([Id])
GO
ALTER TABLE [dbo].[Elmatone] CHECK CONSTRAINT [FK_Elmatone_Matiriale]
GO
ALTER TABLE [dbo].[EvaluationCriteria]  WITH CHECK ADD  CONSTRAINT [FK_EvaluationCriteria_Branches] FOREIGN KEY([BranchId])
REFERENCES [dbo].[Branches] ([Id])
GO
ALTER TABLE [dbo].[EvaluationCriteria] CHECK CONSTRAINT [FK_EvaluationCriteria_Branches]
GO
ALTER TABLE [dbo].[FirestBranchRegister]  WITH CHECK ADD  CONSTRAINT [FK_FirestBranchRegister_Nationality] FOREIGN KEY([NationalityId])
REFERENCES [dbo].[Nationality] ([Id])
GO
ALTER TABLE [dbo].[FirestBranchRegister] CHECK CONSTRAINT [FK_FirestBranchRegister_Nationality]
GO
ALTER TABLE [dbo].[FirestBranchRegister]  WITH CHECK ADD  CONSTRAINT [FK_FirestBranchRegister_Satage] FOREIGN KEY([StageId])
REFERENCES [dbo].[Satage] ([Id])
GO
ALTER TABLE [dbo].[FirestBranchRegister] CHECK CONSTRAINT [FK_FirestBranchRegister_Satage]
GO
ALTER TABLE [dbo].[SecondBranchRegister]  WITH CHECK ADD  CONSTRAINT [FK_SecondBranchRegister_EducationBody] FOREIGN KEY([EducationBodyId])
REFERENCES [dbo].[EducationBody] ([Id])
GO
ALTER TABLE [dbo].[SecondBranchRegister] CHECK CONSTRAINT [FK_SecondBranchRegister_EducationBody]
GO
ALTER TABLE [dbo].[SecondBranchRegister]  WITH CHECK ADD  CONSTRAINT [FK_SecondBranchRegister_Nationality] FOREIGN KEY([NationalityId])
REFERENCES [dbo].[Nationality] ([Id])
GO
ALTER TABLE [dbo].[SecondBranchRegister] CHECK CONSTRAINT [FK_SecondBranchRegister_Nationality]
GO
ALTER TABLE [dbo].[SecondBranchRegister]  WITH CHECK ADD  CONSTRAINT [FK_SecondBranchRegister_Satage] FOREIGN KEY([StageId])
REFERENCES [dbo].[Satage] ([Id])
GO
ALTER TABLE [dbo].[SecondBranchRegister] CHECK CONSTRAINT [FK_SecondBranchRegister_Satage]
GO
ALTER TABLE [dbo].[ThirdBranchRegister]  WITH CHECK ADD  CONSTRAINT [FK_ThirdBranchRegister_EducationBody] FOREIGN KEY([EducationBodyId])
REFERENCES [dbo].[EducationBody] ([Id])
GO
ALTER TABLE [dbo].[ThirdBranchRegister] CHECK CONSTRAINT [FK_ThirdBranchRegister_EducationBody]
GO
ALTER TABLE [dbo].[ThirdBranchRegister]  WITH CHECK ADD  CONSTRAINT [FK_ThirdBranchRegister_Nationality] FOREIGN KEY([NationalityId])
REFERENCES [dbo].[Nationality] ([Id])
GO
ALTER TABLE [dbo].[ThirdBranchRegister] CHECK CONSTRAINT [FK_ThirdBranchRegister_Nationality]
GO
ALTER TABLE [dbo].[ThirdBranchRegister]  WITH CHECK ADD  CONSTRAINT [FK_ThirdBranchRegister_Satage] FOREIGN KEY([StageId])
REFERENCES [dbo].[Satage] ([Id])
GO
ALTER TABLE [dbo].[ThirdBranchRegister] CHECK CONSTRAINT [FK_ThirdBranchRegister_Satage]
GO
/****** Object:  StoredProcedure [dbo].[DeleteOldCourses]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[DeleteOldCourses]


@StudentId int 
as

begin
	BEGIN TRY
	 Delete  from CourcesStudent where StudentId=@StudentId
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_AddAcoustics]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_AddAcoustics]
@Tittel nvarchar(50),
@ElmatoneId int ,


@FilePathe nvarchar(max),
@MatrialId int ,
@Id int 
as

begin
	BEGIN TRY
	
	
if(@Id=0)
Insert Acoustics(Tittel,ElmatoneId,FilePathe,MatrialId) Values (@Tittel,@ElmatoneId,@FilePathe,@MAtrialId)

else
 update  Acoustics set Tittel=@Tittel, ElmatoneId=@ElmatoneId,
	 FilePathe=@FilePathe,MatrialId=@MatrialId where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 end
GO
/****** Object:  StoredProcedure [dbo].[SP_AddAdvantages]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_AddAdvantages]

@Details nvarchar(max),
@BranchId int ,
@Id int 
as

begin
	BEGIN TRY
	
	
	if(@Id=0)

Insert into  Advantages(Details,BranchId) values (@Details,@Branchid)
else
 update  Advantages set Details=@Details ,BranchId=@BranchId where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_AddAdvertisement]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_AddAdvertisement]
@Tittel nvarchar(50),
@Details nvarchar(max),
@ImagePathe nvarchar(max),
@DateTime datetime,
@Id int 
as

begin
	BEGIN TRY
	
	
if(@Id=0)
Insert into Advertisement(Tittel,Details,ImagePathe,Datetime) Values (@Tittel,@Details,@ImagePathe,@DateTime)

else
 update  Advertisement set Tittel=@Tittel, Details=@Details,
	 ImagePathe=@ImagePathe,Datetime=@DateTime where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_AddAssignmentsDate]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_AddAssignmentsDate]

@Details nvarchar(max),
@Tittel nvarchar(50),
@BranchId int ,
@Id int 
as

begin
	BEGIN TRY
	
	
	if(@Id=0)

Insert into  AssignmentsDate(Details,Tittel,BranchId) values (@Details,@Tittel,@Branchid)
else
 update  AssignmentsDate set Details=@Details,
 Tittel=@Tittel ,BranchId=@BranchId where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_AddColleges]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_AddColleges]

@Details nvarchar(max),
@BranchId int ,
@Id int 
as

begin
	BEGIN TRY
	
	
	if(@Id=0)

Insert into  Colleges(Name,BranchId) values (@Details,@Branchid)
else
 update  Colleges set Name=@Details ,BranchId=@BranchId where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_AddContactUs]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_AddContactUs]

@Name nvarchar(50),
@Email nvarchar(50),
@Phone nvarchar(50) ,
@Subject nvarchar(50),
@Message nvarchar(max),

@Id int 
as

begin
	BEGIN TRY
	
	
	

Insert into  ContactUs(Name,Email,Phone,Subject,Message) values (@Name,@Email,@Phone,@Subject,@Message)

 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_AddCourses]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_AddCourses]

@BranchId int,
@ColleageId int ,
@MatarialeId int ,
@ElmatoneId int,
@Level nvarchar(50),
@Masare nvarchar(50),
@Weak nvarchar(50),
@Year nvarchar(50),
@Id int 
as

begin
	BEGIN TRY
	
	
	if(@Id=0)

Insert into  Courses(BranchId,ColleageId,MatarialeId,ElmatoneId,Level,Masare,Weak,Year) values (@BranchId,@ColleageId,@MatarialeId,@ElmatoneId,@Level,@Masare,@Weak,@Year)
else
 update  Courses set BranchId=@BranchId,
 ColleageId=@ColleageId ,MatarialeId=@MatarialeId , ElmatoneId=@ElmatoneId,Level=@Level,Masare=@Masare ,Weak=@Weak,Year=@Year where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_AddElmatone]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_AddElmatone]
@Name nvarchar(50),
@MatrialeId int,

@Id int 
as

begin
	BEGIN TRY
	
	
if(@Id=0)
Insert into Elmatone(Name,MatrialeId) Values (@Name,@MatrialeId)

else
 update  Elmatone set Name=@Name, MatrialeId=@MatrialeId
	 where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_AddEvaluationCriteria]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_AddEvaluationCriteria]

@Name nvarchar(max),
@Details nvarchar(50),
@BranchId int ,
@Id int 
as

begin
	BEGIN TRY
	
	
	if(@Id=0)

Insert into  EvaluationCriteria(Name,Details,BranchId) values (@Name,@Details,@Branchid)
else
 update  EvaluationCriteria set Name=@Name,
 Details=@Details ,BranchId=@BranchId where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_AddEvualtionStudent]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_AddEvualtionStudent]

@StudentId int,

@ElmatoneId int,
@Degree int,

@Day nvarchar(50),
@Weak nvarchar(50),
@Oral nvarchar(50),
@Nots nvarchar(50),
@Id int 
as

begin
	BEGIN TRY
	
	
	if(@Id=0)

Insert into  EvulationStudent(StudentId,ElmatoneId,Degree,Day,Weaky,Oral,Nots) values (@StudentId,@ElmatoneId,@Degree
,@Day,@Weak,@Oral,@Nots)

 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_AddFirestBranchRegister]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_AddFirestBranchRegister]
@Name nvarchar(50),
@NationalityId int ,
@JoinedUniversity nvarchar(50),
@Code nvarchar(40),
@Phonenumber nvarchar(50),
@WhatsNumber nvarchar(50),
@TelgramNumber nvarchar(50),
@Email nvarchar(50),
@EducationBodyId int,
@StageId int,

@Level nvarchar(50),
@Case nvarchar(50),
@ColleageId int ,
@TrackId int ,
@EvaulationDayes nvarchar(50),

@Id int 
as

begin
	BEGIN TRY
	
	declare @LastId int
	if(@Id=0)

Insert into  FirestBranchRegister(Name,NationalityId,JoinedUniversity,Code,Phonenumber,WhatsNumber,Telgramnumber,Email,EducationBodyId,
StageId,Level,[Case],ColleageId,TrackId,EvaulationDayes)
 values (@Name,@NationalityId,@JoinedUniversity,@Code,@Phonenumber,@WhatsNumber,@TelgramNumber,@Email,@EducationBodyId,
 @StageId,@Level,@Case,@ColleageId,@TrackId,@EvaulationDayes)

 select @LastId = max(Id) from [dbo].[FirestBranchRegister]
 --Return @LastId;

 end TRY

 begin Catch
 --IF @@TRANCOUNT > 0
	--				BEGIN
	--					ROLLBACK TRANSACTION ; -- rollback to MySavePoint
	--					return @LastId;
	--				END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_AddFirestBranchRegisterApproved]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_AddFirestBranchRegisterApproved]

@Id int 
as

begin
	BEGIN TRY
	
	
	

 update  FirestBranchRegister set IsAbroved=1 where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_AddIntroducingPrize]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create Proc [dbo].[SP_AddIntroducingPrize]

@Details nvarchar(max),
@Id int 
as
Declare @OldId int
begin
	BEGIN TRY
	 select @OldId= Max( Id) from IntroducingPrize 
	 if(@OldId >0)
	 update  IntroducingPrize set Details=@Details where Id=@Id
	 else

Insert into  IntroducingPrize  (Details) values (@Details)
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION MySavePoint; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_AddLibrary]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_AddLibrary]
@Tittel nvarchar(50),
@Details nvarchar(max),
@ImagePathe nvarchar(max),
@DateTime datetime,
@Id int 
as

begin
	BEGIN TRY
	
	
if(@Id=0)
Insert into Library(Tittel,Details,ImagePathe,Datetime) Values (@Tittel,@Details,@ImagePathe,@DateTime)

else
 update  Library set Tittel=@Tittel, Details=@Details,
	 ImagePathe=@ImagePathe,Datetime=@DateTime where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_AddLibraryRead]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_AddLibraryRead]
@Tittel nvarchar(50),
@ElmatoneId int ,


@FilePathe nvarchar(max),
@MatrialId int ,
@Id int 
as

begin
	BEGIN TRY
	
	
if(@Id=0)
Insert LibraryRead(Tittel,ElmatoneId,FilePathe,MatrialId) Values (@Tittel,@ElmatoneId,@FilePathe,@MAtrialId)

else
 update  LibraryRead set Tittel=@Tittel, ElmatoneId=@ElmatoneId,
	 FilePathe=@FilePathe,MatrialId=@MatrialId where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_AddMatiriale]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_AddMatiriale]
@Name nvarchar(50),

@Id int 
as

begin
	BEGIN TRY
	
	
if(@Id=0)
Insert into Matiriale(Name) Values (@Name)

else
 update  Matiriale set Name=@Name where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_AddNews]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_AddNews]
@Tittel nvarchar(50),
@Details nvarchar(max),
@ImagePathe nvarchar(max),
@DateTime datetime,
@Id int 
as

begin
	BEGIN TRY
	
	
if(@Id=0)
Insert into News(Tittel,Details,ImagePathe,Datetime) Values (@Tittel,@Details,@ImagePathe,@DateTime)

else
 update  News set Tittel=@Tittel, Details=@Details,
	 ImagePathe=@ImagePathe,Datetime=@DateTime where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_AddPhotoGallery]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_AddPhotoGallery]
@Tittel nvarchar(50),
@Details nvarchar(max),
@ImagePathe nvarchar(max),
@DateTime datetime,
@Id int 
as

begin
	BEGIN TRY
	
	
if(@Id=0)
Insert into PhotoGallery(Tittel,Details,ImagePathe,Datetime) Values (@Tittel,@Details,@ImagePathe,@DateTime)

else
 update  PhotoGallery set Tittel=@Tittel, Details=@Details,
	 ImagePathe=@ImagePathe,Datetime=@DateTime where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_AddRepresentations]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_AddRepresentations]

@Details nvarchar(max),
@Id int 
as

begin
	BEGIN TRY
	
	if(@Id=0)
Insert into  Representations(Details) values (@Details)
else
update Representations set Details=@Details where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION MySavePoint; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_AddScandBranchCourses]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_AddScandBranchCourses]


@MatrialId int ,
@ElmatoneId int,

@Id int 
as

begin
	BEGIN TRY
	
	
	if(@Id=0)

Insert into  ScandBranchCourses(MatrialId,ElmatoneId) values (@MatrialId,@ElmatoneId)
else
 update  ScandBranchCourses set MatrialId=@MatrialId, ElmatoneId=@ElmatoneId where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_AddSecondBranchRegister]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_AddSecondBranchRegister]
@AcadmayDetails nvarchar(50),
@Bachelorsdegree nvarchar(50),
@Case nvarchar(50),
@EducationBodyId int ,
@Elmatone nvarchar(50),




@Email nvarchar(50),
@Level nvarchar(50),
@Name nvarchar(50),
@NationalityId int ,
@Phonenumber nvarchar(50),
@STageEducationhigher nvarchar(50),

@StageId int ,
@TelegramNumber nvarchar(50),
@WhatsNumber nvarchar(50),
@JoinedUniversity nvarchar(50),
@Id int 
as

begin
	BEGIN TRY
	
	
	if(@Id=0)

Insert into  SecondBranchRegister(AcadmayDetails,Bachelorsdegree,[Case],EducationBodyId,Elmatone,Email,Level,Name,NationalityId,Phonenumber,
STageEducationhigher,StageId,TelegramNumber,WhatsNumber,JoinedUniversity)
 values (@AcadmayDetails,@Bachelorsdegree,@Case,@EducationBodyId,@Elmatone,@Email,@Level,@Name,@NationalityId,@Phonenumber,@STageEducationhigher,@StageId
 ,@TelegramNumber,@WhatsNumber,@JoinedUniversity)
else
 update  SecondBranchRegister set AcadmayDetails=@AcadmayDetails ,Bachelorsdegree=@Bachelorsdegree,
 [Case]=@Case,EducationBodyId=@EducationBodyId,Email=@Email,Level=@Level,Name=@Name,NationalityId=@NationalityId,PhoneNumber=@PhoneNumber,STageEducationhigher=@STageEducationhigher,StageId=@StageId,TelegramNumber=@TelegramNumber,WhatsNumber=@WhatsNumber where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_AddSecondBranchRegisterApproved]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_AddSecondBranchRegisterApproved]

@Id int 
as

begin
	BEGIN TRY
	
	
	

 update  SecondBranchRegister set IsAbroved=1 where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_AddStudentCoursies]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_AddStudentCoursies]

@StudentId int,
@ElmatoneId int ,

@Id int 
as

begin
	BEGIN TRY
	
	
	if(@Id=0)

Insert into  CourcesStudent(StudentId,ElmatoneId) values (@StudentId,@ElmatoneId)

 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_AddStudentGards]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_AddStudentGards]

@StudentId int,
@Value money,
@ElmtoneId int ,
@Datetime datetime,

@Id int 
as

begin
	BEGIN TRY
	
	
	if(@Id=0)

Insert into  StudentGrades(StudentId,EmatoneId,Value,Datetime) values (@StudentId,@ElmtoneId,@Value,@Datetime)
else
 update  StudentGrades set StudentId=@StudentId ,EmatoneId=@ElmtoneId, Value=@Value,Datetime=@Datetime where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_AddStudentInstructions]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_AddStudentInstructions]
@Tittel nvarchar(50),
@Details nvarchar(50),
@Datetime datetime,
@Id int 
as

begin
	BEGIN TRY
	
	
if(@Id=0)
Insert into StudentInstructions(Tittel,Details,Datetime) Values (@Tittel,@Details,@Datetime)

else
 update  StudentInstructions set Tittel=@Tittel ,Details=@Details,Datetime=@Datetime where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_AddStudentPrepare]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_AddStudentPrepare]
@StudentId int,
@Weak nvarchar(50),
@Day nvarchar(50),
@Attandce bit,
@Id int 
as

begin
	BEGIN TRY
	
	
if(@Id=0)
Insert into PrepareStudents(StudentId,Weak,Day,IsAttandce) Values (@StudentId,@Weak,@Day,@Attandce)


 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_AddSysteamPrize]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_AddSysteamPrize]

@Details nvarchar(max),
@Id int 
as

begin
	BEGIN TRY
	
	
	if(@Id=0)

Insert into  SysteamPrize(Details) values (@Details)
else
 update  SysteamPrize set Details=@Details where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_AddTeacherInstructions]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_AddTeacherInstructions]
@Tittel nvarchar(50),
@Details nvarchar(50),
@Datetime datetime,
@Id int 
as

begin
	BEGIN TRY
	
	
if(@Id=0)
Insert into TeacherInstructions(Tittel,Details,Datetime) Values (@Tittel,@Details,@Datetime)

else
 update  TeacherInstructions set Tittel=@Tittel ,Details=@Details,Datetime=@Datetime where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_AddTempDetails]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_AddTempDetails]
@ElmatoneId int,
@headerId int ,

@Id int 
as

begin
	BEGIN TRY
	
	
if(@Id=0)
Insert CoursesStudentTempDetails(ElmatoneId,HeaderId) Values (@ElmatoneId,@HeaderId)

 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 end
GO
/****** Object:  StoredProcedure [dbo].[SP_AddTempHeader]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_AddTempHeader]
@StudentId int,

@Id int 
as

begin
	BEGIN TRY
	
	
if(@Id=0)
Insert CoursesStudentTempHeader(StudentId,CoursesCase) Values (@StudentId,0)

else
 update  CoursesStudentTempHeader set StudentId=@StudentId, CoursesCase=1 where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 end
GO
/****** Object:  StoredProcedure [dbo].[SP_AddThirdBranchRegister]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create Proc [dbo].[SP_AddThirdBranchRegister]
@Code nvarchar(50),
@Tracks nvarchar(50),
@Case nvarchar(50),
@EducationBodyId int ,





@Email nvarchar(50),
@Level nvarchar(50),
@Name nvarchar(50),
@NationalityId int ,
@Phonenumber nvarchar(50),


@StageId int ,
@TelgrameNumber nvarchar(50),
@WhatsNumber nvarchar(50),
@Id int 
as

begin
	BEGIN TRY
	
	
	if(@Id=0)

Insert into  ThirdBranchRegister(Code,Tracks,[Case],EducationBodyId,Email,Level,Name,NationalityId,Phonenumber
,StageId,TelgrameNumber,WhatsNumber)
 values (@code,@Tracks,@Case,@EducationBodyId,@Email,@Level,@Name,@NationalityId,@Phonenumber,@StageId
 ,@TelgrameNumber,@WhatsNumber)
else
 update  ThirdBranchRegister set Code=@Code ,Tracks=@Tracks,
 [Case]=@Case,EducationBodyId=@EducationBodyId,Email=@Email,Level=@Level,Name=@Name,NationalityId=@NationalityId,PhoneNumber=@PhoneNumber,StageId=@StageId,TelgrameNumber=@TelgrameNumber,WhatsNumber=@WhatsNumber where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_AddThirdBranchRegisterApproved]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_AddThirdBranchRegisterApproved]

@Id int 
as

begin
	BEGIN TRY
	
	
	

 update  ThirdBranchRegister set IsAbroved=1 where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_AddVisualiztions]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_AddVisualiztions]
@Tittel nvarchar(50),
@Link nvarchar(50),
@MatrialId int ,
@ElmatoneId int ,

@Id int 
as

begin
	BEGIN TRY
	
	
if(@Id=0)
Insert into Visualiztions(Tittel,Link,MatrialId,ElmatoneId) Values (@Tittel,@Link,@MatrialId,@ElmatoneId)

else
 update  Visualiztions set Tittel=@Tittel, Link=@Link,
	 MatrialId=@MatrialId, ElmatoneId=@ElmatoneId  where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_AddWordsManage]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_AddWordsManage]

@Details nvarchar(max),
@ImagePathe nvarchar(max),
@Id int 
as
Declare @OldId int
begin
	BEGIN TRY
	 select @OldId= Max( Id) from Words where IsSupervisor=0
	 if(@OldId >0)
	 update  Words set Details=@Details, ImagePathe=@ImagePathe where Id=@Id
	 else

Insert into  Words(Details,ImagePathe,IsSupervisor) values (@Details,@ImagePAthe,0)
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION MySavePoint; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_AddWordsSupevisor]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_AddWordsSupevisor]

@Details nvarchar(max),
@ImagePathe nvarchar(max),
@Id int 
as
Declare @OldId int
begin
	BEGIN TRY
	 select @OldId= Max( Id) from Words where IsSupervisor=1 
	 if(@OldId >0)
	 update  Words set Details=@Details,
	 ImagePathe=@ImagePathe where Id=@Id
	 else

Insert into  Words(Details,ImagePathe,IsSupervisor) values (@Details,@ImagePathe,1)
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[Sp_BranchesDrop]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create proc [dbo].[Sp_BranchesDrop]

as

select  
pc.Id,
pc.Name


from  [dbo].[Branches] pc  


GO
/****** Object:  StoredProcedure [dbo].[Sp_ColleageDrop]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[Sp_ColleageDrop]
@BranchId int 
as

select  
pc.Id,
pc.Name


from  [dbo].[Colleges] pc   where pc.BranchId=@BranchId


GO
/****** Object:  StoredProcedure [dbo].[SP_DeleteAcoustics]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_DeleteAcoustics]


@Id int 
as
Declare @OldId int
begin
	BEGIN TRY
	 Delete  from Acoustics where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_DeleteAdvantages]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_DeleteAdvantages]


@Id int 
as

begin
	BEGIN TRY
	 Delete  from Advantages where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_DeleteAdvertisement]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_DeleteAdvertisement]


@Id int 
as

begin
	BEGIN TRY
	 Delete  from Advertisement where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_DeleteAssignmentsDate]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_DeleteAssignmentsDate]


@Id int 
as

begin
	BEGIN TRY
	 Delete  from AssignmentsDate where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_DeleteColleges]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_DeleteColleges]


@Id int 
as

begin
	BEGIN TRY
	 Delete  from Colleges where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_DeleteCourses]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_DeleteCourses]


@Id int 
as

begin
	BEGIN TRY
	 Delete  from Courses where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_DeleteElmatone]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_DeleteElmatone]


@Id int 
as

begin
	BEGIN TRY
	 Delete  from Elmatone where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_DeleteEvaluationCriteria]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_DeleteEvaluationCriteria]


@Id int 
as

begin
	BEGIN TRY
	 Delete  from EvaluationCriteria where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_DeleteFirestBranchRegister]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_DeleteFirestBranchRegister]


@Id int 
as

begin
	BEGIN TRY
	 Delete  from FirestBranchRegister where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_DeleteIntroducingPrize]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create Proc [dbo].[SP_DeleteIntroducingPrize]


@Id int 
as
Declare @OldId int
begin
	BEGIN TRY
	 Delete  from IntroducingPrize where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION MySavePoint; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_DeleteLibrary]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_DeleteLibrary]


@Id int 
as
Declare @OldId int
begin
	BEGIN TRY
	 Delete  from Library where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_DeleteLibraryRead]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_DeleteLibraryRead]


@Id int 
as
Declare @OldId int
begin
	BEGIN TRY
	 Delete  from LibraryRead where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_DeleteMatiriale]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_DeleteMatiriale]


@Id int 
as

begin
	BEGIN TRY
	 Delete  from Matiriale where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_DeleteNews]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_DeleteNews]


@Id int 
as

begin
	BEGIN TRY
	 Delete  from News where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_DeletePhotoGallery]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_DeletePhotoGallery]


@Id int 
as
Declare @OldId int
begin
	BEGIN TRY
	 Delete  from PhotoGallery where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_DeleteRepresentations]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_DeleteRepresentations]


@Id int 
as
Declare @OldId int
begin
	BEGIN TRY
	 Delete  from Representations where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_DeleteScandBranchCourses]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_DeleteScandBranchCourses]


@Id int 
as

begin
	BEGIN TRY
	 Delete  from ScandBranchCourses where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_DeleteSecondBranchRegister]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_DeleteSecondBranchRegister]


@Id int 
as

begin
	BEGIN TRY
	 Delete  from SecondBranchRegister where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_DeleteStudentInstructions]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_DeleteStudentInstructions]


@Id int 
as

begin
	BEGIN TRY
	 Delete  from StudentInstructions where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_DeleteSysteamPrize]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_DeleteSysteamPrize]


@Id int 
as

begin
	BEGIN TRY
	 Delete  from SysteamPrize where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_DeleteTeacherInstructions]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_DeleteTeacherInstructions]


@Id int 
as

begin
	BEGIN TRY
	 Delete  from TeacherInstructions where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_DeleteThirdBranchRegister]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_DeleteThirdBranchRegister]


@Id int 
as

begin
	BEGIN TRY
	 Delete  from ThirdBranchRegister where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_DeleteVisualiztions]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_DeleteVisualiztions]


@Id int 
as
Declare @OldId int
begin
	BEGIN TRY
	 Delete  from Visualiztions where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_DeleteWordsSupevisor]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_DeleteWordsSupevisor]


@Id int 
as
Declare @OldId int
begin
	BEGIN TRY
	 Delete  from Words where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION MySavePoint; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_EditTimeTable]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_EditTimeTable]

@ElmatoneId int ,
@Id int 
as

begin
	BEGIN TRY
	

 update  TimeTable set ElmatoneId=@ElmatoneId  where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[Sp_ElmatoneDrop]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[Sp_ElmatoneDrop]
@MatarialeId int 
as
if(@MatarialeId =0)
select  
pc.Id,
pc.Name


from  [dbo].[Elmatone] pc   
else
select  
pc.Id,
pc.Name


from  [dbo].[Elmatone] pc  where pc.MatrialeId=@MatarialeId


GO
/****** Object:  StoredProcedure [dbo].[SP_GeStudentId]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_GeStudentId]

@UserId nvarchar(128) 
as
begin
	BEGIN TRY
	
select 
p.Id,
p.Name,
p.Code,
p.Phonenumber,
p.WhatsNumber,
p.TelgramNumber,
p.EvaulationDayes,
p.Email,
p.TrackId,

p.IsAbroved,
p.JoinedUniversity,
p.Level,
p.[Case],
c.Name as ColleageName,
n.Name as NationalityName,
e.Name as EducationBodyName,
t.Name as Trackname,
s.Name as StageName




 From  FirestBranchRegister p join Colleges c on p.ColleageId=c.Id   
 join Nationality  n on p.NationalityId=n.Id join Satage s  on p.StageId=s.Id join EducationBody e on p.EducationBodyId=e.Id join Tracks t on p.TrackId=t.Id where p.UserId=@UserId
 

 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAllAcoustics]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_GetAllAcoustics]


as
begin
	BEGIN TRY

select 
p.Tittel,
p.Id,
p.FilePathe,
m.Name as MatrialName,
e.Name as ElmatoneName

 From Acoustics p join Matiriale m on p.MatrialId=m.Id join Elmatone e on p.ElmatoneId=e.Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAllAdvantages]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_GetAllAdvantages]

@BranchId int 
as
begin
	BEGIN TRY

select 
p.Id,
p.Details,
b.Name as BranchName


 From Advantages p join Branches b on p.BranchId=b.Id where p.BranchId=@BranchId
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAllAdvantagesWeb]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_GetAllAdvantagesWeb]


as
begin
	BEGIN TRY

select 
*


 From Advantages 
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAllAdvertisement]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_GetAllAdvertisement]


as
begin
	BEGIN TRY

select 
*


 From Advertisement 
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAllAssignmentsDate]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_GetAllAssignmentsDate]
@BranchId int 

as
begin
	BEGIN TRY

select 
p.Id,
p.Details,
p.Tittel,
b.Name as BranchName


 From AssignmentsDate p join Branches b on p.BranchId=b.Id where p.BranchId=@BranchId
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAllAssignmentsDateWeb]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_GetAllAssignmentsDateWeb]


as
begin
	BEGIN TRY

select 
*

 From AssignmentsDate 
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAllColleges]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_GetAllColleges]


as
begin
	BEGIN TRY

select 
p.Id,
p.Name,
b.Name as BranchName


 From Colleges p join Branches b on p.BranchId=b.Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAllCollegesWeb]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_GetAllCollegesWeb]


as
begin
	BEGIN TRY

select 
*


 From Colleges 
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAllCouressByTrackingId]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_GetAllCouressByTrackingId]

@TrackingId int 
as
begin
	BEGIN TRY

select 
p.Id,
b.Name as BranchName,
c.Name as ColleageName,
m.Name as MatarialeName,
e.Name as ElmatoneName,
p.ElmatoneId,
p.Masare,
p.Level,
p.TrackId


 From Courses p join Branches b on p.BranchId=b.Id join Colleges c on p.ColleageId=c.Id join Matiriale m on p.MatarialeId=m.Id join Elmatone e on p.ElmatoneId=e.Id
 where p.TrackId=@TrackingId
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAllCourses]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_GetAllCourses]
@TrackId int 

as
begin
	BEGIN TRY
	if(@TrackId >0)

	select 
p.Id,
b.Name as BranchName,
c.Name as ColleageName,
m.Name as MatarialeName,
p.ElmatoneId,
p.Masare,
p.Level,
e.Name as ElmatoneName


 From Courses p join Branches b on p.BranchId=b.Id join Colleges c on p.ColleageId=c.Id join Matiriale m on p.MatarialeId=m.Id join Elmatone e on p.ElmatoneId=e.Id where p.TrackId=@TrackId
	
	else

select 
p.Id,
b.Name as BranchName,
c.Name as ColleageName,
m.Name as MatarialeName,
p.ElmatoneId,
p.Masare,
p.Level,
e.Name as ElmatoneName


 From Courses p join Branches b on p.BranchId=b.Id join Colleges c on p.ColleageId=c.Id join Matiriale m on p.MatarialeId=m.Id join Elmatone e on p.ElmatoneId=e.Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAllCoursesByStudentId]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_GetAllCoursesByStudentId]
@StudentId int

as
begin
	BEGIN TRY
	

	select 
p.Id,
s.Name as StudentName,

e.Name as ElmatoneName 



 From CourcesStudent p join FirestBranchRegister s on p.StudentId=s.Id join Elmatone e on p.ElmatoneId=e.Id
	

 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAllCoursesWeb]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_GetAllCoursesWeb]


as
begin
	BEGIN TRY

select 

*

 From Courses 
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAllElmatone]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_GetAllElmatone]


as
begin
	BEGIN TRY

select 
p.Id,
p.Name,
m.Name as MatrialeName  


 From Elmatone p join Matiriale m on p.MatrialeId=m.Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAllEvaluationCriteria]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_GetAllEvaluationCriteria]
@BranchId int

as
begin
	BEGIN TRY

select 
p.Id,
p.Details,
p.Name,
b.Name as BranchName


 From EvaluationCriteria p join Branches b on p.BranchId=b.Id where p.BranchId=@BranchId
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAllEvaluationCriteriaWeb]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_GetAllEvaluationCriteriaWeb]


as
begin
	BEGIN TRY

select 
*


 From EvaluationCriteria 
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAllFirestBranchRegister]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_GetAllFirestBranchRegister]

@TrackId int 
as
begin
	BEGIN TRY
	if(@TrackId !=0)
select 
p.Id,
p.Name,
p.Code,
p.Phonenumber,
p.WhatsNumber,
p.TelgramNumber,
p.EvaulationDayes,
p.Email,
p.TrackId,
p.IsAbroved,
p.JoinedUniversity,
p.Level,
p.[Case],
c.Name as ColleageName,
n.Name as NationalityName,
e.Name as EducationBodyName,
t.Name as Trackname,
s.Name as StageName




 From  FirestBranchRegister p join Colleges c on p.ColleageId=c.Id   
 join Nationality  n on p.NationalityId=n.Id join Satage s  on p.StageId=s.Id join EducationBody e on p.EducationBodyId=e.Id join Tracks t on p.TrackId=t.Id where p.TrackId=@TrackId
 
 else
 select 
p.Id,
p.Name,
p.Code,
p.Phonenumber,
p.WhatsNumber,
p.TelgramNumber,

p.Email,
p.EvaulationDayes,
p.IsAbroved,
p.TrackId,
p.JoinedUniversity,
p.Level,
p.[Case],
c.Name as ColleageName,
n.Name as NationalityName,
e.Name as EducationBodyName,
t.Name as Trackname,
s.Name as StageName




 From  FirestBranchRegister p join Colleges c on p.ColleageId=c.Id   
 join Nationality  n on p.NationalityId=n.Id join Satage s  on p.StageId=s.Id join EducationBody e on p.EducationBodyId=e.Id join Tracks t on p.TrackId=t.Id
 

 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAllIntroducingPrize]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create Proc [dbo].[SP_GetAllIntroducingPrize]


as
begin
	BEGIN TRY

select 
*


 From IntroducingPrize 
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION MySavePoint; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAllLibrary]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_GetAllLibrary]


as
begin
	BEGIN TRY

select 
*


 From Library 
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAllLibraryRead]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_GetAllLibraryRead]


as
begin
	BEGIN TRY

select 
p.Tittel,
p.Id,
p.FilePathe,
m.Name as MatrialName,
e.Name as ElmatoneName

 From LibraryRead p join Matiriale m on p.MatrialId=m.Id join Elmatone e on p.ElmatoneId=e.Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAllMatiriale]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_GetAllMatiriale]


as
begin
	BEGIN TRY

select 
*


 From Matiriale 
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAllNews]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_GetAllNews]


as
begin
	BEGIN TRY

select 
*


 From News 
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAllPhotoGallery]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_GetAllPhotoGallery]


as
begin
	BEGIN TRY

select 
*


 From PhotoGallery 
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAllRepresentations]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_GetAllRepresentations]


as
begin
	BEGIN TRY

select 
*


 From Representations 
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION MySavePoint; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAllScandBranchCourses]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_GetAllScandBranchCourses]


as
begin
	BEGIN TRY
	

select 
p.Id,


m.Name as MatrialName,


e.Name as ElmatoneName


 From ScandBranchCourses p join Matiriale m on p.MatrialId=m.Id join Elmatone e on p.ElmatoneId=e.Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAllSecondBranchRegister]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_GetAllSecondBranchRegister]


as
begin
	BEGIN TRY

select 
p.Id,
p.Name,
p.PhoneNumber,

p.WhatsNumber,
p.TelegramNumber,
p.Email,
p.AcadmayDetails,
p.Level,

p.Elmatone,
p.STageEducationhigher,
p.Bachelorsdegree,
p.Year,
p.[Case],
e.Name as EducationBodyName,

n.Name as NationalityName,
s.Name as StageName



 From   SecondBranchRegister p   join Nationality  n on p.NationalityId=n.Id join Satage s  on p.StageId=s.Id join EducationBody e on p.EducationBodyId=e.Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAllStudentInstructions]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_GetAllStudentInstructions]


as
begin
	BEGIN TRY

select 



  * From StudentInstructions 
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAllSysteamPrize]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_GetAllSysteamPrize]


as
begin
	BEGIN TRY

select 
*


 From SysteamPrize 
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION MySavePoint; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAllTeacherInstructions]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_GetAllTeacherInstructions]


as
begin
	BEGIN TRY

select 



  * From TeacherInstructions 
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAllTempHeader]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_GetAllTempHeader]


as
begin
	BEGIN TRY

select 
p.Id,
m.Name as StudentName,
p.StudentId,
p.CoursesCase

 From CoursesStudentTempHeader p join FirestBranchRegister m on p.StudentId=m.Id 
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAllTempHeaderDetails]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_GetAllTempHeaderDetails]

@HeaderId int 
as
begin
	BEGIN TRY

select 
p.Id,
m.Name as ElmatoneName,
p.HeaderId,
p.ElmatoneId


 From CoursesStudentTempDetails p join Elmatone m on p.ElmatoneId=m.Id  where p.HeaderId=@HeaderId
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAllThirdBranchRegister]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_GetAllThirdBranchRegister]


as
begin
	BEGIN TRY

select 
p.Id,
p.Tracks,
p.Name,
p.PhoneNumber,
p.Code,

p.WhatsNumber,
p.TelgrameNumber,
p.Email,

p.Level,


e.Name as EducationBodyName,

n.Name as NationalityName,
s.Name as StageName



 From  ThirdBranchRegister p   join Nationality  n on p.NationalityId=n.Id join Satage s  on p.StageId=s.Id join EducationBody e on p.EducationBodyId=e.Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAllTimeTable]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_GetAllTimeTable]


as
begin
	BEGIN TRY
	

select 
p.Id,
p.Day,
p.Year,
p.Time,
p.ElmatoneId,




e.Name as ElmatoneName


 From TimeTable p join   Elmatone e on p.ElmatoneId=e.Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAllWordsManage]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_GetAllWordsManage]


as
begin
	BEGIN TRY

select 
*


 From Words where IsSupervisor=0
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION MySavePoint; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAllWordsSupevisor]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_GetAllWordsSupevisor]


as
begin
	BEGIN TRY

select 
*


 From Words where IsSupervisor=1
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION MySavePoint; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAlVisualiztions]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_GetAlVisualiztions]


as
begin
	BEGIN TRY

select 
p.Id,
p.Tittel,
p.Link,
m.Name as MatrialName,
e.Name as ElmatoneName


 From Visualiztions p  join Matiriale m on p.MatrialId=m.Id join Elmatone  e on p.ElmatoneId=e.Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetHeader]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_GetHeader]


as
begin
	BEGIN TRY

select 
p.Id,
p.StudentId,
p.CoursesCase,
s.Name as StudentName


 From CoursesStudentTempHeader p join FirestBranchRegister s on p.StudentId=s.Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetLAstId]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_GetLAstId]


as
begin
	BEGIN TRY

select 
*



 From FirestBranchRegister 
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[Sp_MatarialeDrop]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[Sp_MatarialeDrop]

as

select  
pc.Id,
pc.Name


from  [dbo].[Matiriale] pc   


GO
/****** Object:  StoredProcedure [dbo].[SP_SelecLibraryById]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_SelecLibraryById]


@Id int 
as

begin
	BEGIN TRY
	 select * from Library where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_SelecStudentInstructionsById]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_SelecStudentInstructionsById]


@Id int 
as

begin
	BEGIN TRY
	 select * from StudentInstructions where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_SelectAcousticsById]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_SelectAcousticsById]


@Id int 
as

begin
	BEGIN TRY
	 select * from Acoustics where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_SelectAdvantagesId]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_SelectAdvantagesId]


@Id int 
as

begin
	BEGIN TRY
	 select * from Advantages where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_SelectAdvertisementById]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_SelectAdvertisementById]


@Id int 
as

begin
	BEGIN TRY
	 select * from Advertisement where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_SelectAssignmentsDateId]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_SelectAssignmentsDateId]


@Id int 
as

begin
	BEGIN TRY
	 select * from AssignmentsDate where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_SelectCollegesId]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_SelectCollegesId]


@Id int 
as

begin
	BEGIN TRY
	 select * from Colleges where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_SelectCoursesId]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_SelectCoursesId]


@Id int 
as

begin
	BEGIN TRY
	 select * from Courses where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_SelecTeacherInstructionsById]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_SelecTeacherInstructionsById]


@Id int 
as

begin
	BEGIN TRY
	 select * from TeacherInstructions where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_SelectElmatoneId]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_SelectElmatoneId]


@Id int 
as

begin
	BEGIN TRY
	 select * from Elmatone where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_SelectEvaluationCriteriaId]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_SelectEvaluationCriteriaId]


@Id int 
as

begin
	BEGIN TRY
	 select * from EvaluationCriteria where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_SelectFirestBranchRegisterId]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_SelectFirestBranchRegisterId]


@Id int 
as

begin
	BEGIN TRY
	 select * from FirestBranchRegister where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_SelectIntroducingPrizeId]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create Proc [dbo].[SP_SelectIntroducingPrizeId]


@Id int 
as
Declare @OldId int
begin
	BEGIN TRY
	 select * from IntroducingPrize where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION MySavePoint; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_SelectLibraryReadById]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_SelectLibraryReadById]


@Id int 
as

begin
	BEGIN TRY
	 select * from LibraryRead where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_SelectMatirialeId]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_SelectMatirialeId]


@Id int 
as

begin
	BEGIN TRY
	 select * from Matiriale where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_SelectNewsById]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_SelectNewsById]


@Id int 
as

begin
	BEGIN TRY
	 select * from News where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_SelectPhotoGalleryById]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_SelectPhotoGalleryById]


@Id int 
as

begin
	BEGIN TRY
	 select * from PhotoGallery where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_SelectRepresentationsId]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_SelectRepresentationsId]


@Id int 
as
Declare @OldId int
begin
	BEGIN TRY
	 select * from Representations where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION MySavePoint; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_SelectScandBranchCoursesId]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_SelectScandBranchCoursesId]


@Id int 
as

begin
	BEGIN TRY
	 select * from ScandBranchCourses where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_SelectSecondBranchRegisterId]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_SelectSecondBranchRegisterId]


@Id int 
as

begin
	BEGIN TRY
	 select * from SecondBranchRegister where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_SelectSysteamPrizeId]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_SelectSysteamPrizeId]


@Id int 
as

begin
	BEGIN TRY
	 select * from SysteamPrize where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_SelectThirdBranchRegisterId]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_SelectThirdBranchRegisterId]


@Id int 
as

begin
	BEGIN TRY
	 select * from ThirdBranchRegister where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_SelectVisualiztionsById]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_SelectVisualiztionsById]


@Id int 
as

begin
	BEGIN TRY
	 select * from Visualiztions where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION ; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_SelectWordsSupevisorId]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[SP_SelectWordsSupevisorId]


@Id int 
as
Declare @OldId int
begin
	BEGIN TRY
	 select * from Words where Id=@Id
 end TRY

 begin Catch
 IF @@TRANCOUNT > 0
					BEGIN
						ROLLBACK TRANSACTION MySavePoint; -- rollback to MySavePoint
						return 0;
					END

 End Catch

 
end
GO
/****** Object:  StoredProcedure [dbo].[SpEducationBodyDrop]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[SpEducationBodyDrop]
@BranchId int 
as

select  
pc.Id,
pc.Name


from  [dbo].[EducationBody] pc   where pc.BranchId=@BranchId

GO
/****** Object:  StoredProcedure [dbo].[SpNationalityDrop]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create proc [dbo].[SpNationalityDrop]

as

select  
pc.Id,
pc.Name


from  [dbo].[Nationality] pc


GO
/****** Object:  StoredProcedure [dbo].[SpStageDrop]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create proc [dbo].[SpStageDrop]

as

select  
pc.Id,
pc.Name


from  [dbo].[Satage] pc  

GO
/****** Object:  StoredProcedure [dbo].[SpTrackDrop]    Script Date: 11/10/2018 04:32:04 م ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create proc [dbo].[SpTrackDrop]

as

select  
pc.Id,
pc.Name


from  [dbo].[Tracks] pc   

GO
USE [master]
GO
ALTER DATABASE [IslamicAcademy] SET  READ_WRITE 
GO
